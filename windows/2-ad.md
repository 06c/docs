<!-- Cours 14 -->

# Active Directory

Active Directory (AD) est le terme général qui désigne la technologie de services d'annuaire de Microsoft utilisée pour gérer les réseaux informatiques. Il fournit un ensemble de services qui permettent une gestion centralisée des utilisateurs, des groupes, des ordinateurs, et d'autres objets au sein d'un environnement réseau. AD stocke ces informations dans une base de données distribuée et permet leur accès facile et sécurisé.

## Active Directory Domain Services (AD DS)


Active Directory Domain Services est un **annuaire** [LDAP](https://fr.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol) (Lightweight Directory Access Protocol) pour les systèmes d’exploitation Windows. Cet annuaire contient différents objets, de différents types (utilisateurs, ordinateurs, etc.).

Les services de domaine Active Directory (AD DS) sont les services principaux d'Active Directory, qui fournissent les fonctionnalités de gestion des identités et des accès. Ils incluent des services tels que l'authentification, l'autorisation, la réplication, et la gestion des politiques de sécurité.



## Intérêt d'un annuaire

L'Active Directory permet de centraliser la gestion des identités et des accès au sein d'un réseau d'entreprise. Il offre de nombreux avantages, notamment :

- **Centralisation des identités** : Tous les utilisateurs, ordinateurs, groupes, et autres objets du réseau sont stockés dans un annuaire centralisé, ce qui facilite leur gestion et leur administration.
- **Authentification unique** : Les utilisateurs peuvent s'authentifier une seule fois pour accéder à l'ensemble des ressources du réseau, ce qui simplifie l'expérience utilisateur et renforce la sécurité.
- **Gestion des politiques de sécurité** : L'Active Directory permet de définir des politiques de sécurité pour contrôler l'accès aux ressources du réseau, et de les appliquer de manière centralisée.
- **Réplication des données** : Les données de l'Active Directory peuvent être répliquées sur plusieurs serveurs, ce qui permet d'améliorer la disponibilité et la résilience du service.
- **Interopérabilité** : L'Active Directory est compatible avec d'autres services et protocoles, ce qui facilite son intégration au sein d'une infrastructure existante.

## Forêt et domaine

Active Directory utilise une structure hiérarchique pour organiser les objets et les ressources du réseau. Une forêt est le plus haut niveau de la hiérarchie, et peut contenir un ou plusieurs domaines. Un domaine est une unité d'organisation logique qui regroupe des objets et des ressources, et qui définit les limites de sécurité et d'administration au sein de l'infrastructure.


Une entreprise sera donc représentée par « une forêt ». Une forêt est un ensemble de domaines "Active Directory" qui partagent une structure logique, un schéma de données, une configuration d’annuaire et des fonctionnalités identiques.

Même s’il est très courant de n’avoir qu’un domaine au sein d’une forêt, les entreprises de grande taille vont utiliser ce mode de fonctionnement pour identifier leurs différentes structures, par exemple par pays : ca.cegep.com, fr.cegep.com, etc.

```js
Forêt 🌳
├── Domaine 1
│   ├── Unité d'organisation 1
│   └── Unité d'organisation 2
├── Domaine 2
│   ├── Unité d'organisation 1
│   └── Unité d'organisation 2
└── Domaine 3

```

## Utilisateurs et groupes

Active Directory permet de gérer les utilisateurs et les groupes de manière centralisée. Les utilisateurs peuvent être ajoutés, modifiés, ou supprimés, et des groupes peuvent être créés pour regrouper des utilisateurs ayant des droits et des permissions similaires. Cela facilite la gestion des accès et des autorisations au sein du réseau.

## Politiques de groupe

Les politiques de groupe (GPO) sont des outils de gestion qui permettent de définir des paramètres de configuration pour les utilisateurs et les ordinateurs du réseau. Ces paramètres peuvent inclure des restrictions d'accès, des paramètres de sécurité, des configurations de bureau, et d'autres paramètres système. Les GPO sont appliquées de manière centralisée à l'ensemble des objets du réseau, ce qui permet de simplifier la gestion et de garantir la cohérence des configurations.

## Réplication

Active Directory utilise la réplication pour maintenir la cohérence des données entre les différents contrôleurs de domaine. La réplication permet de copier les données d'annuaire d'un contrôleur de domaine à un autre, ce qui garantit que les informations sont à jour et disponibles sur l'ensemble du réseau.


