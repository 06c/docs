<!-- Cours 14 -->
# Windows Server

Windows Server est une famille de systèmes d'exploitation de serveur conçue par Microsoft, qui supporte les infrastructures d'entreprise, les applications web, et la gestion des données et réseaux.

Windows Server sert à :

- **Héberger des services et des applications** : Il permet de déployer et de gérer des applications web, des services de messagerie, des bases de données, et des applications d'entreprise.

- **Gestion des identités** : Grâce à Active Directory, Windows Server facilite la gestion des identités utilisateurs et des politiques d'accès, permettant une authentification et une autorisation sécurisées au sein de l'entreprise.

- **Support de la virtualisation** : Avec Hyper-V, il offre des capacités de virtualisation pour créer et gérer des machines virtuelles.

- **Fournir un réseau d'entreprise** : Il inclut des outils et des services pour la gestion des réseaux d'entreprise.

- **Améliorer la sécurité** : Windows Server intègre des fonctionnalités avancées de sécurité pour protéger contre les menaces externes et internes.

## Services Windows Server

Windows Server inclut de nombreux services et rôles pour répondre aux besoins des entreprises. Voici quelques-uns des services les plus courants :

1. **Active Directory Domain Services (AD DS)** : Permet la gestion centralisée des identités et des politiques d'accès, l'authentification et l'autorisation.

2. **DHCP (Dynamic Host Configuration Protocol)** : Attribue automatiquement des adresses IP aux dispositifs sur le réseau pour faciliter leur configuration réseau.

3. **DNS (Domain Name System)** : Traduit les noms de domaine en adresses IP, facilitant ainsi la navigation et l'accès aux ressources réseau.

4. **File Services** : Permet le stockage, le partage et la sécurisation des fichiers au sein du réseau.

5. **Print Services** : Gère les imprimantes et les travaux d'impression dans un réseau d'entreprise.

6. **Hyper-V** : Offre des services de virtualisation pour exécuter plusieurs systèmes d'exploitation simultanément sur une seule machine physique.

7. **IIS (Internet Information Services)** : Un serveur Web flexible pour héberger des sites Web, des services Web et des applications.

8. **Remote Desktop Services (RDS)** : Permet aux utilisateurs d'accéder à des applications et des bureaux virtuels situés sur un serveur central depuis n'importe quel endroit.

9. **Windows Server Update Services (WSUS)** : Facilite la gestion des mises à jour de sécurité et des correctifs pour les systèmes Windows au sein d'une organisation.

10. **Network Policy and Access Services** : Fournit des services de connexion réseau sécurisée, notamment par VPN (Virtual Private Network), pour les utilisateurs distants.



## Éditions

Il existe plusieurs éditions de Windows Server, chacune adaptée à des besoins spécifiques. Les éditions les plus courantes sont :

- **Essentials**: Pour les petites entreprises avec jusqu'à 25 utilisateurs et 50 appareils.
- **Standard**: Pour les environnements à faible densité ou non virtualisés.
- **Datacenter**: Pour les environnements hautement virtualisés avec des besoins avancés en matière de stockage, de réseau et de virtualisation.