## Cours 28 - Semaine 14

| Thème | Liens |
|-------|-------|
| Cours | [Sécurité des données](securite/6-securite_donnees.md) |

## Cours 27 - Semaine 14

| Thème | Liens |
|-------|-------|
| Cours | [Politique de sécurité](securite/1-politique.md) <br> [Pare-feu (Firewall)](securite/2-pare-feu.md) <br> [Proxy](securite/3-proxy.md) <br> [VPN](securite/4-vpn.md) <br> [Bug Bounty](securite/5-bugbounty.md) |

## Cours 26 - Semaine 13

| Thème | Liens |
|-------|-------|
| Cours | [NAT](cours/18-nat.md) <br> [DMZ](cours/19-dmz.md)|
| Exercice | [Correction Exercices-ACL](https://livecegepfxgqc.sharepoint.com/:w:/s/H24-42006CFX-00001552/Ecpex_A5sadAshaBamDe_lEBK6jk70epY0th3pi3zupYfA?e=AeEXZi) |

## Cours 25 - Semaine 13

| Thème | Liens |
|-------|-------|
| Énoncé du TP4| Voir sur Léa |
| Temps TP3 et Lab3 | |


## Cours 24 - Semaine 12

| Thème | Liens |
|-------|-------|
| Cours | [ACL](cours/17-acl.md) |
| Exercice | [Exercices-ACL](https://livecegepfxgqc.sharepoint.com/:w:/s/H24-42006CFX-00001552/EUVB5INXw_RKtesYxDcXzLcBZCJx33it_LMsE0Sst13_Pg?e=ZUQ20Q) |
| Packet Tracer | [Exercice 1](https://livecegepfxgqc.sharepoint.com/:u:/s/H24-42006CFX-00001552/EXBEA64PSlNOjzzTBdZvPpUBNU3GWxxjq2H__Ps3i-Io5w?e=a9YJSb) <br> [Exercice 2](https://livecegepfxgqc.sharepoint.com/:u:/s/H24-42006CFX-00001552/EbBerH4_Xa5CsHiXGHtK7YcBg6lwPr0v12l1A-G0titQXg?e=imCWGv) |

## Cours 23 - Semaine 12

| Thème | Liens |
|-------|-------|
| Cours | [DNS](cours/16-dns.md) |
| Commande | [nslookup](cli/8-nslookup.md) |
| Énoncé du Lab3 | Voir sur Léa |

## Cours 22 - Semaine 11

| Thème | Liens |
|-------|-------|
| Cours | [Dépannage de réseau](cours/15-depannage.md) |
| Commande | [Telnet](cli/6-telnet.md) <br> [SSH](cli/7-ssh.md) |
| Pratique | [12-TECH_ Creation_des_vlans_routeurRV340](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/Eeuij6S89AdKh31Q_MhY0y4Bx63vt5nfYSsWB-XWy-HZCw?e=VLJsiv) |
| Exercices | [Dépannage 1 - Packet Tracer](https://livecegepfxgqc.sharepoint.com/:u:/s/H24-42006CFX-00001552/EWiLhVc_o8JEn67mD2D3ljQB3rjxvtcwyVaRIHF9A06WVA?e=strPNR) <br> [Dépannage 2 - Packet Tracer](https://livecegepfxgqc.sharepoint.com/:u:/s/H24-42006CFX-00001552/EaRi4bIR0HVBsQkTkif9MLsBmoRST_ZqxMednwRhNhGoDw?e=fNwgFE) |

## Cours 21 - Annulé


## Cours 20 - Semaine 10

| Thème | Liens |
| ------- | ------- |
| Pratique |  [11-TECH_routage_inter-VLAN](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EQ3ja5XpMaJErvswX9l_JrABASZLR6eoyBFaF3V-9ic4Jg?e=C8GRQK) |

## Cours 19 - Semaine 10

| Thème | Liens |
|-------|-------|
| Cours | [Routage](cours/14-routage.md) |
| Pratique | [10-TECH_Commandes_configuration_de_base](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/ESqLK-VYb_dMjKldtyb88ikB6rBOtI9tOxzwztsFtzI3mQ?e=vWbdTl) |
| Énoncé du TP3 | Voir sur Léa |

## Cours 18 - Semaine 9

| Thème | Liens |
|-------|-------|
| Temps TP2 et Lab2 | |


## Cours 17 - Semaine 9

| Thème | Liens |
|-------|-------|
| Cours | [VLAN](cours/13-vlan.md) |
| Pratique | [8-TECH_Configuration_VLANs_modeles_29xx.pdf](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EYv-LCC-HU9IuHbwsmm1rCUBPh4vUXNf8B8Pjihb6fAwpA?e=jy9QK9) <br> [9-TECH_TRUNK_et_VTP](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EQbKjCUvJnJFlIrzgAO7F_EBY12cQlFPCp9EBODo-iPivg?e=sNbXJx) |

## Cours 16 - Semaine 8

| Thème | Liens |
|-------|-------|
| Énoncé du Lab2 | Voir sur Léa |

## Cours 15 - Semaine 8

| Thème | Liens |
|-------|-------|
| Examen 1 | |
| Énoncé du TP2| Voir sur Léa |

## Cours 14 - Semaine 7 

| Thème | Liens |
|-------|-------|
| Cours | [Windows Serveur](windows/1-intro.md), [Active Directory](windows/2-ad.md) |
| Pratique | [4-TECH Windows_Server.pdf](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/ER7ULfLRztdNh9QZUa-X89AB2n9uBNVkyxXcQ1-Qub1-ZQ?e=zrMupb) <br> [5-TECH-_Installation_dun_controleur_de_domaines](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EQDavgi78thDl_xSeOjlXFgBD3V8PCQHaDktq_-5OMHClw?e=wrH0As) <br> [6-TECH_services_dimpression](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EXTn9-B5xuVHpHrkgmE-1UsBDdMPeTENolLmWqLM71E0Kg?e=BFZjGy) <br> [7-TECH Unités_et_utilisateur](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/ET42fLJI2ORPtgktJ56vuO8BlXzdSoEH_VbY8EK6pqkEcg?e=vguody) |

## Cours 13 - Annulé

## Cours 12 - Semaine 6

| Thème | Liens |
|-------|-------|
| Protocole | [FTP](protocoles/8-ftp.md) |
| Cours | [Sondage de satisfaction](soutien/4-satisfaction.md) <br> [09-Les_bases_IOS_Cisco](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/ERPIZ0fCFpRMkVDrGmVt2agB51sDHRf2w5Lpfu1UF0SstQ?e=i4ko4g) |
| Exercices | [10-Exercice_IOS_Cisco](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EZMHwFFhw4RLgwhaP03WjkUBPUzMK49yv9Wcc5HON7C-WQ?e=LflVWp) |

## Cours 11 - Semaine 6

| Thème | Liens |
|-------|-------|
| Cours | [Méthodologie de la résolution de problèmes](soutien/3-methodologie.md) <br> [GLPI - Référentiel ITIL](soutien/2-glpi.html#referentiel-itil) |
| Énoncé du Lab1 | Voir sur Léa |


## Cours 10 - Semaine 5

| Thème | Liens |
|-------|-------|
| Protocole | [IP](protocoles/6-ip.md) <br> [TCP / UDP](protocoles/7-tcp-udp.md) |


## Cours 9 - Semaine 5

| Thème | Liens |
|-------|-------|
| Cours | [Protocole HTTP](protocoles/5-http.html#http) |
| Exercices | [08-Exercice-dépannage-ip-dhcp](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EUCXVnBJBg5PvdJMYhtALj0BjMe55HdO9fcinojL3ZNrFA?e=fna380) |
| Correction | [06-Exercice-Masque-réseau](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EfJoJAsmApRIqPbJtORiXo0BSOz3L8C02qVppKJp4-tHHA?e=Dl36EG) |

## Cours 8 - Annulé

## Cours 7 - Semaine 4

| Thème | Liens |
|-------|-------|
| Cours | [Adressage IP statique et dynamique (DHCP)](cours/12-adressages.md) |
| Packet Tracer | [Serveur](packet_tracer/6-serveur.md), [ARP Poisoning](packet_tracer/7-arp_poisoning.md) |

## Cours 6 - Semaine 3

| Thème | Liens |
|-------|-------|
| Cours | [Sous-reseautage](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/ER-qeM8fsP9DgCB2xg69Mx8Brc_MDTI6qBC7XOED-FBrng?e=EFcYSL), |
| Exercices | [06-Exercice-Masque-réseau](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EaLlr6VOBUtEpFeYg-AviTkB9Djz35a75d4y3Sc6ZaJcTw?e=9EUxF1) |
| Protocole | [ARP](protocoles/4-arp) |
| Commande | [ARP](cli/5-arp) |
| Pratique | [Activité Packet Tracer](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EQOKLYk9frJChOnhruNiQwwBKQ6AJqKbTrnp2lDm14_dKw?e=eN0hR3) |
| Correction | [Correction 04-Exercice-IP](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EU-Id0M6ISpFoNhzodxT-akBt_E_rVv3SvPStszID7N9cw?e=D0Y36f) |

## Cours 5 - Semaine 3

| Thème | Liens |
|-------|-------|
| Cours | [Modèle TCP/IP](cours/9-modele_tcpip.md), [Adressage IP](cours/10-adressage_ip.md) |
| Exercices | [04-Exercice-IP](https://livecegepfxgqc.sharepoint.com/:w:/s/H24-42006CFX-00001552/EcDhyfpfNKBLl4EIRb3MPv0B2PfPVJcfofhHLZlal6UXoQ?e=nl1ZRf) |
| Protocole| [DHCP](protocoles/3-dhcp) |
| Énoncé du TP1| Voir sur Léa |
| Pratique | [Mise en route des routeurs](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EciFtzCkxVNPtjgGaH2pXpwBGmFZXnFkY2TanWISq7m_FA?e=V226gV), [Config DHCP](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EV_ZywmVUPNGineo6AbCpEABt6FFV1GsnERCIfAWgagaMw?e=Dvpvtk) |


## Cours 4 - Semaine 2

| Thème | Liens |
|-------|-------|
| Cours | [Modèle OSI](cours/8-modele_osi.md) |
| Commande | [Netstat](cli/4-netstat) |
| Péripérique | [Routeur](peripheriques/4-routeur.md) |
| Soutien technique | [Introduction](soutien/1-intro.md), [GLPI](soutien/2-glpi.md) |
| Pratique | [Installer GLPI](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/Ea_pYRFx8mtKpFJ-2syAwNIB9LsWVfta9z7VApERIKUisw?e=bwi0vf) et faire des captures d'écran |


## Cours 3 - Semaine 2

| Thème | Liens |
|-------|-------|
| Cours | [Supports de transmission](cours/5-supports.md), [Méthodes d'accès](cours/6-methodes_acces.md), [Diagramme de réseau](cours/7-diagramme.md) |
| Pratique | [Diagramme de réseau](cours/7-diagramme.md#pratique) |


## Cours 2 - Semaine 1

| Thème | Liens |
|-------|-------|
| Cours | [Classification des réseaux](cours/4-classification.md) |
| Vocabulaires | [Adresse MAC](cours/2-vocabulaire.html#adresse-mac-media-access-control), [Collision](cours/2-vocabulaire.md#collision) |
| Commandes | [Tracert](cli/3-tracert) |
| Packet Tracer | [Connexion 3 PC](packet_tracer/5-3pc.md) |
| Périphériques | [Concentrateur](peripheriques/1-hub.md), [Commutateur](peripheriques/2-commutateur.md), [Point d'accès](peripheriques/3-point_acces_sans_fil.md) |
| Pratique | Faire une vidéo "Comment connecter trois PC ensemble" |

## Cours 1 - Semaine 1

| Thème | Liens |
|-------|-------|
| Introduction | [Introduction](cours/1-intro.md), [Cartes réseau](cours/3-cartes_reseau.md)|
| Vocabulaires | [Nœuds](cours/2-vocabulaire.md#nœuds), [Nuages](cours/2-vocabulaire.md#nuage), [Péripériques](cours/2-vocabulaire.md#peripheriques), [Supports](cours/2-vocabulaire.md#supports), [Serveurs](cours/2-vocabulaire.md#serveurs), [Adresse IP](cours/2-vocabulaire.md#adresse-ip) |
| Packet Tracer | [Installation](packet_tracer/1-installation.md), [Configuration](packet_tracer/2-configuration.md), [Démo](packet_tracer/3-demo.md), [Connexion 2 PC](packet_tracer/4-2pc.md) |
| Commandes | [ping](cli/1-ping), [ipconfig](v) |
| Protocole | [ICMP](protocoles/2-icmp) |
 