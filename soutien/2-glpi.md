<!-- Cours 4 -->

# GLPI

https://glpi-project.org/fr/

GLPI (Gestionnaire Libre de Parc Informatique) est un logiciel open-source de gestion des ressources informatiques et de service d'assistance, largement utilisé dans le domaine du soutien technique et de la gestion de parc informatique.

Le Cegep Garneau utilise GLPI pour gérer son parc informatique et pour suivre les demandes de support des utilisateurs.

## Fonctionnalités Clés :

- **Gestion de Parc Informatique** : Inventaire complet des composants matériels et logiciels, avec gestion des licences, contrats, et expiration de garanties.
- **Gestion des Incidents et Demandes** : Un système de tickets permet de suivre et de gérer les demandes d'assistance et les incidents.
- **Gestion de la Relation Utilisateur** : Fournit des interfaces pour les utilisateurs finaux pour soumettre des tickets et suivre leur progression.
- **Rapports et Statistiques** : Capacité de générer des rapports détaillés sur l'inventaire et les activités de helpdesk.

## Avantages de GLPI :

- **Open-Source** : Gratuit et personnalisable, avec une communauté active pour le support et le développement.
- **Modulaire et Extensible** : Possibilité d'ajouter des fonctionnalités supplémentaires via des plugins.
- **Multi-Utilisateur et Multilingue** : Prise en charge de plusieurs utilisateurs et langues, facilitant son utilisation dans des environnements diversifiés.

## Utilisation dans le Soutien Technique :

- GLPI est particulièrement utile pour centraliser les informations, gérer les ressources informatiques, et suivre les demandes de support.
- Il aide à améliorer l'efficacité du service informatique en organisant et en automatisant les processus de support.


## Installation 📝

:::tip 📝 Installation de GLPI sur votre ordinateur.

Téléchargez les consignes : [Installation de GLPI](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/Ea_pYRFx8mtKpFJ-2syAwNIB9LsWVfta9z7VApERIKUisw?e=5Zx8bm)

La documentation de GLPI est disponible sur le site officiel : [Documentation GLPI](https://glpi-install.readthedocs.io/en/latest//)

:::


<!-- Cours 11 -->

## Référentiel ITIL

- GLPI est un outil ITSM (IT Service Management).
- GLPI suit le référentiel ITIL (Information Technology Infrastructure Library). C'est un ensemble de bonnes pratiques permettant d'assurer une bonne gestion du service informatique dans une organisation.
- GLPI permet de gérer 3 types de tickets : incident, problème et changement.

## ITIL c'est quoi ?

ITIL est un référentiel de bonnes pratiques pour gérer les services informatiques. Il est centré sur le client et vise l’amélioration continu du service.

Il est composé de 5 livres qui décrivent les processus, les fonctions et les rôles nécessaires à la gestion des services informatiques.

ITIL permet d’améliorer la qualité d’un service IT, de réduire les coûts et d’aligner les besoins des utilisateurs avec la technique.

## Les processus ITIL

- **Gestion des incidents** : Gère les incidents pour minimiser l’impact sur l’organisation.
- **Gestion des problèmes** : Identifie et résout les problèmes pour éviter les incidents.
- **Gestion des changements** : Planifie et met en place les changements pour améliorer les services.
- **Gestion des niveaux de services** : Négocie et définit les niveaux de services avec les clients.
- **Gestion des configurations** : Gère les configurations pour assurer la qualité des services.
- **Gestion des mises en production** : Met en production les services dans l’environnement de production.
- **Gestion des connaissances** : Partage les connaissances pour améliorer les services.


## Ticket dans GLPI

Un ticket est une demande d'assistance ou un incident signalé par un utilisateur. Il est enregistré dans GLPI et il est suivi jusqu'à sa résolution.

Un ticket contient des informations sur la demande, comme la catégorie, la priorité, le statut, le technicien assigné, etc.

Les tickets sont généralement classés en 3 catégories :

- **Incident** : Un événement non prévu qui cause une interruption ou une dégradation du service.
- **Problème** : La cause sous-jacente d'un ou plusieurs incidents.
- **Changement** : Une modification planifiée dans l'infrastructure informatique.


La gestion d’incident sous GLPI passe par la création de tickets.

Un ticket a un cycle de vie : 
- Selon les normes ITIL , il devrait y avoir à peu près 15 minutes entre chaque état. Pour un maximum de 2 heures
- Il est possible de faire passer le ticket au statut en attente : ex. problème avec le disque dur d’un serveur. Vous ouvrez un ticket avec HP (le fabriquant) qui vous mentionne que cela va prendre 2 jours.

![Cycle de vie d'un ticket](img/ticket.png)

  



  



  

