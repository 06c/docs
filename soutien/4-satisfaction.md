<!-- Cours 12 -->
# Sondage de statisfaction

## Objectif

Le sondage de satisfaction est un outil de mesure de la qualité des services offerts par le département informatique. Il permet de collecter les retours des utilisateurs sur les services rendus et d'identifier les points à améliorer.

## Méthodologie

Pour réaliser un sondage de satisfaction, il est important de suivre une méthodologie rigoureuse :

1. **Définir les objectifs** : Quels sont les objectifs du sondage&nbsp;? Quelles informations souhaite-t-on collecter&nbsp;?
2. **Définir les questions** : Quelles questions poser aux utilisateurs&nbsp;? Les questions doivent être claires, pertinentes et non ambiguës.
3. **Définir la population cible** : Qui sont les utilisateurs à interroger&nbsp;? Quelle est la taille de l'échantillon&nbsp;?
4. **Définir le mode de collecte** : Comment les réponses seront-elles collectées&nbsp;? Par questionnaire papier, par e-mail, par téléphone, etc.
5. **Définir le mode d'analyse** : Comment les réponses seront-elles analysées&nbsp;? Quels sont les critères de satisfaction à prendre en compte&nbsp;?
6. **Réaliser le sondage** : Envoyer le questionnaire aux utilisateurs et collecter les réponses.
7. **Analyser les résultats** : Analyser les réponses pour identifier les points forts et les points faibles.
8. **Rédiger un rapport** : Rédiger un rapport de synthèse des résultats et des recommandations.

## Types de questions

Les questions posées dans un sondage de satisfaction peuvent être de différents types :

- **Questions fermées** : Les réponses sont limitées à un choix prédéfini (oui/non, échelle de satisfaction, etc.).
- **Questions ouvertes** : Les réponses sont libres et non limitées à un choix prédéfini. Elles permettent de recueillir des commentaires et des suggestions.
- **Questions semi-ouvertes** : Les réponses sont limitées à un choix prédéfini, mais les utilisateurs peuvent ajouter des commentaires supplémentaires.

Les questions fermées permettent de collecter des données quantitatives, tandis que les questions ouvertes permettent de collecter des données qualitatives.

Les questions fermées sont plus rapides à analyser.

## Exemple de questions

::: tip Questionnaire de Satisfaction du Service Informatique

**Nom :** [Optionnel]  
**Date de l'intervention :** 
**Technicien :** [Nom du Technicien]

### 1. Clarté de la Communication
- Comment évalueriez-vous la clarté des informations fournies par le technicien&nbsp;?
  - <input type="radio" name="q1"> Très clair
  - <input type="radio" name="q1"> Assez clair
  - <input type="radio" name="q1"> Peu clair
  - <input type="radio" name="q1"> Pas du tout clair

### 2. Temps de Réponse
- Êtes-vous satisfait du temps de réponse du service de support informatique&nbsp;?
  - <input type="radio" name="q2"> Très satisfait
  - <input type="radio" name="q2"> Satisfait
  - <input type="radio" name="q2"> Insatisfait
  - <input type="radio" name="q2"> Très insatisfait

### 3. Qualité de l'Intervention
- La solution apportée a-t-elle résolu votre problème informatique&nbsp;?
  - <input type="radio" name="q3"> Oui, complètement
  - <input type="radio" name="q3"> Partiellement
  - <input type="radio" name="q3"> Non, pas du tout
  - Si non, veuillez préciser :
  <br><br>

### 4. Professionnalisme
- Comment évalueriez-vous le professionnalisme du technicien de support informatique&nbsp;?
  - <input type="radio" name="q4"> Très professionnel
  - <input type="radio" name="q4"> Assez professionnel
  - <input type="radio" name="q4"> Peu professionnel
  - <input type="radio" name="q4"> Pas du tout professionnel

### 5. Recommandation
- Recommanderiez-vous notre service de support informatique à un collègue&nbsp;?
  - <input type="radio" name="q5"> Oui, sans hésiter
  - <input type="radio" name="q5"> Probablement
  - <input type="radio" name="q5"> Peut-être
  - <input type="radio" name="q5"> Non

### 6. Suggestions d'Amélioration
- Avez-vous des suggestions pour améliorer notre service de support informatique&nbsp;? (Veuillez fournir des détails spécifiques si possible.)

<textarea name="q6" rows="4" cols="100" style="max-width: 100%;"></textarea>

### Instructions de Soumission

Si le questionnaire n'est pas rempli en ligne, veuillez le soumettre par e-mail à [adresse e-mail].

Nous vous remercions de prendre le temps de nous aider à améliorer notre service !

:::

## Outils de Sondage

Il existe de nombreux outils pour réaliser des sondages de satisfaction en ligne. Voici quelques exemples d'outils populaires :

- [Google Forms](https://www.google.com/forms/about/)
- [SurveyMonkey](https://www.surveymonkey.com/)
- [Typeform](https://www.typeform.com/)
- [Microsoft Forms](https://forms.office.com/)

- GLPI permet également de réaliser des sondages de satisfaction.

