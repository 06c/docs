<!-- Cours 11 -->
# Méthodologie de la résolution de problèmes

La résolution de problèmes est une compétence essentielle pour les techniciens en informatique. Elle consiste à identifier, analyser et résoudre des problèmes techniques rencontrés par les utilisateurs ou les systèmes informatiques.

La méthodologie de résolution de problèmes est un processus structuré qui permet de résoudre efficacement les problèmes techniques. Elle comprend plusieurs étapes, telles que l'identification du problème, l'analyse des causes, la recherche de solutions et la mise en œuvre de la solution choisie.

## Étapes de la méthodologie de résolution de problèmes

La méthodologie de résolution de problèmes comprend généralement les étapes suivantes :

1. **Identification du problème :** La première étape consiste à identifier le problème. Cela peut être fait en posant des questions à l'utilisateur, en observant les symptômes du problème ou en effectuant des tests pour reproduire le problème.
2. **Analyse des causes :** Une fois le problème identifié, il est important d'analyser les causes possibles du problème. Cela peut inclure l'analyse des erreurs, des logs, des configurations ou des changements récents qui pourraient être liés au problème.
3. **Recherche de solutions :** Après avoir identifié les causes possibles du problème, il est temps de rechercher des solutions. Cela peut inclure la recherche de solutions similaires sur Internet, la consultation de documentations techniques, ou la collaboration avec d'autres techniciens.
4. **Mise en œuvre de la solution :** Une fois une solution identifiée, il est temps de la mettre en œuvre. Cela peut inclure la configuration de systèmes, l'installation de correctifs logiciels, ou la mise en place de mesures correctives.
5. **Vérification de la solution :** Après avoir mis en œuvre la solution, il est important de vérifier si le problème a été résolu. Cela peut inclure des tests, des vérifications de logs, ou des retours d'information de l'utilisateur.
6. **Documentation :** Enfin, il est important de documenter le problème, les causes, les solutions et les étapes suivies pour résoudre le problème. Cela peut être utile pour référence future, pour partager des connaissances avec d'autres techniciens, ou pour améliorer les processus de résolution de problèmes.

## 1. Identification du problème

**Mise en situation** : un utilisateur n’arrive pas à envoyer un courriel. Il contacte le centre technique en disant que le réseau est non fonctionnel.
**Important** :
- Collecter le maximum d’information.
- Écouter l’utilisateur et voir ce qu’il peut/ne peut faire : essayer de voir s’il ne rencontre pas d’autres problèmes.
- Lui demander de reproduire le problème si possible.
- Voir avec d’autres utilisateurs s’ils rencontrent la même problème.

**Contact avec l’utilisateur** : adopter des attitudes et des comportements favorisant l’établissement d’une relation de confiance : 
- Écoute. 
- Compréhension. 
- Empathie. 
- Mise en confiance. 
- Adapter le niveau de langage à celui de l’utilisateur.
- Éviter les termes techniques.
  
**Déterminer la priorité de l’incident** : afin de déterminer comment l’incident sera traité par les équipes de support (ordre de traitement des incidents et moyens mis en œuvre pour traiter un incident en fonction de sa priorité).

  ![priorité de l’incident](img/priorite.png){data-zoomable width=50%}

## 2. Analyse des causes

- Vérifier si le problème est généralisé ou s’il est spécifique à un utilisateur.
- Vérifier si le problème est lié à un équipement spécifique (couche OSI de 1 à 3).
- Vérifier si le problème est lié à un logiciel spécifique (couche 7).
- Vérifier si le problème est lié à un service spécifique (couche 4).
- Vérifier si le problème est lié à un protocole spécifique (couche 3).
- Vérifier si le problème est lié à un changement récent.

**Exemple précédent** :
- Vérifier si le problème est lié à un utilisateur spécifique.
- Vérifier si le problème est lié à un logiciel spécifique (client de courriel).
- Vérifier si le problème est lié à un service spécifique (serveur de courriel).
- Vérifier si le problème est lié à un protocole spécifique (SMTP).

**Analyse des causes** :
- Vérifier les logs.
- Vérifier les configurations.
- Vérifier les erreurs.
- Vérifier les changements récents.

**Exemple précédent** : Vérifier les logs du serveur de courriel, vérifier les configurations du client de courriel, vérifier les erreurs sur le serveur de courriel, vérifier les changements récents sur le serveur de courriel.

- Penser à voir si des changements récents ont été apportés au poste de l’utilisateur.
- Se baser sur les couches OSI pour  essayer de déterminer la source du problème (quel protocole est non fonctionnel).

**Exemple précédent** : Probablement non lié aux premières couches si l’utilisateur arrive à se connecter à internet (protocole SMTP ?).

![couches OSI](img/osi.png){data-zoomable width=30%}


## 3. Recherche de solutions

- Vérifier si des solutions similaires ont déjà été trouvées sur internet.
- Vérifier si des documentations techniques existent.
- Vérifier si des correctifs logiciels ou matériels existent.
- Vérifier si la solution proposée n’engendre pas d’autres problématiques.


**Exemple précédent** :
- Vérifier si le client de courriel est à jour.
- Vérifier si le serveur de courriel est à jour.
- Vérifier si le serveur de courriel est accessible depuis le poste du client.
- Vérifier si les ports ne sont pas bloqués sur le poste du client.
- Éviter de faire des changements au niveau du serveur de courriel si le problème est spécifique à un utilisateur.

Dans certains cas, il est difficile de trouver une solution de premier niveau => **escalader la problématique** :

Le technicien fait part du problème à un spécialiste ou à un chef d'équipe qui effectue des démarches plus pointues auprès des fournisseurs et fabricants, allant parfois jusqu'à la rédaction d'un rapport d'événement déclarant le dysfonctionnement (bogue) au fabricant.

## 4. Mise en œuvre de la solution

- Mettre en place la solution de son côté.
- Contacter l’utilisateur pour valider que la solution mise en place est bien fonctionnelle.

**Exemple précédent** :
- Mettre à jour le client de courriel.
- Vérifier si le serveur de courriel est accessible depuis le poste du client.
- Vérifier si les ports ne sont pas bloqués sur le poste du client.


## 5. Vérification de la solution

- Tester la solution de son côté.
- Contacter l’utilisateur pour valider que la solution mise en place est bien fonctionnelle.

**Contact avec l’utilisateur** :
- Faire des démonstrations pertinentes et efficaces. 
- Faire preuve de patience.
- Adapter le niveau de langage à celui de l’utilisateur.
- Éviter les termes techniques.

**[Sondage de satisfaction](4-satisfaction)** : pour garder trace du degré de satisfaction des utilisateurs de la solution proposée.

## 6. Documenter la solution

La documentation est un processus important afin de pouvoir réutiliser les solutions adéquates à des problèmes déjà rencontrés.

Produire un document d’aide clair :
- Avec des éléments textuels (par étape).
- Des images (capture d’écran et /ou capture vidéo).
- En utilisant un vocabulaire approprié.
- En respectant des normes de documentation.


## Outils et techniques de résolution de problèmes

La résolution de problèmes techniques peut être facilitée par l'utilisation d'outils et de techniques spécifiques. Voici quelques exemples d'outils et de techniques couramment utilisés :


- **Outils de diagnostic système :** Ces outils permettent de collecter des informations sur l'état du système, telles que les logs, les configurations, les performances, etc. Exemples : `top`, `htop`, `dmesg`, `netstat`, `ipconfig`, `ip`, `ping`, `traceroute`, `nslookup`, `dig`, `tcpdump`, `wireshark`, etc.

- **Outils de gestion à distance :** Ces outils permettent de gérer à distance des systèmes informatiques, tels que les outils de prise de contrôle à distance, les outils de gestion de configuration, etc. Exemples : `ssh`, `telnet`, `vnc`, `rdp`, etc.

- **Outils de surveillance et de monitoring :** Ces outils permettent de surveiller en temps réel l'état des systèmes informatiques, tels que les outils de surveillance de la charge CPU, de la mémoire, du réseau, etc. Exemples : `nagios`, `zabbix`, `cacti`, `munin`, `prometheus`, `grafana`, etc.

- **Outils de gestion des logs :** Ces outils permettent de collecter, stocker, analyser et visualiser les logs des systèmes informatiques. Exemples : `syslog`, `rsyslog`, `logrotate`, `logstash`, `fluentd`, `elasticsearch`, `kibana`, etc.

## Pistes de résolution des problèmes

### Pistes couche physique

Se rappeler les limites des supports physique en termes de :
- Distance
- Bande passante maximale
- Type de câblage : croisé/droit, série

Exemple : Câble catégorie 6 limité à 1Go et 100m

Quelques tests : 
- Essayer un autre câble.
- Le port est-il fonctionnel ?

### Pistes couche liaison

Configuration du commutateur inadéquate :
- Configuration half-duplex d’un côté et full-duplex de l’autre côté.
- Adresse MAC bloquée.
- Bande passante limitée via la configuration.

Quelques tests : 
- Voir les indicateurs du switch (vert clignotant pour les port fonctionnel)
- Consulter la configuration (via interface graphique ou ligne de commande (CLI))
- Utiliser les commandes comme `ping`, `arp` pour voir si les équipements sont joignable et bien configurés.


### Pistes couche réseau

- Défaut dans l’adressage IP.
  - Masque inadéquat.
  - IP n’appartenant pas au réseau.
  - Passerelle non spécifiée.
- Configuration du routeur inadéquate.
- Quelques tests :
  - Voir si le routeur est accessible via ping.
  - Consulter la configuration du routeur (via interface graphique/CLI).
  - Utiliser les commandes comme `ipconfig`, `tracert –d` pour s’assurer de la bonne configuration et du bon fonctionnement du routage.

### Outils pour les couches hautes

Commandes :
- `netstat` : lister les ports ouverts avec les protocoles de la couche réseau
- `ssh / telnet` : pour se connecter à un équipement distant. Possible également sur un commutateur / routeur après configuration de l’IP

Autres logiciels :
- Utilisation d’antivirus.
- Capturer les paquets : (Wireshark).
- Surveiller le trafic sur le réseau : (Netflow, Nagios).
- Outils de prise de contrôle à distance. (VNC, RDP, RustDesk, AnyDesk).


  




  



  



  

