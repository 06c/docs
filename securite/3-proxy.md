<!-- Cours 27 -->

# Proxy

Un proxy est un serveur intermédiaire qui se situe entre un client et un serveur. Il agit comme un intermédiaire pour les demandes des clients cherchant des ressources provenant d'autres serveurs.

- Un proxy joue un rôle de "filtre" entre 2 entités pour faciliter ou surveiller leurs échanges.
- Un fournisseur d'accès internet peut mettre en place des serveurs proxy pour accélérer la navigation.
- Il gère les connexions, les infos qui transitent entre 2 réseaux.

Exemple: un proxy peut filtrer le trafic d'un réseau privé allant sur un réseau extérieur, comme Internet, ainsi il participe à la sécurité du réseau.

![Fonctionnement d'un proxy](img/proxy1.png)


## Fonctionnement d'un proxy

Lorsqu'un client envoie une demande à un serveur via un proxy, le proxy intercepte la demande et l'envoie au serveur au nom du client. Le serveur renvoie ensuite la réponse au proxy, qui la transmet au client. Le client ne communique pas directement avec le serveur, ce qui peut offrir des avantages en termes de sécurité, de performances et de confidentialité.

![Fonctionnement d'un proxy](img/proxy3.png)

## Avantages d'un proxy

Les proxies peuvent offrir plusieurs avantages, notamment :

  - **Sécurité** : Les proxies peuvent agir comme une barrière entre les clients et les serveurs, filtrant le trafic pour bloquer les menaces potentielles telles que les logiciels malveillants, les attaques par déni de service distribué (DDoS), etc.
  - **Anonymat** : Certains proxies offrent la possibilité de masquer l'adresse IP réelle du client, ce qui peut aider à protéger la vie privée en ligne.
  - **Optimisation des performances** : Les proxies peuvent mettre en cache les ressources fréquemment demandées, réduisant ainsi le temps de réponse et la bande passante nécessaire pour accéder à ces ressources.
  - **Contournement des restrictions géographiques** : Certains proxies permettent aux utilisateurs de contourner les restrictions géographiques en masquant leur emplacement réel.
  - **Historique** (logs) : journalisation des requêtes
  - **Le filtrage** : restrictions de sites, blocage des publicités ou des contenus lourds. Souvent utilisé dans les entreprises, écoles, bibliothèques pour bloquer l'accès à certains sites.
  - **La répartition de charge** : répartir les requêtes sur plusieurs serveurs

## Incovénients d'un proxy

  - **Ralentissement** : le trafic doit passer par le proxy, ce qui peut entraîner des retards dans la transmission des données.
  - **Dépendance** : si le proxy tombe en panne, la communication entre le client et le serveur peut être interrompue.
  - **Sécurité** : les proxies peuvent également être utilisés pour des activités malveillantes, comme l'interception de données sensibles.
  - **Confidentialité** : Étant donné que vous demandez toutes vos pages au proxy, celui-ci peut savoir tous les sites que vous avez visité.
  - **Modifications** : Le proxy vous fournit les pages, mais il est également possible qu'il les modifie à la volée avant de vous les donner.
  - **Censure** : Certains proxy peuvent être configurés pour censurer des sites.

## Configuration d'un proxy

Pour configurer un proxy, les clients doivent généralement spécifier l'adresse IP et le port du serveur proxy dans les paramètres de leur navigateur ou de leur application. Certains proxies nécessitent également des informations d'identification pour se connecter.


## Exemple de configuration d'un proxy

Voici un exemple de configuration d'un proxy HTTP dans un navigateur web :

1. Ouvrez les **Paramètres** de votre navigateur web, puis **Général** et **Paramètres réseau**.
1. Entrez l'adresse IP et le port du serveur proxy.
1. Si nécessaire, saisissez des informations d'identification pour vous connecter au proxy.
1. Enregistrez les paramètres et testez la connexion en accédant à un site web.

![Paramètres Proxy dans Firefox](img/proxy2.png){data-zoomable width=40%}

Cette configuration permettra à votre navigateur d'acheminer le trafic via le serveur proxy, offrant ainsi les avantages associés à l'utilisation d'un proxy HTTP.
