<!-- Cours 27 -->

# VPN (Virtual Private Network)

Un réseau privé virtuel (VPN) est un service qui permet de créer une connexion sécurisée entre un appareil et un réseau privé sur Internet. Les VPN sont souvent utilisés pour protéger les communications en ligne, sécuriser les connexions Wi-Fi publiques, et masquer l'adresse IP de l'utilisateur.

![Représentation d'un VPN](img/vpn1.png)

Il permet d'accéder à des ordinateurs distants comme si l'on était connecté au réseau local. On peut ainsi avoir un accès au réseau interne (réseau d'entreprise).

- Les VPN permettent aux entreprises de créer des réseaux privés sécurisés à moindre coût. Ils sont aussi le moyen le plus fiable pour relier des utilisateurs itinérants, des télétravailleurs, etc.
- Le client et le serveur vont utiliser un système de clés publiques et clés privées.
- Une fois les clés échangées le tunnel est monté et tout ce qui passe dedans sera crypté. Personne d’autre ne pourra déchiffrer les données. (Car un pirate pourrait éventuellement les lire, mais sans les déchiffrer).
- Faire la différence entre les VPN offerts aux utilisateurs sur Internet (gratuit ou payant), et les systèmes VPN mis en place par les entreprises pour accéder à leurs réseaux depuis l’extérieur (généralement pour les membres de l’entreprise).

## Fonctionnement d'un VPN

Un VPN fonctionne en établissant une connexion sécurisée entre l'appareil de l'utilisateur et un serveur VPN. Toutes les données échangées entre l'appareil et le serveur sont cryptées, ce qui signifie qu'elles sont protégées contre l'interception par des tiers. Le serveur VPN agit comme un intermédiaire entre l'appareil de l'utilisateur et Internet, masquant l'adresse IP de l'utilisateur et offrant une confidentialité et une sécurité supplémentaires.


## Avantages d'un VPN

Les VPN offrent plusieurs avantages, notamment :

- **Sécurité** : Les VPN cryptent les données échangées entre l'appareil de l'utilisateur et le serveur VPN, ce qui les protège contre l'interception par des tiers.
- **Confidentialité** : Les VPN masquent l'adresse IP de l'utilisateur, ce qui permet de protéger sa vie privée en ligne.
- **Accès aux contenus restreints géographiquement** : Les VPN permettent aux utilisateurs de contourner les restrictions géographiques en masquant leur emplacement réel.
- **Sécurisation des connexions Wi-Fi publiques** : Les VPN offrent une protection supplémentaire lors de l'utilisation de connexions Wi-Fi publiques non sécurisées.
- **Anonymat** : Les VPN permettent aux utilisateurs de naviguer sur Internet de manière anonyme, sans révéler leur identité ou leur emplacement.
- **Contournement de la censure** : Les VPN peuvent aider les utilisateurs à contourner la censure en ligne en masquant leur emplacement et en cryptant leur trafic.

## Inconvénients d'un VPN

Les VPN peuvent également présenter certains inconvénients, notamment :

- **Ralentissement** : L'utilisation d'un VPN peut entraîner une diminution de la vitesse de connexion en raison de l'encapsulation et du cryptage des données.
- **Dépendance** : Si le serveur VPN tombe en panne, la connexion entre l'utilisateur et Internet peut être interrompue.
- **Complexité** : La configuration et l'utilisation d'un VPN peuvent être complexes pour les utilisateurs novices.
- **Coût** : Certains services VPN payants peuvent être coûteux, en particulier pour les fonctionnalités avancées.

## Configuration d'un VPN

Pour configurer un VPN, les utilisateurs doivent généralement installer un logiciel VPN sur leur appareil et se connecter à un serveur VPN. Ils peuvent ensuite choisir le protocole de connexion et les paramètres de sécurité appropriés pour établir une connexion sécurisée.

## Exemples de VPN

- https://vpn-public.fdn.fr/
- https://protonvpn.com/fr (version gratuite ou payante)



## VPN vs Proxy

- **Couverture** : Le VPN couvre tout le trafic réseau tandis que le proxy est limité à des applications ou des protocoles spécifiques.
- **Sécurité** : Les VPN sont généralement plus sécurisés que les proxies car ils cryptent tout le trafic.
- **Performance** : L'utilisation d'un VPN peut parfois ralentir votre connexion Internet en raison du cryptage et de la distance plus longue parcourue par vos données. Un proxy peut être plus rapide mais offre moins de sécurité.