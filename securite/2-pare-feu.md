<!-- Cours 27 -->

# Pare-feu (Firewall)

Un pare-feu est un dispositif de sécurité réseau conçu pour surveiller et contrôler le trafic entrant et sortant sur un réseau en fonction d'un ensemble de règles de sécurité prédéfinies. Sa fonction principale est de créer une barrière entre un réseau interne sécurisé et un réseau externe, tel qu'Internet, afin de protéger les ressources du réseau interne contre les accès non autorisés et les menaces.

- Conçus pour protéger un réseau contre les menaces provenant de l'extérieur, telles que les attaques par [déni de service](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service) (DoS), les virus, les vers, les chevaux de Troie, etc.
- Ils peuvent également être utilisés pour limiter l'accès à certaines ressources du réseau interne, comme les serveurs de fichiers, les serveurs de messagerie, etc.
- Ils peuvent être mis en œuvre sous forme de **logiciel** ou de **matériel**, ou une combinaison des deux.
- Ils peuvent être configurés pour bloquer ou autoriser le trafic en fonction de divers critères, tels que l'adresse IP source et de destination, le port source et de destination, le protocole, etc.
- C'est un élément essentiel de la sécurité réseau et sont largement utilisés dans les entreprises, les organisations gouvernementales, les fournisseurs de services Internet, etc.
- Ils doivent être testés régulièrement pour s'assurer qu'ils fonctionnent correctement et qu'ils protègent le réseau contre les menaces actuelles.

## Fonctionnalités principales d'un pare-feu

- **Filtrage de paquets** : Cette fonctionnalité examine les en-têtes des paquets de données pour déterminer selon des critères prédéfinis (comme les adresses IP source et destination, les ports de protocole, etc.) si les paquets doivent être autorisés à traverser le pare-feu. Le filtrage de paquets est souvent réalisé de manière statique, basé sur des règles simples et ne nécessite pas la compréhension du contexte plus large du trafic.

- **Inspection d'état** : Contrairement au simple filtrage de paquets, l'inspection d'état, ou Stateful Packet Inspection (SPI), prend en compte l'état de la connexion lors de la prise de décisions de filtrage. Cela permet au pare-feu de distinguer entre les paquets de données qui font partie d'une session réseau existante et légitime, et ceux qui ne le sont pas, offrant ainsi une sécurité plus robuste.

- **Proxy de niveau application** : Certains pare-feu peuvent fonctionner comme des proxies, filtrant les échanges de données au niveau de l'application. Ils peuvent inspecter le contenu des données transitant par le pare-feu pour bloquer le trafic spécifique, comme les virus ou les tentatives d'intrusion.

- **NAT** (Network Address Translation) : De nombreux pare-feu incluent une fonctionnalité de NAT qui masque les adresses IP internes des utilisateurs en les remplaçant par une adresse IP publique unique. Cette fonctionnalité aide également à conserver la confidentialité des adresses IP internes.

## Types de pare-feu

- **Pare-feu logiciel** : Ces pare-feu sont des programmes logiciels installés sur des ordinateurs ou des serveurs. Ils filtrent le trafic entre l'ordinateur ou le réseau sur lequel ils sont installés et un réseau externe.

- **Pare-feu matériel** : Il s'agit de dispositifs physiques qui agissent comme une passerelle entre le réseau de l'entreprise et les réseaux externes. Ils sont généralement plus robustes et gèrent un volume plus élevé de trafic réseau.

## Pare-feu Windows

Pour accéder au pare-feu Windows et configurer ses paramètres, vous pouvez suivre ces étapes générales :

- Ouvrir le pare-feu Windows :
  - Dans Windows 10 et 11, tapez "pare-feu" dans la barre de recherche de la barre des tâches, et sélectionnez "Pare-feu Windows Defender".

![Pare-feu Windows](img/parefeu1.png)

Le pare-feu utilise différents profils (Privé, Public et Domaine) pour appliquer des niveaux de sécurité différents selon le type de réseau auquel le PC est connecté. Par exemple, il peut être configuré pour être plus restrictif sur les réseaux publics que sur les réseaux privés.

- Profils de pare-feu :
  - **Profil Privé** : destiné aux réseaux en lesquels l'utilisateur fait confiance, comme le réseau domestique ou celui d'un bureau privé. Ce profil suppose que la plupart des autres dispositifs sur le réseau sont également dignes de confiance.
  - **Profil Public** : conçu pour les réseaux dans lesquels l'utilisateur ne devrait pas faire confiance à d'autres dispositifs connectés, comme dans les cafés, les aéroports, ou d'autres lieux publics. Ce profil est utilisé pour les réseaux où l'exposition au risque est plus élevée.
  - **Profil Domaine** : utilisé lorsque l'ordinateur est connecté à un domaine contrôlé par un administrateur via Active Directory. Ce profil est pertinent pour les environnements d'entreprise où il existe une gestion centralisée des politiques de sécurité.

- Configurer les règles entrantes et sortantes :
  - En cliquant sur `Paramètres avancés`, on accède au panneau pour modifier les règles pour le trafic entrant ou sortant. On peut créer de nouvelles règles, modifier des règles existantes ou supprimer des règles inutiles.

![configuration des règles du pare-feu](img/parefeu2.png)



