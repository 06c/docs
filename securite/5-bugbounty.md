<!-- Cours 27 -->

# Bug Bounty (prime à la découverte de failles)

Le bug bounty est un programme de récompenses pour les chercheurs en sécurité qui découvrent et signalent des vulnérabilités dans les systèmes informatiques d'une organisation. Les entreprises, les gouvernements et d'autres organisations mettent en place des programmes de bug bounty pour améliorer la sécurité de leurs systèmes en identifiant et en corrigeant les vulnérabilités avant qu'elles ne soient exploitées par des pirates informatiques.


[Article Wikipedia](https://fr.wikipedia.org/wiki/Prime_aux_bogues)

Les hackers (chapeau blanc) qui participent à des programmes de bug bounty peuvent recevoir des récompenses en argent, des cadeaux ou des mentions honorables pour leurs découvertes. Les programmes de bug bounty sont un moyen efficace pour les organisations de renforcer leur sécurité en exploitant les compétences et les connaissances des chercheurs en sécurité externes.

Le terme **Hacker** désigne un spécialiste informatique, qui recherche les moyens de contourner les protections logicielles et matérielles.

Il agit par curiosité, à la recherche de la gloire, par conscience politique, contre rémunération, ou bien par vengeance ou envie de nuire (Wikipedia)

Notons qu’il existe plusieurs « types » de hackers :
- **White hat** (chapeau blanc) : spécialiste informatique qui n'a pas de but nuisible. Les hackers white hats sont essentiels, ils contribuent à  la sécurisation. 
- **Black hat** (chapeau noir) : expert malveillant, cybercriminel agissant dans le but de nuire, de faire du profit ou d'obtenir des informations.

## Exemples de programmes de bug bounty

- **HackerOne** : https://www.hackerone.com/
- **Bugcrowd** : https://www.bugcrowd.com/
- **YesWeHack** : https://www.yeswehack.com/



