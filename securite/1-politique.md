<!-- Cours 27 -->

# Politique de sécurité

La politique de sécurité est un document qui définit les règles et les procédures à suivre pour garantir la sécurité des systèmes d'information d'une organisation. Elle établit les responsabilités des différents acteurs, les mesures de sécurité à mettre en place, les procédures de gestion des incidents, etc.

- La sécurité au niveau matériel n’est pas suffisante.
- Chaque entreprise doit mettre en place une politique de sécurité.
- Une politique de sécurité est un énoncé formel des règles que doivent observer les personnes ayant accès à la technologie et aux informations d’une organisation.

Quelques points à prendre en compte :
- Un pirate informatique peut être un employé ou un partenaire « de confiance »
  - « Jusqu’à 80% des violations de sécurité sont perpétrées de l’intérieur » - FBI
- La défense peut être inefficace :
  - « Une intrusion sur trois survient en présence d’un pare-feu » - Computer Security Institute
- Les employés peuvent commettre des erreurs :
  - Pare-feu, serveurs mal configurés, etc.
- Le réseau peut prendre de l’expansion et changera.
  - Chaque changement introduit de nouveaux risques de sécurité.

Voir le site https://www.pensezcybersecurite.gc.ca/fr/ pour plus d'informations.

La page https://www.pensezcybersecurite.gc.ca/fr/ressources/guide-pensez-cybersecurite-pour-les-petites-et-moyennes-entreprises/ contient un guide pour les petites et moyennes entreprises.

