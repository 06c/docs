<!-- Cours 28 -->

<script setup>
import QuizComponent from './components/QuizzSecuriteComponent.vue'
</script>


# Sécurité des données

## Hachage

Le hachage est une fonction qui prend une entrée (ou 'message') et renvoie une chaîne de caractères de longueur fixe. Le hachage est utilisé pour stocker des mots de passe de manière sécurisée, vérifier l'intégrité des données, etc.

Le hachage transforme irréversiblement des données via un algorithme spécifique, empêchant la reconstruction de l'information originale. Il est essentiel pour vérifier l'intégrité des fichiers ou sécuriser les mots de passe en créant un résumé unique des données. La robustesse d'un bon algorithme de hachage réside dans sa capacité à minimiser les collisions et à produire des résultats variés pour des entrées similaires, ce qui renforce la sécurité.

Le résultat obtenu est appelé **hash** ou **empreinte**. Le hashage est utilisé pour stocker des mots de passe de manière sécurisée, vérifier l'intégrité des données, etc.

- Impossible à inverser. 
- Deux données différentes ne peuvent pas donner le même hash.
- Un petit changement dans les données doit donner un hash complètement différent.
- Utilisé pour stocker des mots de passe de manière sécurisée.
- Utilisé pour vérifier l'intégrité d'un fichier.

- Types de hachage : MD5, SHA-1, SHA-256, SHA-512, ...


Sous **Windows**, pour afficher le hash d'un fichier, on peut utiliser la commande `certutil` :

```shell
certutil -hashfile <fichier> <algorithme>
```

- Algorithme peut être `MD5`, `SHA1`, `SHA256`, etc.

Sous **Linux**, on peut utiliser les commandes `sha256sum` ou `md5sum` pour afficher le hash SHA256 ou MD5 d'un fichier. Par exemple&nbsp;:

```shell
sha256sum <fichier>
md5sum <fichier>
```

::: tip Exemple pour afficher le hash SHA256 du fichier d'installation de vscode.

Quand on télécharge un fichier, il est important de vérifier son hash pour s'assurer qu'il n'a pas été altéré. Par exemple, pour vérifier le hash SHA256 du fichier d'installation de vscode, on peut utiliser la commande suivante :

![Commande pour afficher le hash SHA256 du fichier d'installation de vscode](img/hash1.png)

Sur le site de téléchargement de vscode, on peut voir le hash SHA256 du fichier d'installation :

![Empreinte de vscode sur le site de téléchargement](img/hash2.png)

:::

La plupart des langages de programmation modernes ont des bibliothèques intégrées pour calculer les hachages. Par exemple, en Python, on peut utiliser le module `hashlib` pour calculer les hachages. Voici un exemple de code pour calculer le hachage SHA256 d'une chaîne de caractères en Python :

```python
import hashlib

data = "Hello, World!"
hash_object = hashlib.sha256(data.encode())
hash_hex = hash_object.hexdigest()
print(hash_hex)
```

**Avantages :**
- Stockage sécurisé des mots de passe.
- Vérification de l'intégrité des données.

**Inconvénients :**
- Les collisions peuvent se produire (deux données différentes ayant le même hash).
- Les attaques par force brute peuvent être utilisées pour trouver des données ayant le même hash.

Pour lutter contre les attaques par force brute, on peut utiliser des techniques de salage (salt) et de poivrage (pepper) pour renforcer la sécurité des mots de passe stockés.

### Salage (Salt) 🧂

Le salage est une technique de sécurité utilisée pour renforcer la sécurité des mots de passe stockés en ajoutant une chaîne de caractères aléatoire (le sel) aux mots de passe avant de les hacher. Le sel est stocké avec le mot de passe haché dans la base de données. Cette technique rend les attaques par dictionnaire et par force brute plus difficiles, car les attaquants doivent également deviner le sel pour trouver le mot de passe d'origine.

Le sel doit être unique pour chaque mot de passe, et il est généralement généré de manière aléatoire lors de la création du compte utilisateur. Le sel est stocké avec le mot de passe haché dans la base de données.


### Pepper (Poivre) 

Le poivre est une autre technique de sécurité utilisée pour renforcer la sécurité des mots de passe stockés. Contrairement au sel, qui est stocké avec le mot de passe haché dans la base de données, le poivre est une chaîne de caractères fixe et secrète qui est ajoutée au mot de passe avant le hachage. Le poivre est stocké en dehors de la base de données, généralement dans un fichier de configuration sécurisé.

Le poivre ajoute une couche de sécurité supplémentaire en cas de compromission de la base de données, car les attaquants doivent également deviner le poivre pour trouver le mot de passe d'origine. Le poivre est généralement généré de manière aléatoire et doit être conservé en toute sécurité.



## Chiffrement / Déchiffrement

Le chiffrement est une technique de sécurité qui consiste à transformer des données en un format illisible (chiffré) à l'aide d'une clé de chiffrement. Le chiffrement est utilisé pour protéger la confidentialité des données en les rendant inintelligibles pour les personnes non autorisées. Seules les personnes disposant de la clé de chiffrement peuvent déchiffrer les données et les lire.

C'est surtout utile lorsqu'on a besoin de sécuriser du transfert d'information et s'assurer que personne ne soit capable d'intercepter notre message. On peut également utiliser le chiffrement pour sécuriser des fichiers stockés sur un disque dur.

- Réversible : on peut déchiffrer les données chiffrées avec la clé de déchiffrement.
- Utilisé pour protéger la confidentialité des données.
- http**s** : chiffrement des données entre le navigateur et le serveur web.
- Chiffrement des fichiers stockés sur un disque dur. Si le disque est volé, les données ne peuvent pas être lues sans la clé de déchiffrement.

**Clé de chiffrement** : clé utilisée pour chiffrer et déchiffrer les données.

Il existe deux types de chiffrement : le chiffrement **symétrique** et le chiffrement **asymétrique**.


### Chiffrement Symétrique

Le chiffrement symétrique utilise **la même clé** pour chiffrer et déchiffrer les données. La clé de chiffrement est partagée entre les parties autorisées avant le transfert des données.

Il est rapide et efficace pour de grandes quantités de données mais il nécessite un échange sécurisé de clés.

- Nécessite que l'émetteur partage la clé secrète avec tous les destinataires pour qu'ils puissent accéder aux informations. Ce type de chiffrement est souvent utilisé pour sécuriser les données au repos (données qui ne sont pas activement utilisées ou qui ne circulent pas d’un endroit à l’autre) car il nécessite une méthode sécurisée pour transmettre la clé aux destinataires, ce qui peut compliquer sa mise en œuvre.

**Avantages :**
- Rapide et efficace pour de grandes quantités de données.
- Moins complexe que le chiffrement asymétrique.

**Inconvénients :**
- Nécessite un échange sécurisé de clés.
- La clé de chiffrement doit être partagée entre les parties autorisées.


Exemples de chiffrement symétrique :
- **AES** (Advanced Encryption Standard) : C'est l'un des algorithmes les plus utilisés et il est considéré comme très sûr. Il utilise des clés de 128, 192 ou 256 bits et est efficace pour chiffrer de grands volumes de données.
- **DES** (Data Encryption Standard) : Bien qu'ancien et considéré comme moins sécurisé aujourd'hui, il a été l'un des premiers algorithmes adoptés à grande échelle. Il utilise une clé de 56 bits. (il a fallu 22 heures à des ingénieurs pour venir à bout du chiffrement DES en 1999)
- **3DES** (Triple DES) : Une amélioration de DES, 3DES applique le chiffrement DES trois fois à chaque bloc de données, le rendant plus sûr que DES. Bien qu’il s’agisse d’une alternative plus sécurisée au protocole de chiffrement original, il reste trop faible pour les données sensibles.

Sous Windows, on peut utiliser l'outil `cipher` pour chiffrer des fichiers. Par exemple, pour chiffrer un fichier, on peut utiliser la commande suivante :

```shell
cipher /e <fichier>
```

Pour déchiffrer un fichier, on peut utiliser la commande suivante :

```shell
cipher /d <fichier>
```

Sous Linux, on peut utiliser la commande `openssl` pour chiffrer et déchiffrer des fichiers. Par exemple, pour chiffrer un fichier avec AES-256, on peut utiliser la commande suivante :

```shell
openssl enc -aes-256-cbc -in <fichier> -out <fichier_chiffre>
```

Pour déchiffrer un fichier, on peut utiliser la commande suivante :

```shell
openssl enc -d -aes-256-cbc -in <fichier_chiffre> -out <fichier_dechiffre>
```

::: tip Chiffrement du disque dur sous Windows (BitLocker)

BitLocker est un outil de chiffrement de disque dur intégré à Windows qui permet de chiffrer les disques durs et les lecteurs amovibles pour protéger les données contre les accès non autorisés. BitLocker utilise le chiffrement AES pour protéger les données et nécessite une clé de chiffrement pour accéder aux données chiffrées.

![Vue des options de Bitlocker sous Windows](img/chiffrement2.png)

Rechercher "bitlocker" dans le menu Démarrer pour accéder à l'outil BitLocker sous Windows.

Dans l'outil BitLocker, vous pouvez :
- Activer BitLocker pour un lecteur : Cliquez sur "Activer BitLocker" à côté du lecteur que vous souhaitez chiffrer.
- Sauvegarder la clé de récupération : BitLocker génère une clé de récupération qui peut être utilisée pour déverrouiller le lecteur en cas d'oubli du mot de passe.
- Désactiver BitLocker.

Vous pouvez également voir si le chiffrement BitLocker est activé pour un lecteur en ouvrant l'Explorateur de fichiers et en cliquant avec le bouton droit sur le lecteur. Si BitLocker est activé, vous verrez une icône de cadenas sur le lecteur.

![Vue du cadenas sur le lecteur C:](img/chiffrement1.png)
:::


::: warning Info !
Il semble que dès que vous chiffrez votre disque avec BitLocker, ce dernier envoie votre clé de chiffrement à Microsoft. Voir cet article pour plus d'informations : [Recently Bought a Windows Computer? Microsoft Probably Has Your Encryption Key](https://theintercept.com/2015/12/28/recently-bought-a-windows-computer-microsoft-probably-has-your-encryption-key/)
:::


### Chiffrement Asymétrique

Le chiffrement asymétrique utilise une paire de clés : une **clé publique** et une **clé privée**. Bien qu’elles soient distinctes, les deux clés sont mathématiquement liées.

La clé publique est utilisée pour chiffrer les données, tandis que la clé privée est utilisée pour déchiffrer les données. La clé publique peut être partagée publiquement, tandis que la **clé privée doit être gardée secrète**.

Le chiffrement asymétrique est utilisé pour sécuriser les communications sur Internet, les transactions bancaires en ligne, etc.

**Avantages :**
- Pas besoin d'échanger la clé de chiffrement.
- La clé publique peut être partagée publiquement.
- La clé privée doit être gardée secrète.

**Inconvénients :**
- Plus lent que le chiffrement symétrique.
- Plus complexe que le chiffrement symétrique.


Exemples de chiffrement asymétrique :
- **RSA** (Rivest–Shamir–Adleman) : Un des premiers systèmes de chiffrement asymétriques, largement utilisé pour la sécurisation des échanges de clés et la signature numérique.
- **ECC** (Elliptic Curve Cryptography) : Offre une sécurité comparable à RSA avec des clés plus courtes, ce qui le rend plus rapide et moins gourmand en ressources.
- **Diffie-Hellman** : Souvent utilisé pour l'échange sécurisé de clés sur des canaux non sécurisés. Voir cette [vidéo](https://www.youtube.com/watch?v=1Yv8m398Fv0) qui explique le fonctionnement. C'est un protocole de chiffrement asymétrique qui permet à deux parties de s'échanger des clés de manière sécurisée.


::: tip Création de clés publiques et privées sous Windows et Linux

On peut utiliser l'outil `ssh-keygen` pour générer une paire de clés publique et privée. Par exemple, pour générer une paire de clés `ed25519`, on peut utiliser la commande suivante :

```shell
C:\Users\gduquerroy> ssh-keygen -t ed25519
Generating public/private ed25519 key pair.
Enter file in which to save the key (C:\Users\gduquerroy/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\gduquerroy/.ssh/id_ed25519
Your public key has been saved in C:\Users\gduquerroy/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:KzBvqQwinxjcsoE67xlMQPOPO10iR7HftZd6Ix/I gdu@DT0
The key's randomart image is:
+--[ED25519 256]--+
|  .   .o+=       |
| . . o .+ .      |
|  o . ..     .   |
| = + = . .  o = o|
|+ O +.* .    +   |
|.= =.*.. .    E  |
|  o +. .+        |
+----[SHA256]-----+
```

![Fichiers de clé générés dans le dossier .ssh](img/chiffrement4.png)

Cela générera une paire de clés `Ed25519` dans le dossier `.ssh` de l'utilisateur. La clé publique sera stockée dans un fichier avec l'extension `.pub` et la clé privée sera stockée dans un fichier sans extension.

Il est également possible de créer des clés RSA mais il est maintenant recommandé d'utiliser des clés `Ed25519` pour des raisons de sécurité.

**`Ed25519`** est principalement utilisé pour les signatures numériques, pas pour le chiffrement ou l'échange de clés

Les clés générées sont stockées dans le répertoire `.ssh` de l'utilisateur. Il y a deux fichiers, un pour la clé publique (extension `.pub`) et un pour la clé privée (sans extension). Il est important de garder la clé privée en sécurité et de ne pas la partager.

Avec Gitlab ou Github, on peut ajouter la **clé publique** à notre compte pour pouvoir cloner des dépôts en utilisant SSH.

**Exemple avec Gitlab** : Se rendre dans les préférences de son compte, puis dans la section "SSH Keys" et ajouter la clé publique.

![Vue de la section SSH Keys dans les préférences de Gitlab](img/chiffrement3.png)

:::


### SSL/TLS, asymétrique ou symétrique ?

Le **chiffrement asymétrique** étant plus lent que le chiffrement symétrique, il est seulement utilisé pour échanger des clés de chiffrement symétrique, qui sont ensuite utilisées pour chiffrer les données échangées entre le client et le serveur.

Ensuite toutes les données échangées entre le client et le serveur sont chiffrées à l'aide de la clé de **chiffrement symétrique** (généralement AES). 



## Signature Numérique

La signature numérique est un mécanisme de sécurité utilisé pour vérifier l'authenticité et l'intégrité des données dans le monde numérique. 

### Fonctionnement de la Signature Numérique

- Génération de la Signature :
  - L'émetteur du document ou du message génère un hachage (un résumé cryptographique) des données à signer.
  - Ce hachage est ensuite chiffré avec la clé privée de l'émetteur. Le résultat de ce chiffrement est la signature numérique proprement dite.
  - Le document **original non modifié**, ainsi que la **signature numérique**, sont ensuite envoyés au destinataire.

- Vérification de la Signature :
  - À réception, le destinataire génère à son tour le hachage du document ou message reçu.
  - Le destinataire déchiffre la signature numérique en utilisant la clé publique de l'émetteur.
  - Si le hachage généré par le destinataire correspond au hachage déchiffré de la signature, cela confirme que le message n'a pas été altéré depuis sa signature et que l'émetteur est bien celui qui prétend être l'auteur du message.

**Avantages :**

- **Authenticité** : La signature garantit que le document ou le message provient bien de l'émetteur supposé, car seul le détenteur de la clé privée peut générer la signature valide.
- **Intégrité** : Elle assure que le contenu n'a pas été modifié depuis sa signature. Toute altération du contenu entraînerait un désaccord entre le hachage du document et celui décrypté de la signature.
- **Non-répudiation** : L'émetteur ne peut nier avoir signé le document, car la signature liée à sa clé privée est unique et traçable.

**Utilisations Courantes :**

La signature numérique est largement utilisée dans diverses applications telles que les contrats électroniques, les transactions financières en ligne, et les communications officielles pour garantir que les documents sont authentiques et n'ont pas été falsifiés.


## Différence entre chiffrement et cryptographie

Le **chiffrement** est une technique spécifique de la cryptographie, qui consiste à utiliser des algorithmes pour transformer des informations en une forme sécurisée, rendant les données inaccessibles à ceux qui ne possèdent pas la clé de déchiffrement appropriée.

La **cryptographie**, quant à elle, est la science plus vaste englobant le chiffrement ainsi que d'autres méthodes pour sécuriser les communications et les informations, telles que le hachage, la signature numérique, et la gestion des clés.


## Sources

- https://www.windowsdigitals.com/verify-md5-sha256-checksum-windows-11/
- https://www.entrust.com/fr/resources/learn/encryption
- [Sécuriser les données dans une app web](https://www.youtube.com/watch?v=Lb3Re9qZHMo) de Grafikart
- https://chiffrer.info/

### Pour aller plus loin
- [vidéos CS50 Cybersecurity](https://www.youtube.com/playlist?list=PLhQjrBD2T383Cqo5I1oRrbC1EKRAKGKUE)
- [L'Algorithme qui Sécurise Internet](https://www.youtube.com/watch?v=1Yv8m398Fv0) de Science Etonnante
- [SSL/TLS](https://www.youtube.com/watch?v=6NoeD4u4O2E) de Benjamin Sonntag

## Quiz 🎉

<QuizComponent />