---
# https://vitepress.vuejs.org/reference/default-theme-home-page
layout: home


hero:
  name: "Techniques de l'informatique"
  text: "Réseaux et soutien informatique"
  # tagline: My great project tagline
  # actions:
  #   - theme: brand
  #     text: Django
  #     link: /django/install
    # - theme: alt
    #   text: API Examples
    #   link: /api-examples

features:
  - title: Introduction
    # icon: 
      # src: "/django/img/django-logo-positive.svg"
    details: Accés aux cours.
    link: /cours/1-intro
  - title: Calendrier
  #   details: Accés aux cours.
    link: /calendrier
  - title: Plan de cours
    link: https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EVERK_C_ZjRIorW2YahOM_cB_ebQn5dZkkPW1YAW9Ri12A?e=hULFGq
  #   # icon: 
  #   #   src: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Microsoft_Office_Teams_%282018%E2%80%93present%29.svg"
  #   details: Accés aux cours.
  #   link: /nodejs/1-intro
  # - title: Vue.js
  #   details: Accés aux cours.
  #   link: /vuejs/1-intro
---

