<!-- Cours 1 -->
# Configuration de Packet Tracer

Se rendre dans le menu Option / Préférences pour configurer Packet Tracer.

![menu Option / Préférences](img/option.png)

---

Dans l'onglet `Interface`, cocher la case `Always show port labels in Local Workspace` pour afficher les numéros de port sur les interfaces des routeurs et des commutateurs.

![port_label](img/port_label.png)

---

Dans l'onglet `Miscellaneous`, sélectionner `Auto Clear Event List` pour effacer automatiquement la liste des événements lors de la simulation.

Cela permettra de ne pas avoir de message d'erreur lors de la simulation.

![auto_clear_event_list](img/auto_clear_event_list.png)

---

Réglage de la grosseur de la police de caractère.

Suivant la résolution de votre écran, vous pouvez avoir de la difficulté à lire le texte dans Packet Tracer.

Dans l'onglet `Font`, sélectionner la taille de police que vous préférez.

![Réglage de la grosseur de la police](img/font.png)