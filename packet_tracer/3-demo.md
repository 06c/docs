<!-- Cours 1 -->
# Démo de Packet Tracer

## Utilisation de la boîte de sélection

La boîte de sélection permet de sélectionner un composant afin de le placer dans la zone de travail.

![Boîte de sélection](img/pk-1.gif)

## Choix des types de connexions

![Choix des connexions](img/connexion.png)


![Alt text](img/c-auto.png) Choix automatique  
![Alt text](img/c-console.png) Câble console  
![Alt text](img/c-croise.png) Câble croisé  
![Alt text](img/c-droit.png) Câble droit 

## Barre d'outils communs

Cette barre donne accès à ces outils de travail couramment utilisés : sélectionner, inspecter, supprimer, redimensionner la forme, placer une note, palette de dessin.

![Boîte de sélection](img/pk-2.gif)

## Barre de temps

Cette barre fournit des boutons pour redémarrer les dispositifs et avancer rapidement le temps.

Elle contient une horloge qui affiche le temps relatif en mode temps réel et en mode simulation.

![Boîte de sélection](img/pk-3.gif)

## Choix du mode temps réel ou simulation

![Choix du mode](img/choix_mode.png)

Le mode **temps réel** permet de configurer les équipements et de voir les changements en temps réel.

Le mode **simulation** permet de simuler le fonctionnement du réseau.

## Mode simulation

La partie droite de l'interface permet de voir les événements qui se produisent sur le réseau.

![Mode simulation](img/simulation.png)

1. Bouton pour avancer, revenir en arrière, démarrer ou mettre en pause la simulation.
2. Permet de régler la vitesse de la simulation.
3. Liste des protocoles qui sont affichés dans la fenêtre de simulation.
4. Permet de filtrer les protocoles affichés dans la fenêtre de simulation.
5. Permet de cacher ou d'afficher tous les protocoles d'un coup.

## Choisir les protocoles à afficher

Dans un premier temps, nous allons cacher tous les protocoles.

Puis afficher seulement les protocoles ARP et ICMP.

Nous verrons prochainement à quoi servent ces protocoles.

![Protocoles](img/pk-4.gif)

## Ajout / Suppression d'un module

Pour configurer un équipement, il faut cliquer dessus.

Suivant les besoins, il est possible d'ajouter ou de supprimer des modules dans certains équipements.

Dans le premier exemple, on choisit un routeur qui n'a pas de port ethernet. On ajoute deux modules pour avoir deux ports ethernet.

Ensuite on choisit un ordinateur portable qui par défault qui n'a pas de carte wifi. Il faut supprimer la carte ethernet pour pouvoir ajouter une carte wifi.

N'oubliez pas d'éteindre les équipements avant de les modifier.

![Modules](img/pk-5.gif)

## Interfaces

Chaque port est associé à une interface. 

Les interfaces sont nommées en fonction du type de port&nbsp;:

- fastethernet0/0, fastethernet0/1, ..., pour des ports `fast ethernet` (100 mégabits par seconde (Mbps)).
- gigabitethernet0/0, gigabitethernet0/1, ..., pour des ports `gigabit ethernet` (1 gigabit par seconde (Gbps)).

![Interfaces](img/pk-6.gif)


