<!-- Cours 7 -->

# ARP Poisoning

L'empoisonnement ARP (ARP Poisoning), également connu sous le nom d'*ARP Spoofing*, est une technique d'attaque sur les réseaux informatiques utilisée pour intercepter la communication entre les clients et le serveur sur un réseau local (LAN). Cette attaque exploite le protocole ARP, qui est utilisé pour résoudre les adresses IP en adresses MAC.

L'attaquant envoie des paquets ARP falsifiés (spoofed) pour associer son adresse MAC à l'adresse IP d'un autre appareil sur le réseau. Cela permet à l'attaquant de recevoir le trafic destiné à cet appareil, de le modifier et de le renvoyer à sa destination légitime, sans que les deux appareils ne soient au courant.

Un logiciel commme [Ettercap](https://www.ettercap-project.org/) peut être utilisés pour réaliser ce type d'attaque.

::: danger Avertissement 🚨
Ce type d'attaque est illégal et est puni par la loi. 

Faire des tests d'empoisonnement ARP sur un réseau sans autorisation est une infraction grave et peut entraîner des poursuites judiciaires.
:::

Voici une série de vidéo qui illustre le fonctionnement de l'empoisonnement ARP.

### Vidéo 1
Le `PC0` peut voir les pages hébergées sur le `serveur0`. Le trafic passe par le `routeur0`.

<video controls src="/packet_tracer/img/arp_poi1.webm" width="90%"></video>

### Vidéo 2
Le `PC2` modifie son adresse `MAC` pour celle du routeur.
- Il envoie ensuite une série de `ping` au `PC0` pour que son cache ARP soit modifié.
- Le `PC0` voit maintenant le `PC2` comme étant le routeur.
- Quand il envoie une requête `HTTP` pour le serveur, le `PC2` intercepte la requête.
- Dans Packet Tracer, le `PC2` rejette la requête mais dans la réalité, il pourrait intercepter la requête et la modifier avant de la renvoyer au routeur.

<video controls src="/packet_tracer/img/arp_poi2.webm" width="90%"></video>

### Vidéo 3

- Avec le "sniffer" du `PC2`, on peut voir les paquets qui ont été interceptés. On pourrait les lire et les modifier avant de les renvoyer. 
- C'est pour cela qu'il est important de se connecter à des sites sécurisés (**https**) pour éviter que les informations soient interceptées.
- L'attaquant pourrait voir les paquets mais comme ils sont chiffrés, il ne pourrait pas les lire.

<video controls src="/packet_tracer/img/arp_poi3.webm" width="90%"></video>





