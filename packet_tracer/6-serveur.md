<!-- Cours 7 -->

# Serveur

Dans Packet Tracer, le **serveur** permet de simuler un serveur DHCP, DNS, HTTP, FTP, etc.

Cliquer sur le serveur puis dans l'onglet `Services` pour configurer les services.

![Serveur](img/serveur.png){data-zoomable width=60%}

## Pratique DHCP

::: tip &nbsp; Utilisez Packet Tracer
- Créez un réseau avec un serveur, un commutateur et deux PC.
- Configurez le serveur pour qu'il distribue des adresses IP aux PC.
- Configurez un PC pour qu'il obtienne une adresse IP automatiquement.
- Configurez un PC pour qu'il obtienne une adresse IP manuellement.
- Vérifiez la connectivité entre les PC.
:::

## Serveur HTTP

On peut configurer le serveur afin qu'il héberge un site web.

On pourra ensuite accéder au site web en utilisant un navigateur web sur un PC.

Voir la vidéo suivante pour un exemple de configuration d'un serveur HTTP.

<video controls src="/packet_tracer/img/serveur_http.webm" width="80%"></video>