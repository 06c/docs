<!-- Cours 1 -->
# Packet Tracer

Packet Tracer est un logiciel développé par Cisco qui permet de simuler un réseau informatique.

## Installation
Inscrivez-vous sur le site de Cisco Networking Academy pour télécharger Packet Tracer.

[Packet Tracer 8.2.1](https://www.netacad.com/portal/resources/packet-tracer)

![Inscription Packet Tracer](img/signin.png){data-zoomable width=30%}


Télécharger et installer Packet Tracer 8.2.1 (64 bits) : 



![Packet Tracer](img/packet_tracer_download.png){data-zoomable width=50%}


Au premier lancez, vous devez vous connecter avec votre compte Cisco Networking Academy.

Vous pouvez cocher le sélecteur "Keep me logged in" afin de rester connecté.

![Signup Packet Tracer](img/signup.png){data-zoomable width=50%}

## Traduction en français

Voir les instructions ici : https://github.com/barbidule/Traduction-fr-Packet-tracer-8.x

La traduction est incomplète.

## Interface

L'interface est divisée plusieurs parties :

![Interface Packet Tracer](img/interfaceOverview_1.jpg)

1. **Barre de menu** : Cette barre fournit les menus Fichier, Édition, Options, Affichage, Outils, Extensions, Fenêtre et Aide. Vous y trouverez des commandes de base telles qu'Ouvrir, Enregistrer, Enregistrer sous Pkz, Imprimer, ainsi que les paramètres et préférences.

2. **Barre d'Outils Principale** : Cette barre fournit des icônes de raccourci vers les commandes de menu les plus couramment utilisées.

3. **Barre d'outils communs** : Cette barre donne accès à ces outils de travail couramment utilisés : sélectionner, inspecter, supprimer, redimensionner la forme, placer une note, palette de dessin, ajouter un PDU simple, et ajouter un PDU complexe.

4. **Espace de travail logique/physique et barre de navigation** : Vous pouvez basculer entre l'espace de travail physique et l'espace de travail logique avec les onglets de cette barre.

5. **Espace de travail** : C'est dans cette zone que vous allez placer les dispositifs et les connexions pour créer votre réseau.

6. **Barre Temps réel/simulation** : Vous pouvez basculer entre le Mode Temps Réel et le Mode Simulation avec les onglets de cette barre. Cette barre fournit également des boutons pour redémarrer les dispositifs et avancer rapidement le temps ainsi que les boutons de contrôle de lecture et le bouton de bascule de la liste des événements en mode simulation. De plus, elle contient une horloge qui affiche le temps relatif en mode temps réel et en mode simulation.

7. **Boîte de sélection du type de dispositif** : Cette boîte contient les types de dispositifs et de connexions disponibles dans Packet Tracer. La boîte de sélection spécifique au dispositif changera en fonction du type de dispositif que vous choisissez. Il y a un champ de recherche qui vous permet d'entrer un nom de dispositif pour trouver rapidement ce dispositif spécifique

8. **Boîte de sélection spécifique au dispositif** : C'est dans cette boîte que vous choisissez spécifiquement quels dispositifs vous voulez mettre dans votre réseau et quelles connexions effectuer.

9. **Fenêtre des paquets créés par l'utilisateur** : Cette fenêtre gère les paquets que vous mettez dans le réseau lors de scénarios de simulation.
