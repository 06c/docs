<!-- Cours 23 -->

# nslookup

La commande `nslookup` (Name Server Lookup) est un outil en ligne de commande permettant d'interroger des serveurs DNS pour obtenir des informations sur des noms de domaine, des adresses IP, etc.

## Syntaxe

```bash
nslookup [options] [hostname] [server]
```

- `options` : les options de la commande (optionnel).
- `hostname` : le nom de domaine ou l'adresse IP à interroger.
- `server` : le serveur DNS à interroger (optionnel).

## Exemples

Interroger un nom de domaine :

```bash
> nslookup.exe  google.ca
Serveur :   dns9.quad9.net
Address:  9.9.9.9

Réponse ne faisant pas autorité :
Nom :    google.ca
Addresses:  2607:f8b0:4020:807::2003
          172.217.13.163
```

On peut voir que le serveur DNS utilisé est `dns9.quad9.net` et que le nom de domaine `google.ca` est associé à l'adresse IP `172.217.13.163`




