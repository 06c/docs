<!-- Cours 22 -->

# SSH

## Introduction

SSH (Secure Shell) est un protocole de communication sécurisé qui permet d'accéder à distance à un serveur. Il est utilisé pour se connecter à un serveur distant et exécuter des commandes sur ce serveur. SSH est un protocole de couche application qui utilise le protocole TCP pour établir une connexion entre le client et le serveur.

:::warning ⚠ Information ⚠
SSH est une alternative sécurisée à Telnet, car il crypte toutes les données, y compris les noms d'utilisateur et les mots de passe, ce qui rend ces informations inaccessibles aux attaquants. Cela réduit considérablement les risques de sécurité liés à l'interception des données lors de la communication avec un serveur distant.
:::

## Fonctionnalités Clés de SSH

- Cryptage :
  - SSH crypte l'ensemble de la session, garantissant que les données échangées, y compris les mots de passe, sont protégées contre les écoutes clandestines.

- Authentification :
  - SSH supporte plusieurs formes d'authentification. Les plus courantes sont l'authentification par mot de passe et l'authentification par clé publique.
  - L'authentification par clé publique est particulièrement puissante, impliquant une paire de clés (une publique et une privée) pour établir une connexion sécurisée sans nécessiter de mots de passe.

- Intégrité des données :
  - SSH utilise des codes de hachage pour s'assurer de l'intégrité des données transmises.

## Installation sous Windows (interface graphique)

Sous Windows, SSH est un client qui n'est pas installé par défaut. Pour utiliser SSH sous Windows, il est recommandé d'installer un client SSH tiers tel que [PuTTY](https://www.putty.org/), qui est un client SSH gratuit et open source largement utilisé.

## Installation sous Windows (ligne de commande)

Il est également possible d'activer le client OpenSSH intégré à Windows 11 en suivant les étapes suivantes :

1. Ouvrir les **Paramètres**.
2. Cliquer sur **Applications**.
3. Cliquer sur **Fonctionnalités facultatives**.
4. Cliquer sur **Ajouter une fonctionnalité**.
5. Sélectionner **Client OpenSSH**.
6. Cliquer sur **Installer**.


## Installation sous Linux ou macOS

Sous Linux et macOS, SSH est généralement installé par défaut. Pour vérifier si SSH est installé sur votre système, vous pouvez ouvrir un terminal et taper la commande suivante :

```bash
ssh -V
```

Si SSH est installé, vous verrez la version du client SSH affichée à l'écran.

## Utilisation de SSH

Pour se connecter à un serveur via SSH en utilisant le terminal ou la ligne de commande :

```bash
ssh username@hostname
```

- Remplacez **username** par votre nom d'utilisateur sur le serveur distant et **hostname** par l'adresse IP du serveur ou son nom de domaine.

Une fois la connexion établie, on peut exécuter des commandes sur le serveur à distance. Par exemple, pour afficher la liste des fichiers dans le répertoire courant, on peut utiliser la commande `ls` sous Linux :

```bash
ls
```

Pour quitter la session SSH, on peut utiliser la commande `exit` :

```bash
exit
```

## Sécurisation de SSH

Bien que SSH soit sécurisé par défaut, il existe plusieurs pratiques recommandées pour renforcer la sécurité :

- Désactiver l'authentification par mot de passe : privilégiez l'utilisation des clés SSH.
- Changer le port par défaut (22) pour réduire les attaques de force brute.
- Utiliser des mots de passe forts pour les clés SSH.
- Configurer Fail2Ban ou un outil similaire pour bloquer les adresses IP qui tentent de nombreuses connexions infructueuses.

## Exemple de Connexion SSH avec Gitlab ou Github

Un exemple courant d'utilisation de SSH est la connexion à un serveur Git distant, tel que Gitlab, pour cloner ou pousser des dépôts Git. Pour cela, il est recommandé d'utiliser des clés SSH pour l'authentification, ce qui est plus sécurisé que l'authentification par mot de passe.

Les clés SSH sont générées en paires : une **clé publique** et une **clé privée**. La clé publique est ajoutée au serveur Git distant, tandis que la clé privée est stockée localement sur votre machine et **ne doit pas être partagée**.

Pour configurer une clé SSH pour Gitlab, vous pouvez suivre les instructions de cette page [Générer une paire de clés SSH](https://www.it-connect.fr/comment-generer-une-paire-de-cles-ssh-et-lutiliser-avec-gitlab/).

Voir également les instructions plus complète sur Gitlab pour [Ajouter une clé SSH à votre compte Gitlab](https://docs.gitlab.com/ee/user/ssh.html).

Une fois la clé SSH configurée, vous pouvez cloner un dépôt Git distant en utilisant SSH :

```bash
git clone git@hostname:username/repository.git
```

- Remplacez **hostname** par l'adresse IP ou le nom de domaine du serveur Gitlab, **username** par votre nom d'utilisateur Gitlab et **repository** par le nom du dépôt Git.


