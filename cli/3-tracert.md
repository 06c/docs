<!-- cours 2 -->
# tracert

La commande `tracert` permet de tracer le chemin emprunté par un paquet IP entre deux équipements.

Sous Linux, la commande équivalente est `traceroute`.

Exemple&nbsp;:

```bash
C:\>tracert wikipedia.org


Détermination de l’itinéraire vers wikipedia.org [208.80.153.224]
avec un maximum de 30 sauts :

  1     4 ms     2 ms     1 ms  mynetwork.home [192.168.2.1]
  2     4 ms     4 ms     3 ms  10.11.16.41
  3     *        *        *     Délai d’attente de la demande dépassé.
  4    31 ms    30 ms    31 ms  64.230.36.102
  5    31 ms    29 ms    31 ms  cr02-toroon12xfw_bundle-ether1.net.bell.ca [142.124.127.252]
  6     *        *        *     Délai d’attente de la demande dépassé.
  7    27 ms    27 ms    26 ms  64.230.79.87
  8    30 ms    29 ms    29 ms  xe-0-1-4.cr2-eqord.wikimedia.org [208.115.136.238]
  9    53 ms    52 ms    52 ms  xe-5-2-1.cr2-codfw.wikimedia.org [208.80.153.223]
 10    52 ms    53 ms    53 ms  text-lb.codfw.wikimedia.org [208.80.153.224]

Itinéraire déterminé.
```

Dans cet exemple, on voit que le paquet IP a emprunté 10 sauts pour arriver à destination.

Un saut correspond à un routeur.

