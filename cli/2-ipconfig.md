<!-- Cours 1 -->
# ipconfig

https://fr.wikipedia.org/wiki/Ipconfig

La commande `ipconfig` permet d'afficher les informations sur les interfaces réseau d'un ordinateur.

Sous Linux, la commande équivalente est `ip addr`.

```bash
C:\>ipconfig

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . : cegep-fxg.qc.ca
   Adresse IPv6 de liaison locale. . . . .: fe80::4ab9:2ab9:832a:5808%15
   Adresse IPv4. . . . . . . . . . . . . .: 10.183.34.29
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.183.32.1

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : cegep-fxg.qc.ca
   Adresse IPv6 de liaison locale. . . . .: fe80::2d2f:ea73:4e31:3be4%10
   Adresse IPv4. . . . . . . . . . . . . .: 10.189.74.97
   Masque de sous-réseau. . . . . . . . . : 255.255.192.0
   Passerelle par défaut. . . . . . . . . : 10.189.64.1

```

Dans cet exemple, on voit que l'ordinateur possède deux interfaces réseau&nbsp;:

- `Ethernet` (filaire)
- `Wi-Fi` (sans fil)

Pour chaque interface, on peut voir&nbsp;:

- l'adresse IPv6
- l'adresse IPv4
- le masque de sous-réseau
- la passerelle par défaut

La commande `ipconfig /all` permet d'afficher plus d'informations sur les interfaces réseau.

```bash
C:\>ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : home
   Description. . . . . . . . . . . . . . : Realtek RTL8852BE WiFi 6 802.11ax PCIe Adapter
   Adresse physique . . . . . . . . . . . : CC-47-40-2E-22-D3
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::2d2f:ea73:4e31:3be4%11(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.2.10(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : mercredi 24 janvier 2024 12:38:03
   Bail expirant. . . . . . . . . . . . . : samedi 27 janvier 2024 12:38:05
   Passerelle par défaut. . . . . . . . . : 192.168.2.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.2.1
   IAID DHCPv6 . . . . . . . . . . . : 114050880
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2B-FC-0A-16-E0-73-E7-D3-0B-C1
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.2.1
                                       207.164.234.129
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : cegep-fxg.qc.ca
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : E0-73-E7-D3-0D-E2
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
```

`ipconfig /release` : libère la configuration IP d'une carte réseau.
`ipconfig /renew` : renouvelle la configuration IP d'une carte réseau.


Sous Windows vous pouvez également voir les cartes réseau en allant dans&nbsp;:
- `panneau de configuration / Centre Réseau et partage / Modifier les paramètres de la carte`.

![Configuration réseau](img/config_reseau.png){data-zoomable width=40%}

![Cartes réseau](img/cartes_reseau.png)

Un clic droit sur une carte réseau permet d'afficher les propriétés de la carte.
