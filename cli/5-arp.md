<!-- Cours 6 -->
## ARP

La commande `arp` (Address Resolution Protocol) permet de visualiser et de modifier la table ARP d'un équipement.

Sous Windows, la commande `arp -a` permet de voir le cache ARP.

Sous Linux et macOS, il faut utiliser la commande `arp -n`.

Exemple&nbsp;:

```bash
C:\>arp -a

Interface : 192.168.2.10 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.2.1           84-a1-ee-51-2b-d1     dynamique
  192.168.2.22          ea-9f-83-03-04-e2     dynamique
  192.168.2.104         ea-9f-83-03-04-e2     dynamique
  192.168.2.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.252           01-01-5a-00-01-fc     statique
```

- **Dynamique** signifie que l'entrée a été ajoutée automatiquement par le système. Les entrées dynamiques ont une durée de vie limitée, ou TTL (Time To Live). Après une certaine période, si l'entrée n'est pas réutilisée, elle expire et est supprimée de la table ARP pour libérer de l'espace et assurer que les informations sont à jour.
- **Statique** signifie que l'entrée a été ajoutée manuellement par l'utilisateur. Les entrées statiques ne sont pas supprimées automatiquement.

La commande `arp -s <adresse_ip> <adresse_mac>` permet d'ajouter une entrée statique dans la table ARP.

