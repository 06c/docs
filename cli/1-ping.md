<!-- Cours 1 -->
# ping

La commande `ping` permet de vérifier la connectivité entre deux équipements.

Exemple&nbsp;:

```bash
C:\>ping 192.168.1.2

Pinging 192.168.1.2 with 32 bytes of data:

Reply from 192.168.1.2: bytes=32 time<1ms TTL=128
Reply from 192.168.1.2: bytes=32 time<1ms TTL=128
Reply from 192.168.1.2: bytes=32 time<1ms TTL=128
Reply from 192.168.1.2: bytes=32 time<1ms TTL=128

Ping statistics for 192.168.1.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

```bash
C:\>ping wikipedia.org

Envoi d’une requête 'ping' sur wikipedia.org [208.80.154.224] avec 32 octets de données :
Réponse de 208.80.154.224 : octets=32 temps=20 ms TTL=53
Réponse de 208.80.154.224 : octets=32 temps=20 ms TTL=53
Réponse de 208.80.154.224 : octets=32 temps=20 ms TTL=53
Réponse de 208.80.154.224 : octets=32 temps=26 ms TTL=53

Statistiques Ping pour 208.80.154.224:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 20ms, Maximum = 26ms, Moyenne = 21ms
```


Sous Windows, cette commande envoie 4 paquets [`ICMP`](/protocoles/2-icmp) vers l'adresse IP de destination. 

Sous Linux et macOS, cette commande envoie les paquets jusqu'à ce que l'utilisateur l'arrête. Il faut utiliser la combinaison de touches `Ctrl + C` pour arrêter la commande.

[`ICMP`](/protocoles/2-icmp) (Internet Control Message Protocol) est un protocole qui permet de vérifier la connectivité entre deux équipements et de diagnostiquer les problèmes de réseau.

Si la commande `ping` fonctionne, cela signifie que les deux équipements sont connectés et que la configuration des adresses IP est correcte.

