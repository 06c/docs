<!-- cours 4 -->
# netstat

La commande `netstat` (network statistics)  peut lister toutes les **connexions réseau actives**, à la fois entrantes et sortantes, sur un ordinateur. Cela inclut les **adresses IP** et les **ports** utilisés par les connexions.

Exemple&nbsp;:

| Commande | Description |
|---------|-------------|
| netstat -a | Affiche toutes les connexions et tous les ports d'écoute |
| netstat -p tcp | Liste connexions TCP |
| netstat -p udp | Liste connexions UDP |
| netstat -r | Affiche le contenu de la table de routage |
| netstat -i |  Affiche le temps passé par une connexion TCP dans son état en cours |
| netstat -b | Affiche le nom du programme impliqué dans la création de chaque connexion (windows) |

Il est possible de combiner plusieurs options&nbsp;:

Exemple&nbsp;:

```bash
C:\>netstat -p tcp -b
```

Affiche les connexions TCP avec le nom du programme impliqué dans la création de chaque connexion.

```bash
Connexions actives
Proto  Adresse locale         Adresse distante       État
TCP    127.0.0.1:54010        DT08131:54011          ESTABLISHED
[firefox.exe]
TCP    127.0.0.1:54011        DT08131:54010          ESTABLISHED
[firefox.exe]
TCP    127.0.0.1:54012        DT08131:54013          ESTABLISHED
[firefox.exe]
TCP    127.0.0.1:54013        DT08131:54012          ESTABLISHED
[firefox.exe]
TCP    127.0.0.1:57112        DT08131:57111          ESTABLISHED
[adb.exe]
TCP    127.0.0.1:57119        DT08131:5037           ESTABLISHED
[studio64.exe]
TCP    192.168.2.10:22000     192.168.2.252:22000    ESTABLISHED
[syncthing.exe]
```

## Linux

Sous Linux, les commandes équivalentes sont `ss` (socket statistics) et `ip`.

Voici un tableau comparatif des commandes `netstat` et `ss`&nbsp;:

| netstat | ss |
|-----|-----|
|netstat -p tcp | ss -t |
|netstat -p udp | ss -u |
|netstat -r | ip route |
