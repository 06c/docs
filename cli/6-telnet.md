<!-- Cours 22 -->

# Telnet

## Introduction

Telnet est un protocole de communication qui permet d'accéder à distance à un serveur. Il est utilisé pour se connecter à un serveur distant et exécuter des commandes sur ce serveur. Telnet est un protocole de couche application qui utilise le protocole TCP pour établir une connexion entre le client et le serveur.

:::danger ⚠ Attention ⚠
Telnet transmet les données, y compris les noms d'utilisateur et les mots de passe, **en texte clair**, ce qui rend ces informations vulnérables à l'interception par des attaquants. Cela peut entraîner des risques de sécurité importants, surtout sur des réseaux ouverts ou sur Internet.

Malgré ses faiblesses, Telnet est parfois encore utilisé dans des environnements contrôlés, comme les réseaux internes qui ne sont pas exposés à Internet, pour des tâches spécifiques qui ne nécessitent pas de haute sécurité. Cela peut inclure le dépannage, la gestion de certains types d'équipements anciens ou spécialisés, ou les situations où les anciens systèmes ne supportent pas SSH.
:::

## Activation sous Windows

Sous Windows, Telnet est un client qui est installé par défaut. Pour l'activer, il suffit de suivre les étapes suivantes :

1. Ouvrir le **Panneau de configuration**.
2. Cliquer sur **Programmes**.
3. Cliquer sur **Activer ou désactiver des fonctionnalités Windows**.
4. Cocher la case **Client Telnet**.
5. Cliquer sur **OK**.

![Panneau de configuration](img/telnet1.png){data-zoomable width=50%}

![Activer ou désactiver des fonctionnalités Windows](img/telnet2.png){data-zoomable width=50%}

![Séléctionner Client Telnet](img/telnet3.png){data-zoomable width=50%}


## Utilisation de Telnet

Pour utiliser Telnet, il faut ouvrir une session Telnet en utilisant la commande `telnet` suivie de l'adresse IP du serveur et du port sur lequel le serveur écoute. Par exemple, pour se connecter à un serveur qui écoute sur le port 23, on utilise la commande suivante :

```bash
telnet <adresse IP du serveur> 23
```

Une fois la connexion établie, on peut exécuter des commandes sur le serveur à distance. Par exemple, pour afficher la liste des fichiers dans le répertoire courant, on peut utiliser la commande `ls` sous Linux ou `dir` sous Windows. Voici un exemple de session Telnet sous Linux:

```bash
ls
```

Pour quitter la session Telnet, on peut utiliser la commande `exit` :

```bash
exit
```


## Alternatives à Telnet

En raison de ces failles de sécurité, de nombreuses organisations et réseaux ont remplacé Telnet par des protocoles plus sécurisés tels que :

- **SSH (Secure Shell)** : SSH est le successeur le plus direct de Telnet pour l'accès à distance aux serveurs et aux équipements réseau. Il offre toutes les fonctionnalités de Telnet mais avec une couche de sécurité supplémentaire, car il crypte toute la communication entre le client et le serveur, empêchant ainsi l'écoute clandestine, le détournement de connexion et d'autres attaques.
- **SCP/SFTP** : Pour le transfert de fichiers, SCP (Secure Copy) et SFTP (SSH File Transfer Protocol) sont utilisés à la place de l'ancien FTP (File Transfer Protocol), qui, comme Telnet, n'offre pas de cryptage.

:::tip Pratique
## Connexion Telnet avec un routeur Cisco

Dans cet exemple, nous allons nous connecter à un routeur Cisco à l'aide de Telnet. Pour cela, nous allons utiliser le logiciel de simulation réseau Packet Tracer.

**1. Connexion au routeur :**

- Connectez-vous au routeur via une console ou une autre connexion initiale pour accéder au mode EXEC privilégié.

Entrer en mode de configuration :

```shell
Router> enable
Router# configure terminal
```

**2. Configurer le routeur avec une adresse IP sur l'interface :**

Supposons que nous configurons l'interface FastEthernet 0/0.

```shell
Router(config)# interface fastethernet0/0
Router(config-if)# ip address 192.168.1.1 255.255.255.0
Router(config-if)# no shutdown
Router(config-if)# exit
```

**3. Configurer un mot de passe pour Telnet :**

Définissez un mot de passe pour l'accès VTY (Virtual Terminal Lines).

```shell
Router(config)# line vty 0 4
Router(config-line)# password UnMotDePasse
Router(config-line)# login
Router(config-line)# exit
```

**4. Définir un mot de passe Enable pour l'accès privilégié :**

Ce mot de passe permet de sécuriser l'accès au mode EXEC privilégié.

```shell
Router(config)# enable secret motDePasseEnable
Router(config)# exit
```

**5. Sauvegarder la configuration :**

```shell
Router# copy running-config startup-config
```

ou

```shell
Router# write memory
```

(Les deux commandes sont équivalentes)

**6. Connexion Telnet à partir d'un PC**

Après avoir configuré le routeur, vous pouvez vous connecter à celui-ci en utilisant Telnet à partir d'un PC du réseau.

Le PC n'est pas forcément directement connecté au routeur, mais doit être sur le même réseau IP que l'interface du routeur que vous avez configurée (dans cet exemple, 192.168.1.1).


![Connexion Telnet à partir d'un PC](img/pc_telnet1.png){data-zoomable width=50%}

Sur le PC, séléctionner l'application **Command Prompt**.

- Démarrer une Session Telnet :

```shell
    telnet 192.168.1.1    
```

- Connexion et Authentification :
  - Lorsque vous êtes invité, entrez le mot de passe défini pour les connexions VTY (dans cet exemple, **UnMotDePasse**).
  - Une fois connecté, vous serez peut-être invité à entrer le mot de passe Enable pour accéder aux commandes privilégiées (si configuré).

![Connexion Telnet à partir d'un PC](img/pc_telnet2.png)

**7. Exemple de Commandes dans la Session Telnet**

Supposons que vous êtes connecté et souhaitez vérifier la configuration de l'interface :

```shell
Router> enable
(Entrez le mot de passe Enable, dans cet exemple => motDePasseEnable)
Router# show ip interface brief
```

Cette commande affichera un résumé des interfaces et de leur état, vous permettant de vérifier si l'interface FastEthernet 0/0 est correctement configurée et active.

**8. Déconnexion de Telnet**

Pour terminer votre session Telnet, tapez :

```shell
Router# logout
```
ou

```shell
Router# exit
```

:::

