<!-- Cours 9 -->
## HTTP

Le protocole HTTP (Hypertext Transfer Protocol) est un protocole de communication fondamental du World Wide Web. Développé par [Tim Berners-Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee) au CERN dans les années 1990, HTTP est devenu le moyen standard par lequel les navigateurs web demandent des pages web et envoient des informations aux serveurs web, et par lequel les serveurs répondent à ces demandes.

Sur le modèle OSI, HTTP fonctionne au niveau de la couche application (couche 7).

### Fonctionnement de HTTP

HTTP est un protocole orienté requête-réponse utilisé principalement pour le transfert d'informations sur Internet. Il fonctionne sur le modèle client-serveur, où un client HTTP (généralement un navigateur web) envoie une requête HTTP à un serveur, qui traite la requête et renvoie une réponse au client. Ce processus utilise le protocole TCP (Transmission Control Protocol) généralement sur le port 80 pour la communication non sécurisée (HTTP) et le port 443 pour la communication sécurisée (HTTPS, avec S pour Secure).

- **Requête** : Le client envoie une requête HTTP au serveur pour demander une ressource (page web, image, etc.).
- **Réponse** : Le serveur répond avec la ressource demandée, ou un code d'erreur si la ressource n'est pas disponible.

### Composants Clés de HTTP

- **URL** (Uniform Resource Locator) : L'adresse web que vous entrez dans votre navigateur pour accéder à une ressource spécifique sur Internet.
- **Méthodes HTTP** : Indiquent l'action à réaliser sur la ressource spécifiée.
- Codes de Statut : Réponses numériques du serveur indiquant le résultat de la requête. Les exemples incluent 200 (OK), 404 (Non Trouvé), et 500 (Erreur Interne du Serveur).
- **En-têtes** (Headers) : Fournissent des informations supplémentaires sur la requête ou la réponse, telles que le type de contenu, le type de navigateur, etc.
- **Corps** (Body) : La partie principale de la requête ou de la réponse, contenant les données envoyées ou reçues (comme le contenu d'une page web).

### Méthodes

Les requêtes HTTP peuvent utiliser différentes méthodes pour interagir avec les ressources. Les plus courantes sont :

- **GET** : Demande une représentation de la ressource spécifiée. Les requêtes GET ne doivent pas avoir d'effet sur les ressources.
- **POST** : Soumet des données à être traitées à la ressource spécifiée. Les données sont incluses dans le corps de la requête.
- **PUT** : Remplace toutes les représentations actuelles de la ressource cible par les données de la requête.
- **DELETE** : Supprime la ressource spécifiée.
- **HEAD** : Demande une réponse identique à une requête GET, mais sans le corps de la réponse.
- **OPTIONS** : Utilisé pour décrire les options de communication pour la ressource cible.

### Caractéristiques de HTTP

- Sans état (Stateless) : Chaque requête HTTP est indépendante; le serveur ne garde pas de trace des requêtes précédentes. Cela simplifie le design des serveurs mais nécessite des moyens comme les cookies pour maintenir l'état entre les requêtes.
- Extensible : HTTP permet d'ajouter des méthodes, des codes de statut, et des en-têtes pour étendre ses fonctionnalités.

### HTTPS

Avec l'augmentation des préoccupations en matière de sécurité et de confidentialité en ligne, HTTPS (HTTP Secure) est devenu essentiel. HTTPS ajoute une couche de chiffrement SSL/TLS à la communication HTTP, sécurisant ainsi les données échangées entre le client et le serveur.

### Inspecter les Requêtes HTTP

![Console du navigateur en mode réseau](img/http.png)

Dans la console du navigateur, on peut voir les requêtes HTTP effectuées par le navigateur pour charger une page web. 

On y voit:
- les requêtes GET / POST... pour les fichiers HTML, CSS, JavaScript, images, etc.
- les codes de statut 200 pour les fichiers trouvés, 404 pour les fichiers non trouvés, etc.

En cliquant sur une requête, on peut voir les en-têtes (headers) de la requête et de la réponse, ainsi que le contenu de la réponse (body).

![Console du navigateur en mode réseau](img/http2.png)

