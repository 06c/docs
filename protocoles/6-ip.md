<!-- Cours 10 -->
# Protocole IP

IP fournit une méthode standardisée pour adresser et router les paquets de données d'une source à une destination, souvent à travers plusieurs réseaux et dispositifs intermédiaires (comme des routeurs).

Dans le modèle OSI, IP est un protocole de la **couche réseau** (couche 3).

Le protocole IP permet d'**encapsuler** les données dans des paquets appelés **datagrammes IP**, qui sont ensuite transmis à travers le réseau. Chaque datagramme IP contient des informations sur l'adresse** IP de la source et de la destination**, ainsi que des données utiles à transmettre.


On peut voir dans les deux images ci-dessous des exemples de datagrammes IP :

![Datagramme IP](img/ip1.png)

Ce datagramme IP est émit par `192.168.1.10` et est destiné à `192.168.1.100`. Il contient des données ICMP. Cela est visible dans le champ `PRO: 0x01` qui indique que le protocole de couche supérieure utilisé est ICMP.

Voir la liste des protocoles sur [Wikipedia](https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers).

![Datagramme IP](img/ip2.png)

Ce datagramme IP a la même adresse source mais la destination est `255.255.255.255`, cela signifie qu'il est diffusé à tous les appareils du réseau.

Il contient des données UDP. Cela est visible dans le champ `PRO: 0x11` qui indique que le protocole de couche supérieure utilisé est UDP.

On observe également d'autres informations comme le TTL (Time To Live) qui indique combien de sauts (routeurs) le paquet peut effectuer avant d'être éliminé, la version du protocole IP utilisé `VER: 4` pour IPv4 et `VER: 6` pour IPv6, la longueur du datagramme...


### Fonctionnalités clés du protocole IP :

- **Adressage :** IP utilise des adresses uniques pour identifier les appareils sur un réseau.
- **Routage :** Le protocole IP détermine le chemin le plus approprié pour les paquets à travers différents réseaux jusqu'à leur destination finale, en utilisant des tables de routage maintenues par des routeurs.
- **Sans connexion et non fiable :** IP est un protocole sans connexion, ce qui signifie qu'il n'établit pas de connexion fixe entre la source et la destination avant de transférer les données. Il est également non fiable dans le sens où il ne garantit pas la livraison des paquets, leur ordre, ni leur intégrité ; ces aspects sont gérés par des protocoles de couche supérieure comme TCP.

### IPv4 vs IPv6 :

- **IPv4 :**(Internet Protocol version 4) est la première version du protocole IP. Elle utilise des adresses de 32 bits (comme 192.168.1.1), ce qui permet de définir environ 4,3 milliards d'adresses uniques. Cependant, en raison de la croissance rapide d'Internet, ces adresses sont devenues insuffisantes.
- **IPv6 :** IPv6 (Internet Protocol version 6) est la dernière version du protocole IP. Elle utilise des adresses de 128 bits, ce qui permet de définir un nombre pratiquement illimité d'adresses uniques. IPv6 a été conçu pour remplacer IPv4 et résoudre le problème de pénurie d'adresses.

## Routage IP

Le routage IP est le processus de transfert de paquets de données d'un réseau à un autre. Il est basé sur des tables de routage qui indiquent comment les paquets doivent être acheminés à travers le réseau. Le routage IP est réalisé par des **routeurs**, qui sont des appareils spécialisés conçus pour transférer les paquets de données entre les réseaux.




