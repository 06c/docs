<!-- Cours 1 -->

## ICMP (Internet Control Message Protocol)

`ICMP` (Internet Control Message Protocol) est un protocole qui permet de vérifier la connectivité entre deux équipements et de diagnostiquer les problèmes de réseau.

Il est utilisé par la commande [`ping`](/cli/1-ping).

ICMP est un protocole de la couche réseau dans le modèle OSI et TCP/IP. Il fonctionne au niveau de la couche IP.

https://fr.wikipedia.org/wiki/Internet_Control_Message_Protocol


