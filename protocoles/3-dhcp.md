<!-- Cours 5 -->
## DHCP

Le protocole DHCP (Dynamic Host Configuration Protocol) est un protocole de réseau standard utilisé sur les réseaux IP (Internet Protocol) pour attribuer automatiquement une adresse IP temporaire à un appareil sur le réseau. 

Cela simplifie considérablement la gestion des adresses IP, surtout dans les grands réseaux où l'attribution manuelle serait fastidieuse et sujette aux erreurs. 

<!-- ajout au Cours 7 -->
[Voir le cours sur DHCP](/cours/12-adressages.html)