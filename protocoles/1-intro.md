<!-- Cours 1 -->
# Protocoles

Les protocoles sont des règles qui permettent à des équipements de communiquer entre eux.

https://fr.wikipedia.org/wiki/Protocole_r%C3%A9seau

Pour que les machines puissent communiquer entre elles, elles doivent parler une langue commune. Cette langue commune est appelée un **protocole**. 

La plupart des "langues" utilisées par les machines sur Internet sont définies de manière précise dans des documents publics ([RFC](https://fr.wikipedia.org/wiki/Request_for_commentshttps://fr.wikipedia.org/wiki/Request_for_comments) - Request For Comments) et standardisés par l'[IETF](https://fr.wikipedia.org/wiki/Internet_Engineering_Task_Force) (Internet Engineering Task Force).

Cela permet aux réseaux, aux ordinateurs et aux logiciels divers de collaborer efficacement, à condition qu'ils respectent ces normes. C'est ce que l'on entend par le terme "interopérabilité".

Différents protocoles sont conçus pour répondre à des besoins spécifiques tels que le téléchargement d'un fichier, l'envoi d'un courriel, la consultation d'un site web, et autres...