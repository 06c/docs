<!-- Cours 10 -->

<script setup>
import QuizComponent from './components/QuizzTcpUdpComponent.vue'
</script>


# TCP / UDP

TCP et UDP s’exécutent au-dessus de IP et se fonde sur les services fournis par ce dernier.

Protocoles utilisés dans la couche **Transport** (**couche 4** du modèle OSI).

Rappel des couches du modèle OSI et TCP/IP :

![modèle OSI et TCP/IP ](img/osi-tcpip.png)

Avec TCP ou UDP, il est possible de remettre des données à des processus d’application s’exécutant sur une machine distante. 
Ces processus d’application sont identifiés par **numéros de port**. 

Une [socket](https://fr.wikipedia.org/wiki/Socket) est un point de communication par lequel un processus peut émettre et recevoir des informations.

C’est la combinaison d’une **adresse IP** et d’un **numéro de port**.

La combinaison de 2 sockets définit complètement une connexion TCP ou un échange UDP.

Exemple de connexion :
- 199.21.32.26:**1400** avec 196.62.132.11:**80**

![Connexion tcp udp](img/connexion-tcp-udp.png)

## TCP (Transmission Control Protocol)

- Assure un service de transmission de  données fiables avec une détection et  une correction d’erreurs de bout en bout.
- Orienté connexion.
- Fiabilité.
- Division des messages en segments
- Assemblage des messages au poste de destination.
- Renvoi de tout message non reçu.
- Assemblage des messages à partir des segments entrants.

![Datagramme TCP](img/tcp.png)

## UDP (User Datagram Protocol)

- Protocole non fiable ou de « best effort ».
- Offre un service de transmission de datagrammes sans connexion.
- Sans confirmation.
- Transmission de messages (appelés datagrammes).
- Absence de vérification pour la livraison des messages.
- Aucun réassemblage des messages entrants.
- Sans accusés de réception.
- Aucun contrôle du flux.
- Nécessite moins de bande passante que TCP. Il peut donc être intéressant d’utiliser ce protocole pour l’envoi de messages ne nécessitant pas de contrôle de qualité.

![Datagramme UDP](img/udp.png)

## Comparaison TCP / UDP

| Caractéristique | TCP | UDP |
| --- | --- | --- |
| Type| Avec connexion. Établi une connexion en 3 étapes avant d’envoyer les données. | Sans connexion. |
| Fiabilité | Oui | Non |
| Contrôle de flux | Oui | Non |
| ACK | Système d’accusé de réception (ACK) en retransmission des données si problème. | Seulement un léger contrôle des erreurs par « checksum ».Pas de récupération des données perdues |
| Ordre des messages | Oui | Non |
| Taille des messages | Illimitée | Limitée |
| Vitesse | Plus lent | Plus rapide |
| Utilisation | Web (HTTP), email (POP, SMTP, IMAP), transfert de fichiers (FTP) | Streaming vidéo / audio, jeux, téléphonie |

## Port

Afin que plusieurs communications puissent circuler en même temps, TCP et UDP utilisent des numéros de ports.

Un port est un numéro de 16 bits (0 à 65535) qui identifie un processus sur une machine.


Des conventions ont été établies pour des applications.

Liste des ports logiciels.

- Les ports de **0 à 1023** sont appelés **ports bien connus**. Ils sont réservés pour les services les plus courants (HTTP, FTP, SSH, etc.).

- **1023 à 65535** → attribué aux entreprises pour les applications commerciales et utilisé par le système d’exploitation pour l’attribution dynamique des ports source.

### Exemples de ports

- **HTTP** utilise le port **80**.
- **HTTPS** utilise le port **443**.
- **SSH** utilise le port **22**.
- **FTP** utilise le port **21**.
- **SMTP** utilise le port **25**.
- **POP3** utilise le port **110**.
- **IMAP** utilise le port **143**.

![Port](img/port.png){data-zoomable width=40%}

![Quelques ports](img/qques-ports.png)

::: tip Rappel

La commande [netstat](/cli/4-netstat.html) permet de visualiser les ports et les adresses IP des connexions distantes.

![netstat](img/netstat.png)
:::

## Établissement de connexion

### TCP

Les hôtes TCP établissent une connexion en 3 étapes, appelée aussi **handshake** (« poignée de main »).

- **3-way handshake** (établissement de la connexion) :
  - **SYN** (synchronisation) : demande de connexion.
  - **SYN-ACK** : acquiescement de la demande de connexion.
  - **ACK** : acquiescement de l’acquiescement.

![Handshake](img/handshake.png){data-zoomable width=30%}

### UDP

UDP n’établit pas de connexion.

## Qualité de service

### TCP

- **Contrôle de flux** : évite la congestion du réseau.
- **Contrôle de congestion** : évite la perte de paquets.
- **Contrôle d’erreur** : vérifie l’intégrité des données.

### UDP

- **Aucun contrôle de flux**.
- **Aucun contrôle de congestion**.
- **Aucun contrôle d’erreur**.


## Fiabilité
- L’utilisation d’un mécanisme appelé PAR (Positive Acknowlegment with Retransmission, Accusé de réception positif avec la retransmission) permet à TCP de garantir des transmissions fiables.
- Un système utilisant PAR envoie à nouveau les données si l’accusé de réception n’est pas reçu dans un délai spécifié.

![Contrôle d'erreur](img/ctrl_erreur.png){data-zoomable width=60%}


## Fenêtre de transmission

La **fenêtre de transmission** est la quantité de données que l’émetteur peut envoyer avant de recevoir un accusé de réception.

Le **Fenêtrage** est un mécanisme dans lequel le récepteur envoi un accusé de réception (acknowledgment  ACK) après avoir reçu un certain nombre de données.

Si le destinataire n’envoie pas d’accusé, cela signifie pour l’émetteur que les informations ne sont pas parvenues correctement et dans ce cas sont retransmises.


![Fenêtre de transmission](img/fenetre_transmission.png){data-zoomable width=50%}


La taille de la fenêtre détermine la quantité de données que l’on peut transmettre avant de recevoir un
accusé de réception.

![Datagramme TCP](img/datagramme_TCP.png)

## Comparaison des entre TCP - IP - TCP/IP

- **TCP/IP** est une combinaison de deux protocoles distincts, soit TCP et IP.
- Le protocole **IP** est un protocole de **couche 3**, un service sans confirmation qui offre la remise au mieux au sein d'un réseau.
- **TCP** est un protocole de **couche 4**, un service orienté connexion qui assure le contrôle du flux ainsi que la fiabilité.
- Le mariage de ces protocoles permet d'offrir une plus vaste gamme de services. Ensemble, ils offrent une suite complète.

::: tip Le protocole TCP/IP
 est le protocole de réseau le plus utilisé dans le monde.
:::

## Quiz 🎉 

<QuizComponent />