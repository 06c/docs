<!-- Cours 6 -->
## ARP

Le protocole ARP (Address Resolution Protocol) est un protocole fondamental utilisé dans les réseaux IP pour la résolution d'adresses. Son rôle principal est de faire correspondre une adresse IP, qui identifie un appareil sur un réseau au niveau de la couche **Internet**, à son adresse MAC (Media Access Control), qui fonctionne au niveau de la couche de **liaison de données**. 

Sur le modèle OSI, ARP fonctionne au niveau de la couche liaison de données (couche 2).

Lorsqu'un appareil sur un réseau IP souhaite communiquer avec un autre appareil, il a besoin de connaître son adresse MAC. Si seul l'adresse IP de la destination est connue, l'appareil source utilise ARP pour découvrir l'adresse MAC correspondante. Le processus se déroule en plusieurs étapes :

- **Requête ARP** : L'appareil source envoie une requête ARP sur le réseau, demandant l'adresse MAC correspondant à une adresse IP donnée. Cette requête est diffusée à tous les appareils sur le réseau local (multidiffusion).
- **Réponse ARP** : L'appareil qui possède l'adresse IP demandée reçoit la requête ARP et répond en envoyant son adresse MAC à l'appareil source.
- **Mise à Jour du Cache ARP** : À la réception de la réponse ARP, l'appareil source met à jour son cache ARP, qui est une table de correspondance entre les adresses IP et les adresses MAC, avec les informations reçues. Cela permet de réduire la nécessité de futures requêtes ARP pour la même adresse.


::: tip Exemple
L'animation suivante illustre le processus ARP. Le PC0 souhaite communiquer avec le PC1.

- Il envoie une requête ARP en multidiffusion (broadcast) pour connaître l'adresse MAC de PC1.
- PC1 répond avec son adresse MAC. PC0 met à jour son cache ARP avec l'adresse MAC de PC1.

![ARP](img/arp.gif)
:::

### Cache ARP

Sous Windows, la commande `arp -a` permet de voir le cache ARP.

Sous Linux et macOS, il faut utiliser la commande `arp -n`.

Voir commande [`arp`](/cli/5-arp).

