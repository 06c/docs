<!-- Cours 12 -->
# FTP

Le protocole FTP (File Transfer Protocol) est un protocole de transfert de fichiers. Il est utilisé pour transférer des fichiers entre un client et un serveur. Le client envoie des commandes au serveur pour lui demander de lister les fichiers, d'en télécharger ou d'en envoyer. Le serveur répond à ces commandes en envoyant des messages et des fichiers.

Dans le modèle OSI, FTP fonctionne dans la couche **application** (**couche 7**).

## Fonctionnement

Le protocole FTP utilise deux canaux de communication :

- **Canal de commande** : Utilisé pour envoyer des commandes et recevoir des réponses. Il utilise le port 21.
- **Canal de données** : Utilisé pour transférer les fichiers. Il utilise un port dynamique.

Le client se connecte au serveur en utilisant le port 21. Une fois connecté, le client peut envoyer des commandes pour lister les fichiers, en télécharger ou en envoyer. Le serveur répond à ces commandes en envoyant des messages et des fichiers.

## Sécurité

Le protocole FTP n'est pas sécurisé car il transmet les données en clair. Pour sécuriser les transferts de fichiers, il est recommandé d'utiliser FTPS (FTP sécurisé) ou SFTP (SSH File Transfer Protocol).

FTPS utilise SSL/TLS pour chiffrer les données et authentifier le serveur et le client. SFTP utilise SSH pour chiffrer les données et authentifier le serveur et le client.

## FileZilla

FileZilla est un client FTP open-source qui permet de se connecter à des serveurs FTP pour transférer des fichiers. Il est disponible pour Windows, Mac et Linux.

Télécharger [FileZilla](https://filezilla-project.org/).

## Langage de programmation et FTP

Il est possible de programmer des scripts pour automatiser les transferts de fichiers avec FTP. Par exemple, en utilisant un langage de programmation comme Python, il est possible de se connecter à un serveur FTP, de lister les fichiers, d'en télécharger ou d'en envoyer.

Voici un exemple de script Python pour se connecter à un serveur FTP et récupérer un fichier :

```python
import ftplib

def telecharger_fichier(serveur_ftp, utilisateur, mot_de_passe, chemin_distant, nom_fichier_distant, chemin_local):
    try:
        # Connexion au serveur FTP
        with ftplib.FTP(serveur_ftp) as ftp:
            ftp.login(utilisateur, mot_de_passe)
            
            # Activer le mode passif si nécessaire, utile dans les situations où le client est derrière un pare-feu
            ftp.set_pasv(True) 
            # Changement du répertoire courant au chemin où se trouve le fichier
            ftp.cwd(chemin_distant)
            
            # Téléchargement du fichier spécifié
            with open(chemin_local, 'wb') as fichier_local:
                ftp.retrbinary(f'RETR {nom_fichier_distant}', fichier_local.write)
            
            print("Téléchargement réussi.")
    except ftplib.all_errors as e:
        print('Erreur FTP:', e)

# Exemple d'utilisation
serveur_ftp = 'adresse_du_serveur_ftp'
utilisateur = 'nom_utilisateur'
mot_de_passe = 'mot_de_passe'
chemin_distant = 'chemin/vers/le/dossier/sur/le/serveur'
nom_fichier_distant = 'nom_du_fichier_à_télécharger.ext'
chemin_local = 'chemin/vers/le/fichier/local/nom_du_fichier_à_enregistrer.ext'

telecharger_fichier(serveur_ftp, utilisateur, mot_de_passe, chemin_distant, nom_fichier_distant, chemin_local)

```


Il est bien sûr possible d'utiliser Node.js, PHP, Java, C#, etc. pour se connecter à un serveur FTP.