import { SearchPlugin } from "vitepress-plugin-search";
import { defineConfig } from 'vitepress'


var options = {
  previewLength: 62,
  buttonLabel: "Recherche",
  placeholder: "Recherche docs",
  language: "fr",
  allow: [],
  ignore: [],
};

// https://vitepress.vuejs.org/reference/site-config
export default defineConfig({
  head: [
    ['link', { rel: 'icon', href: '/docs/favicon.ico' }]
  ],
  markdown: {
    theme: 'github-dark', 
  },
  title: "420-06C-FX",
  description: "Réseaux informatiques",
  // outDir: '../public',
  base: "/docs/",
  ignoreDeadLinks: true,
  themeConfig: {
    // https://vitepress.vuejs.org/reference/default-theme-config
    aside: true,
    nav: [
      { text: 'Accueil', link: '/' },
      { text: 'Calendrier', link: '/calendrier' },
      // { text: 'Examples', link: '/markdown-examples' }
    ],
    sidebar: [
      {
        text: 'Cours',
        collapsed: false,
        items: [
          { text: 'Présentation', link: '/cours/1-intro' },
          { text: 'Vocabulaires', link: '/cours/2-vocabulaire' },
          { text: 'Cartes réseau', link: '/cours/3-cartes_reseau' },
          { text: 'Classification', link: '/cours/4-classification' }, // cours 2
          { text: 'Supports de transmission', link: '/cours/5-supports' }, // cours 3
          { text: 'Méthodes d\'accès', link: '/cours/6-methodes_acces.md' }, // cours 3
          { text: 'Diagramme de réseau', link: '/cours/7-diagramme.md' }, // cours 3
          { text: 'Modèle OSI', link: '/cours/8-modele_osi.md' }, // cours 4
          { text: 'Modèle TCP/IP', link: '/cours/9-modele_tcpip.md' }, // cours 5
          { text: 'Adressage IP', link: '/cours/10-adressage_ip.md' }, // cours 5
          { text: 'Sous réseautage', link: '/cours/11-sous_reseautage.md' }, // cours 6
          { text: 'Adressage IP statique et dynamique (DHCP)', link: '/cours/12-adressages.md' }, // cours 7
          { text: 'VLAN', link: '/cours/13-vlan.md' }, // cours 17
          { text: 'Routage', link: '/cours/14-routage.md' }, // cours 19
          { text: 'Dépannage', link: '/cours/15-depannage.md' }, // cours 22
          { text: 'DNS', link: '/cours/16-dns.md' }, // cours 23
          { text: 'ACL', link: '/cours/17-acl.md' }, // cours 24
          { text: 'NAT', link: '/cours/18-nat.md' }, // cours 26
          { text: 'DMZ', link: '/cours/19-dmz.md' }, // cours 26
        ],
      },
      {
        text: 'Sécurité',
        collapsed: false,
        items: [
          { text: 'Politique de sécurité', link: '/securite/1-politique.md' }, // cours 27
          { text: 'Pare-feu', link: '/securite/2-pare-feu.md' }, // cours 27
          { text: 'Proxy', link: '/securite/3-proxy.md' }, // cours 27
          { text: 'VPN', link: '/securite/4-vpn.md' }, // cours 27
          { text: 'Bug Bounty', link: '/securite/5-bugbounty.md' }, // cours 27
          { text: 'Sécurité des données', link: '/securite/6-securite_donnees.md' }, // cours 28
        ],
      },
      {
        text: 'Protocoles',
        collapsed: true,
        items: [
          { text: 'Introduction', link: '/protocoles/1-intro' }, // cours 1
          { text: 'ICMP', link: '/protocoles/2-icmp' }, //  cours 1
          { text: 'DHCP', link: '/protocoles/3-dhcp' }, // cours 5
          { text: 'ARP', link: '/protocoles/4-arp' }, // cours 6
          { text: 'HTTP', link: '/protocoles/5-http' }, // cours 9
          { text: 'IP', link: '/protocoles/6-ip' }, // cours 10
          { text: 'TCP/UDP', link: '/protocoles/7-tcp-udp' }, // cours 10
          { text: 'FTP', link: '/protocoles/8-ftp' }, // cours 12
        ],
      },
      {
        text: 'Périphériques réseau',
        collapsed: true,
        items: [
          { text: 'Concentrateur', link: '/peripheriques/1-hub' }, // cours 2
          { text: 'Commutateur', link: '/peripheriques/2-commutateur' }, // cours 2
          { text: 'Point d\'accès sans fil', link: '/peripheriques/3-point_acces_sans_fil.md' }, // cours 2
          { text: 'Routeur', link: 'peripheriques/4-routeur' }, // cours 4
        ],
      },
      {
        text: 'Packet Tracer',
        collapsed: true,
        items: [
          { text: 'Installation', link: '/packet_tracer/1-installation' },
          { text: 'Configuration', link: '/packet_tracer/2-configuration' },
          { text: 'Démonstration interface', link: '/packet_tracer/3-demo' },
          { text: 'Connexion 2 PC', link: '/packet_tracer/4-2pc' },
          { text: 'Connexion 3 PC', link: '/packet_tracer/5-3pc' }, // cours 2
          { text: 'Serveur', link: '/packet_tracer/6-serveur' }, // cours 7
          { text: 'ARP poisoning', link: '/packet_tracer/7-arp_poisoning' }, // cours 7
        ],
      },
      {
        text: 'Commandes en ligne',
        collapsed: true,
        items: [
          { text: 'ping', link: '/cli/1-ping' },
          { text: 'ipconfig', link: '/cli/2-ipconfig' },
          { text: 'tracert', link: '/cli/3-tracert' },
          { text: 'netstat', link: '/cli/4-netstat' },
          { text: 'arp', link: '/cli/5-arp' },
          { text: 'telnet', link: '/cli/6-telnet' }, // Cours 22
          { text: 'ssh', link: '/cli/7-ssh' }, // Cours 22
          { text: 'nslookup', link: '/cli/8-nslookup' }, // Cours 23
        ],
      },
      {
        text: 'Soutien technique',
        collapsed: true,
        items: [
          { text: 'Introduction', link: '/soutien/1-intro' }, // cours 4
          { text: 'GLPI', link: '/soutien/2-glpi' }, // cours 4
          { text: 'Méthodologie', link: '/soutien/3-methodologie' }, // cours 11
          { text: 'Sondage', link: '/soutien/4-satisfaction' }, // cours 12
        ],
      },
      { text: 'Windows Server',
        collapsed: true,
        items: [
          { text: 'Introduction', link: '/windows/1-intro' }, // cours 14
          { text: 'Active Directory', link: '/windows/2-ad' }, // cours 14
        ]
      }
    ],
    footer: {
      message: '.',
      copyright: ''
    },
    returnToTopLabel: 'Retour en haut',
    outlineTitle: 'Sur cette page',
    socialLinks: [
      // { icon: 'github', link: 'https://gitlab.com/aec3' }
    ]
  },
  vite: { plugins: [SearchPlugin(options)] }
})
