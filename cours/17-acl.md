<!-- Cours 24 -->

<script setup>
import QuizComponent from './components/QuizzAclComponent.vue'
</script>


# ACL

Un ACL (Access Control List) est une liste de règles qui contrôle le trafic réseau entrant et sortant sur un routeur ou un commutateur. Les ACL sont utilisés pour filtrer le trafic en fonction de divers critères, tels que les adresses IP source et destination, les ports TCP/UDP, les protocoles, etc.

Les ACL sont généralement utilisés pour sécuriser un réseau en limitant l'accès aux ressources réseau, en bloquant le trafic indésirable ou en autorisant uniquement le trafic autorisé. Ils peuvent également être utilisés pour limiter la bande passante, prioriser le trafic, ou pour d'autres tâches de gestion du trafic.

Ces ACL peuvent être appliquées en entrée (IN) ou en sortie (OUT) d'une interface de routeur, de manière à filtrer les paquets entrants et/ou sortants.

Les ACL permettent donc d'autoriser, d'interdire ou de limiter l'accès à certaines ressources.
Cela se configure grâce à une liste utilisant les mots clés&nbsp;:
- `PERMIT` pour autoriser,
- `DENY` pour interdire.


## Types d'ACL

Il existe deux types d'ACL principaux :

- **ACL standard** : Les ACL standard filtrent le trafic en fonction des adresses IP source uniquement. Ils sont généralement utilisés pour autoriser ou bloquer le trafic en fonction des adresses IP source.
- **ACL étendu** : Les ACL étendus filtrent le trafic en fonction de critères plus avancés, tels que les adresses IP source et destination, les ports TCP/UDP, les protocoles, etc. Ils offrent une plus grande flexibilité pour contrôler le trafic réseau.

## Principe de filtrage par access-list

![Principe de filtrage par access-list](img/acl1.png)

## Positionnement des access-lists

Les ACL sont positionnées au niveau des interfaces du routeur.

![Positionnement des access-lists](img/acl2.png)

::: danger Important
La philosophie CISCO consiste (en simplifiant)&nbsp;:
- à appliquer les **ACL standard** au plus près de **la destination**, puisqu'on ne peut filtrer que la source ;
- à appliquer au contraire les **ACL étendues** au plus près de la **source**, autant que possible, pour éviter notamment le traitement (routage) inutile de paquets par les routeurs.
:::

## Fonctionnement d'une access-list

À la réception d'un paquet, le routeur vérifie si l'interface est associée à une ACL en entrée. Si oui, alors le routeur teste les conditions une par une. **La première condition respectée arrête le processus**, d'où l'importance de l'ordre de déclaration des conditions.

Une access-list peut comporter plusieurs règles&nbsp;:
- Les paquets sont vérifiés en testant les règles dans l'ordre où elles ont été définies.
- Si le paquet vérifie le critère énoncé par la règle, l'action définie (`permit` = autorisation, `deny` = interdiction) est appliquée et on ne regarde pas les règles suivantes.
  - Sinon, on passe à la règle suivante.
- En fin de liste, si aucune règle ne peut s'appliquer au paquet, **c'est l'action deny qui est appliquée** (même si elle ne figure pas dans la liste, **elle est implicite**). Le paquet est rejeté et un message ICMP est envoyé «&nbsp;host unreachable&nbsp;»

Si le paquet est autorisé en entrée du routeur, le paquet est ensuite routé vers une autre interface pour sortir. Le routeur vérifie si une ACL en sortie est associée à l'interface. Si oui, alors il vérifie les conditions une par une. Si l'ACL permet cette adresse, alors le paquet est routé. Sinon, le paquet est rejeté et un message ICMP est envoyé «&nbsp;host unreachable&nbsp;».

![Fonctionnement d'une access-list](img/acl3.png)

## Une liste de conditions

Une ACL est en fait une liste ordonnée logiquement de `PERMIT` ou/et de `DENY`.

Dès qu'une condition est remplie, le processus est terminé.
- Il faut définir les ACL de la plus spécifique à la plus générale.
- Mettre les conditions plus probables avant celles moins fréquentes pour augmenter les performances.

Les critères sont définis sur les informations contenues dans les en-têtes IP, TCP ou UDP.


## Syntaxe des ACL standard

La syntaxe des ACL varie en fonction du type de périphérique réseau sur lequel elles sont configurées. Les exemples qui suivent sont basés sur les routeurs Cisco.

### ACL standard

- Autoriser ou refuser l'adresse IP source à accéder au réseau de destination.
- Elle s'applique le plus proche de la destination.

Commande de l'IOS&nbsp;:

```bash
access-list numéro {permit | deny | remark} {any | ip_address masque_inversé | host ip_address}
```

- `numéro` : numéro de 1 à 99 ou entre 1300 et 1999 (dépendant des modèles de routeurs).
- `ip_source` : adresse IP source.
- `masque_inversé` : indique les bits de l'adresse source qui doivent correspondre.

La source peut être indiquée sous 3 formes&nbsp;:

- `any` (n'importe quelle source)
- `<destination> <masque-inversé>` (un réseau ou une partie de réseau)
- `host <ip_address>` (un seul hôte désigné par son adresse IP)

Exemple&nbsp;:

```bash
Router1 (conf)# access-list 1 remark ceci est un commentaire
Router1 (conf)# access-list 1 permit 192.192.1.0 0.0.0.255
```

- Cette règle a le numéro 1
- On peut écrire un commentaire avec le mot clé remark
- Elle autorise (permit)
- Elle s'applique pour l'adresse de réseau IP 192.192.1.0
- Le masque inversé précise que cette règle autorise toutes les adresses du réseau `192.192.1.0 255.255.255.0`

::: tip Remarque importante sur le masque :
Pour les access-lists on parle de masque inversé ou générique (wildcard Mask) : les bits positionnés (à 1) ne sont pas ceux que l'on vérifie, mais au contraire ceux que l'on ne vérifie pas.

Dans la règle ci-dessus, on autorise tout le réseau 192.192.1.0 /24. La valeur du 4ème octet n'est pas vérifiée, mais seulement les 3 premiers octets.

Voici quelques exemples :

| Adresses      | WildCard Mask    | Correspondance    |
|---------------|------------------|-------------------|
| 132.10.54.41  | 0.0.0.0          | 132.10.54.41      |
| 132.10.54.41  | 0.0.0.255        | 132.10.54.X       |
| 132.10.54.41  | 0.0.255.255      | 132.10.X.X        |
| 132.10.54.41  | 0.255.255.255    | 132.X.X.X         |
| 132.10.54.41  | 255.255.255.255  | X.X.X.X (any)     |
:::


### Masque implicite

Le but est de réduire les écritures et de simplifier la configuration.
- À moins de définir un `permit any` à la fin de l'ACL, tous les paquets ne remplissant aucune condition seront rejetés (implicite `deny any`).
- Quand le mot symbolique `any` est utilisé, le masque `255.255.255.255` est implicite.
- Si aucun masque n'est spécifié, alors le masque `0.0.0.0` est assumé.

### Association de l'ACL à une interface

La commande `ip access-group` associe une ACL à une interface en particulier (mode interface)

Seulement une ACL par interface, par protocole et par direction est permise.

Activation d'une ACL sur une interface :

```bash	
ip access-group no_liste {in|out}
```

- `no_liste` : le numéro d'ACL à associer à l'interface.
- `in|out` : filtrage en entrée ou en sortie.

## Exemples ACL standard

**Exemple 1 :**

```bash
Router1(conf)# access-list 1 permit 192.192.1.0 0.0.0.255
Router1(conf)# interface fa0/1
Router1(conf-if)# ip access-group 1 out
```	
- Création de la règle numéro 1
- Autorisation des IP sources du réseau `192.192.1.0 /24`
- Interdiction de toutes les autres IP (implicite `deny any`)
- On se positionne sur l'interface `fa0/1`
- Association de l'ACL numéro 1 avec l'interface `fa0/1` en sortie


**Exemple 2 :**

![Exemple 2](img/acl4.png)


```bash
Router1(conf)# access-list 1 deny 132.208.135.100  
Router1(conf)# access-list 1 permit any

Router1(conf)# interface fa1/0
Router1(conf-if)# ip access-group 1 out
```	
- Création de la règle numéro 1
- Elle interdit l'IP `132.208.135.100`
- Elle autorise toutes les autres adresses IP

- On se positionne sur l'interface `fa1/0`
- Association de l'ACL numéro 1 avec l'interface `fa1/0` en sortie


**Exemple 3 :**
```bash	
access-list 1 deny 172.16.3.10 0.0.0.0 
# ou
access-list 1 deny host 172.16.3.10
```
- Refuse les paquets d'IP source 172.16.3.10
- Le masque signifie ici que tous les bits de l'adresse IP sont significatifs
- Dans ce cas, on peut remplacer par deny host IP

**Exemple 4 :**
```bash	
access-list 1 permit 0.0.0.0 255.255.255.255
# ou
access-list 1 permit any
```
- Tous les paquets IP sont autorisés
- Le masque 255.255.255.255 signifie qu'aucun bit n'est significatif
- Dans ce cas, on peut remplacer par permit any


## Annuler les ACL
Ajouter « no » devant les commandes pour annuler des ACL, voici quelques exemples :
```bash	
Router1(conf)# no access-list 1
Router1(conf)# no access-list 2
Router1(conf)# interface fa0/0
Router1(conf-if)# no ip access-group 2 out
Router1(conf-if)# exit
Router1(conf)# interface fa0/1
Router1(conf-if)# no ip access-group 1 out
```

## Visualiser les ACL

La commande `show access-lists` permet de visualiser les ACL déjà présentes sur le routeur. On observe également le nombre de fois que les règles ont concordées.

![Visualiser les ACL](img/acl5.png)

La commande `show running-config` permet de voir la configuration du routeur et on y trouve donc les informations concernant les ACL.

![Visualiser les ACL](img/acl6.png)

![Visualiser les ACL](img/acl7.png)

![Visualiser les ACL](img/acl8.png)

## Syntaxe des ACL étendues

- [Documentation](https://www.cisco.com/c/en/us/support/docs/security/ios-firewall/23602-confaccesslists.htm)

- Elle s’applique le plus proche de la source.
- Le filtrage peut se faire selon :
  - L’adresse source
  - L’adresse de destination
  - Le protocole (IP, TCP, UDP, ICMP…)
  - Le numéro de port pour TCP ou UDP
  - Une opération logique sur un numéro de port spécifique
    - `=` (eq)
    - `≠` (neq)
    - `<` (lt)
    - `>` (gt)
- Les options varient selon le protocole spécifié.
- Identifiée par un numéro de 100 à 199 et de 2000 à 2699.

Commande de l'IOS&nbsp;:

```bash
access-list numéro {permit | deny} protocole ip_source port_source ip_dest port_dest options
```
- `numéro` : numéro de 100 à 199 – 2000 à 2699.
- `protocole` : IP, TCP, UDP, ICMP…
- `ip_source` : adresse IP source + masque inversé
  - ou `host IP`
  - ou `any` (= tout)
- `port_source` : port de la source.
- `ip_dest` : adresse IP de destination + masque inversé
  - ou `host IP`
  - ou `any` (= tout)
- `port_dest` : port de la destination.



### Association de l'acl à une interface

Comme pour les ACL standards, il faut ensuite associer l’ACL sur une interface

```bash
ip access-group no_liste {in|out}
```

- `no_liste` : le numéro d’ACL à associer à l’interface.
- `in|out` : filtrage en entrée ou en sortie.

## Exemples ACL étendues

**Exemple 1 :**

```bash
R1(conf)# access-list 100 permit tcp any host 192.168.1.2 eq 80
R1(conf)# access-list 100 permit icmp 192.168.0.0 0.0.0.255 host 192.168.1.2
(implicit deny any any)
```

- Création de la règle numéro 100.
- Autorise le trafic TCP sur le port 80 de toutes les sources vers l’hôte `192.168.1.2`
- Autorise le réseau `192.168.0.0 /24` à envoyer des requêtes ICMP vers l’hôte `192.168.1.2` pour faire des ping par exemple.
- Tout le reste sera refusé avec la règle `deny any any` qui est appliquée par défaut.

**Exemple 2 :**

![Exemple 2](img/acl9.png)

On veut bloquer l’accès au serveur FTP (port 21) pour le réseau externe (Internet) mais le permettre pour tous les réseaux internes (classe B 172.16.0.0) sans bloquer le trafic normal (HTTP, SMTP, etc.)

```bash	
access-list 101 permit tcp 172.16.0.0 0.0.255.255 172.16.1.1 0.0.0.0 eq 21
access-list 101 deny tcp any 172.16.1.1 0.0.0.0 eq 21
access-list 101 permit ip any any
(implicit deny any any)
interface fast-ethernet 0/0
ip access-group 101 out
```	

- Création de la règle numéro 101.
- Autorise le trafic TCP du réseau `172.16.0.0 /16` vers l’IP `172.16.1.1` sur le port 21.
- Refuse tous les paquets TCP de toutes les sources (`any`) vers l’IP `172.16.1.1` sur le port 21.
- Autorise tous les paquets IP de toutes les sources vers toutes les destinations.
- Tout le reste sera refusé avec la règle `deny any any` qui est appliquée par défaut.
- Association de la règle 101 sur l’interface `fast-ethernet 0/0` en sortie.

**Exemple 3 :**

![Exemple 3](img/acl10.png)

Supposons que l’on veut rejeter tous les ping allant au serveur Web sauf ceux venant de la station de gestion.

```bash
access-list 150 permit icmp 172.16.1.1 0.0.0.0 172.16.2.1 0.0.0.0 echo
access-list 150 deny icmp any 172.16.2.1 0.0.0.0 echo
access-list 150 permit ip any any
interface fast-ethernet 0/0
ip access-group 150 out
```

- Création de la règle numéro 150.
- Autorise les paquets `ICMP` de source IP `172.16.1.1` vers la destination IP `172.16.2.1` (`echo` correspond au ping)
- Le masque `0.0.0.0` signifie ici que tous les bits de l'adresse IP sont significatifs
- Refuse les paquet `ICMP` de toutes les sources vers la destination IP `172.16.2.1`
- Autorise tous les paquets IP de toutes les sources vers toutes les destinations
- Association de la règle avec l’interface `fast-ethernet 0/0` en sortie

**Autres exemples**
```bash
access-list 101 deny ip any host 10.1.1.1
```
Refus des paquets IP à destination de la machine `10.1.1.1` et provenant de n’importe quelle source

```bash
access-list 101 deny tcp any gt 1023 host 10.1.1.1 eq 23
```
Refus de paquet TCP provenant d'un port > 1023 et à destination du port 23 de la machine d'IP `10.1.1.1`

```bash
access-list 101 deny tcp any host 10.1.1.1 eq 80
```
Refus des paquets TCP à destination du port 80 de la machine d'IP `10.1.1.1`

## Quiz 🎉

<QuizComponent />