<!-- Cours 5 -->
# Modèle TCP/IP

## Introduction

Le modèle TCP/IP est un ensemble de protocoles de communication qui forme le fondement d'Internet et, plus largement, du réseau informatique mondial. Développé initialement dans les années 1970 par le Department of Defense des États-Unis (DoD), il a été conçu pour assurer une communication fiable dans un réseau pouvant être sujet à des pertes de données.

**TCP/IP** désigne communément une architecture réseau, c’est un ensemble de protocoles.

Désigne en fait 2 protocoles étroitement liés : 
- un protocole de transport, TCP (Transmission Control Protocol), basé en couche « Transport ».
- un protocole réseau, IP (Internet Protocol). Il représente le protocole réseau le plus répandu. Il permet de découper l’information à transmettre en paquets, de les adresser, de les transporter indépendamment les uns des autres et de recomposer le message initial à l’arrivée. Il est basé sur la couche « Réseau ».

Architecture réseau en 4 couches dans laquelle les protocoles TCP et IP jouent un rôle prédominant, car ils en constituent l’implémentation la plus courante.

Par abus de langage, TCP/IP peut donc désigner deux choses : le modèle TCP/IP et la suite de deux protocoles TCP et IP.


![Modèle TCP/IP](img/tcpip.png)

Modèle OSI et TCP/IP

- Le modèle TCP/IP comporte 4 couches.

## Couche d'accès réseau

Cette couche, également connue sous le nom de couche de liaison, est responsable de la transmission physique des données sur le support matériel. 

- TCP/IP ne s’occupe pas de la couche Accès au réseau.
- Elle organise le flux et identifie physiquement les hôtes.
- Place le flux de données sur les supports physiques.
- Les commutateurs, cartes réseaux, connecteurs, câbles… font partie de cette couche.

## Couche Réseau ou Internet

Au cœur du modèle, cette couche s'occupe de l'acheminement des paquets de données à travers différents réseaux. Elle utilise l'adresse IP pour déterminer la façon dont les paquets sont dirigés d'un expéditeur à un destinataire.

- Permet de déterminer les meilleurs chemins à emprunter vers les réseaux en fonction des adresses.
- Les routeurs transfèrent le trafic IP qui leur est pas destiné.

Protocoles Clés :

- **IP** (Internet Protocol) : pour l'adressage et le routage des paquets.
- **ICMP** (Internet Control Message Protocol) : pour la gestion des erreurs et des messages de contrôle.
- **ARP** (Address Resolution Protocol) : pour la résolution des adresses IP en adresses MAC.

## Couche Transport

La couche Transport est responsable de la communication de bout en bout entre les applications. Elle est chargée de la segmentation et de l'assemblage des données, ainsi que de la gestion des connexions.

- Responsable du dialogue entre les terminaux.
- Les équipements de type pare-feu peuvent opérer un filtrage au niveau de la couche Transport.

Protocoles Clés :

- **TCP** (Transmission Control Protocol) : pour les applications nécessitant une communication fiable.
- **UDP** (User Datagram Protocol) : pour les applications nécessitant une communication rapide.

## Couche Application

- La couche Application est la couche la plus haute du modèle TCP/IP. 
- Elle est responsable de la communication entre les applications.
- Les protocoles de cette couche sont utilisés pour l'échange de données entre les programmes applicatifs. 
- S’exécute sur les machines hôtes.

Protocoles Clés :

- **HTTP** (Hypertext Transfer Protocol) : pour le web.
- **FTP** (File Transfer Protocol) : pour le transfert de fichiers.
- **SMTP** (Simple Mail Transfer Protocol) : pour l'envoi d'e-mails.
- **POP3** (Post Office Protocol version 3) : pour la récupération d'e-mails.
- **IMAP** (Internet Message Access Protocol) : pour l'accès aux messages électroniques.
