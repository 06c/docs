<!-- Cours 4 -->

<script setup>
import QuizComponent from './components/QuizzOsiComponent.vue'
</script>


# Modèle OSI

## Introduction

Le modèle OSI (Open Systems Interconnection) a été développé par l'Organisation Internationale de Normalisation (ISO) dans les années 1980. Il sert de guide universel pour la conception et la compréhension des réseaux informatiques. Le modèle divise le processus complexe de communication réseau en sept couches distinctes, chacune ayant un rôle spécifique.

Chaque couche a un rôle bien précis et **communique avec les couches adjacentes**.

Le modèle OSI est un modèle théorique qui permet de comprendre le fonctionnement des réseaux informatiques. Il permet de séparer les problèmes en sous-problèmes plus simples.

![Modèle OSI](img/osi.png)

7 couches divisées en deux parties :

**Couches supérieures** :
De la couche **4 à 7** – orientées application et plutôt réalisées par des bibliothèques ou un programme spécifique.

**Couches inférieures** :
De la couche **1 à 3** – orientées communication et sont souvent fournies par un système d'exploitation et par le matériel. 

![Modèle Osi](img/osi1.png)

::: tip &nbsp;
Sur ce schéma, on peut voir les différents **protocoles** ou **matériels** qui interviennent à chaque couche.
:::

## Avantages

- Réduit la complexité
- Permet de diviser les communications sur le réseau en éléments plus petits et plus simples.
- Empêche les changements apportés à une couche d'affecter les autres couches ce qui assure un développement plus rapide.
- Permet à différents types de matériel et de logiciel réseau de communiquer entre eux.
- Assure une parfaite compatibilité des différentes technologies.

## Inconvénients

- Le modèle OSI est un modèle théorique et n'est pas toujours utilisé dans la pratique.
- Il est souvent simplifié pour être plus facile à comprendre.
- Les protocoles réels ne correspondent pas toujours exactement à une seule couche du modèle OSI.

## Analogie avec le transport de courrier

![Analogie avec le transport de courrier](img/courrier.png)

Pour comprendre le modèle OSI, on peut le comparer au transport de courrier. Chaque couche du modèle OSI correspond à une étape du processus d'envoi d'une lettre.

## Encapsulation / Désencapsulation

Lorsqu'un message est envoyé d'un ordinateur à un autre, il traverse les différentes couches du modèle OSI. À chaque couche, des informations supplémentaires sont ajoutées au message. On parle d'**encapsulation**.

![Encapsulation](img/encapsulation.webp)

Lorsque le message arrive à destination, les couches du modèle OSI du destinataire retirent les informations ajoutées par les couches de l'expéditeur. On parle de **désencapsulation**.


## Couche 1 : Physique

- **Transmission des Bits**: La couche physique s'occupe de la transmission des bits (1 et 0) à travers le réseau. Elle ne traite pas le contenu des données, mais se concentre sur la façon dont elles sont physiquement envoyées et reçues.
- **Supports Physiques**: Elle utilise divers supports physiques pour la transmission des données, tels que les câbles en cuivre (Ethernet), la fibre optique, ou les ondes radio pour le WiFi.
- **Signaux et Modulation**: Convertit les données numériques en signaux électriques, optiques ou radio, selon le type de média utilisé.

Équipements Impliqués:

- **Câbles et Connecteurs**: Câbles Ethernet, coaxiaux, et les connecteurs associés.
- **Composants Réseau Physiques**: Concentrateurs (hub), répéteurs, modems, interfaces de réseau (NIC => Network Interface Controller, carte réseau).


## Couche 2 : Liaison de données

- Gérer les échanges de données entre les équipements connectés au même réseau local.
- **Adressage Physique**: La couche liaison de données utilise les **adresses MAC** (Media Access Control) pour identifier les périphériques sur le réseau.
- **Contrôle d'Accès au Support**: Elle gère l'accès au support physique (câble) pour éviter les collisions et les conflits.
- **Découpage des Trames**: Elle découpe les données en **trames** plus petites pour les envoyer sur le réseau.
- **Détection d'Erreurs**: Elle vérifie que les données reçues sont correctes et les corrige si nécessaire.

Équipements Impliqués:

- **Commutateurs (Switches)**: Ils utilisent les adresses MAC pour transférer les trames entre les périphériques.
- **Cartes Réseau**: Elles utilisent les adresses MAC pour communiquer avec d'autres périphériques.

(Les cartes d'interface réseau fonctionnent à la fois au niveau de la couche Physique et de la couche Liaison de données.)

Exemple de protocoles de la couche liaison de données: **Ethernet**, **Wi-Fi**

## Couche 3 : Réseau

- **Routage des Paquets**: La couche réseau est responsable du routage des paquets de données à travers le réseau. Elle détermine le meilleur chemin à emprunter entre le point A d'un réseau et le point B d'un autre réseau.
- **Adressage Logique**: Elle utilise les adresses IP pour identifier les périphériques sur le réseau et acheminer les données à leur destination
- **Interconnexion des Réseaux**: Elle permet de connecter des réseaux locaux entre eux pour former un réseau plus vaste.
- **Fragmentation et Réassemblage des Paquets**: Elle découpe les données en **paquets** pour les envoyer sur le réseau, puis les réassemble à destination.

Équipements Impliqués:

- **Routeurs**: Jouent un rôle crucial dans la couche réseau. Ils créent une frontière entre deux réseaux et utilisent les adresses IP pour acheminer les paquets de données.

Exemple de protocoles de la couche réseau: **IP**, **ICMP**, **ARP**, **RIP**

## Couche 4 : Transport

- **Contrôle de Flux**: Elle gère le contrôle de flux, soit la méthode permettant de fixer un taux de transmission approprié en fonction de la vitesse à laquelle le récepteur accepte les données.
- **Gestion des Ports** : La couche Transport utilise des **numéros de port** pour identifier spécifiquement les applications ou les processus de communication sur les hôtes. Ces ports permettent de distinguer les différents services ou applications qui utilisent le réseau simultanément. Chaque service ou application utilise un port spécifique (entre 0 et 65535).
- **Fiabilité de la Communication**: Elle s'assure que les données sont correctement reçues par le destinataire.
- **Multiplexage**: Elle permet d'envoyer plusieurs flux de données simultanément sur le même support physique.
- **Segmentation et Réassemblage des Données**: Elle découpe les données en **segments** pour les envoyer sur le réseau, puis les réassemble à destination.
- Les protocoles de la couche Transport se chargent aussi du contrôle de flux, soit la méthode permettant de fixer un taux de transmission approprié en fonction de la vitesse à laquelle le récepteur accepte les données.


Équipements Impliqués:

- **Firewalls**: Ils filtrent le trafic réseau en fonction de règles de sécurité définies.

Exemple de protocoles de la couche transport: **TCP**, **UDP**

::: tip Ports
Un **port** est un numéro de 16 bits (0 à 65535) qui identifie un service ou une application sur un hôte. Par exemple si votre navigateur à 3 onglets d'ouverts, chaque onglet utilise un port différent pour communiquer avec les serveurs web.

Si vous avez une autre application qui utilise le réseau, elle utilisera un autre port.

Voir la commande [`netstat`](/cli/4-netstat.html) pour voir les ports utilisés sur votre ordinateur.
:::




## Couche 5 : Session
- **Établissement, Gestion et Terminaison des Sessions**:La couche session permet d'ouvrir, de maintenir et de fermer les sessions de communication. Une session peut être vue comme un échange de données ou une conversation entre deux applications.
- **Synchronisation des Données**: Elle synchronise les données entre les applications (qui peut envoyer des données à quel moment) et la gestion des points de reprise pour récupérer les informations en cas d'interruption de la session


## Couche 6 : Présentation

La couche Présentation sert de *traducteur* entre l'application et le réseau.
Elle met en forme les informations de telle sorte qu’elles soient lisibles pour les applications logicielles.

Au niveau de cette couche, les données sont « formatées », mises en une forme pour que le réseau puisse « comprendre ». Ce format peut varier en fonction du type de réseau utilisé.

C'est la première couche non impliquée dans le mécanisme de transfert d'informations.

- **Encodage et Compression des Données**: Elle s'occupe de l'encodage et de la compression des données pour assurer leur compatibilité entre les différentes machines.
- **Chiffrement et Décryptage des Données**: Elle chiffre et déchiffre les données pour assurer leur confidentialité.
- **Conversion des Données**: Elle convertit les données dans un format compréhensible par l'application.

Exemple de protocoles de la couche présentation: **SSL**, **TLS**

## Couche 7 : Application
C’est le niveau le plus proche des utilisateurs.

Les API appartiennent à la couche Application du modèle OSI (une API est un ensemble d'instructions qui permet à un programme d'interagir avec le système d'exploitation).

- **Interface avec l'Utilisateur**: Elle fournit une interface pour les utilisateurs et les applications pour accéder au réseau.
- **Services Réseau**: Elle fournit des services réseau aux applications, tels que l'envoi et la réception de fichiers, la messagerie électronique, le partage de fichiers, etc.
- **Protocoles d'Application**: Elle utilise des protocoles d'application pour communiquer avec d'autres applications sur le réseau.

Exemple de protocoles de la couche application: **HTTPS**, **FTP**, **SMTP**, **POP3**, **IMAP**, **DNS**, **DHCP**, **SSH**

---
:::tip Info
Les couches 5, 6 et 7 sont souvent regroupés sous le terme **couche application**.

Elles permettent de gérer les échanges de données entre les applications et les services réseau.

Dans un autre modèle, le modèle TCP/IP (que nous étudierons plus tard), ces couches sont regroupées en une seule couche (couche application).
:::
---

## PDU

Chaque couche du modèle OSI utilise des **unités de données** spécifiques pour communiquer avec les couches adjacentes. Ces unités de données sont appelées **PDU** (Protocol Data Unit).

| Couche OSI     | Fonction                                          | Exemple        | **PDU**      |
|----------------|---------------------------------------------------|----------------|----------|
| Application    | Transfère les informations entre les programmes. | http           | **Données**  |
| Présentation   | Formatage de texte et conversion de code.        | .jpg           | **Données**  |
| Session        | Établit et entretient la communication.          | TLS            | **Données**  |
| Transport      | Assure la livraison fiable des données.          | TCP, port            | **Segment**  |
| Réseau         | Détermine voies de transport, gère le transfert des messages. | Routeur, IP            | **Paquet**   |
| Liaison        | Codifie, adresse et transmet les informations.   | Commutateur, MAC         | **Trame**    |
| Physique       | Gère les connexions matérielles.                 | Câble, répéteur| **Bits**     |


## Résumé

Le modèle OSI est un modèle théorique qui permet de comprendre le fonctionnement des réseaux informatiques. Il permet de séparer les problèmes en sous-problèmes plus simples.

**Un Guide, Pas une Règle**: Le modèle OSI est plus un **guide théorique** qu'une spécification stricte. De nombreux protocoles réseaux ne suivent pas exactement le modèle OSI mais s'en inspirent.

##  🎉 

<QuizComponent />
