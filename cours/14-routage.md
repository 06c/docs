<!-- Cours 19 -->

<script setup>
import QuizComponent from './components/QuizzRoutageComponent.vue'
</script>

# Le routage

## Introduction

Le routage est le processus qui consiste à acheminer des paquets de données d'un réseau à un autre. Il est réalisé par des équipements appelés **routeurs**. Le routage est une fonction essentielle des réseaux informatiques, car il permet de connecter des réseaux locaux entre eux pour former un réseau étendu.

- Pour qu’une machine soit en mesure d’en contacter **une autre sur un autre réseau**, il faut mettre en place un mécanisme qui décrit comment aller de l’une à l’autre.
- C’est ce que l’on appelle le **routage**.
- Si un paquet émis par une machine ne trouve pas sa destination dans le réseau local, il doit être redirigé vers un routeur qui rapproche le paquet de son objectif.
- Il faut que toutes les stations du réseau possèdent l’adresse du routeur par défaut. C'est ce qu'on appelle la **passerelle par défaut**.

Exemple de passerelle par défaut sur un ordinateur à la maison&nbsp;:

![Passerelle sur un ordinateur](img/passerelle.png){data-zoomable width=50%}

Exemple de passerelle configuré sur un ordinateur dans Packet Tracer&nbsp;:

![Passerelle dans Packet Tracer](img/passerelle_packet_tracer.png){data-zoomable width=50%}

Schéma d'un réseau avec des routeurs&nbsp;:

![Réseau avec routeurs](img/routeurs1.png){data-zoomable width=50%}

- Les routeurs sont nécessaires pour atteindre les hôtes et périphériques **qui ne sont pas dans le même réseau local**.
- Les routeurs utilisent une **table de routage** pour parcourir les réseaux.

![Réseau avec routeurs](img/routeurs2.png)


## Fonctions d’un routeur

- Les commutateurs (couche 2) permettent l’échange d’informations entre périphériques appartenant à un même domaine de collision, LAN ou VLAN, mais ils n’ont pas la capacité de relier deux réseaux entre eux. 
- Pour cela, il faut utiliser un routeur (couche 3).
- Les fonctions principales du routeur sont de déterminer la meilleure route pour une destination (déterminée par l’adresse IP de destination du paquet) et d’envoyer les informations dans la bonne direction, un peu comme un aiguillage ferroviaire. Pour cela, il s’appuiera sur les informations contenues dans sa table de routage.
- Un routeur a au minimum 2 ports réseau pour connecter 2 réseaux ensemble. Il peut y avoir plusieurs dizaines de ports pour les routeurs d’entreprise.

### Les deux processus de base d'un routeur&nbsp;: 

- Déterminer le chemin grâce à sa table de routage.
- Routage des paquets&nbsp;:

## Le routeur, un ordinateur spécialisé

![Icône du routeur](img/icon_routeur.png){data-zoomable width=20%}

- Un routeur est un ordinateur spécialisé dans le routage des paquets de données entre les réseaux.
- Il est équipé de plusieurs interfaces réseau pour se connecter à différents réseaux.
- Il utilise des **protocoles de routage** pour échanger des informations avec d'autres routeurs et mettre à jour sa table de routage.
- Il peut également fournir des services de sécurité, de filtrage et de traduction d'adresses réseau.

## Connecter le routeur à son environnement

Comme pour le commutateur, pour pouvoir se connecter à un routeur Cisco, il faut disposer :
- D’un câble console (rollover) câble bleu reliant le port série du PC au port console du commutateur.
- D’un port série ou d’un adaptateur USB/série.
- D’un émulateur de terminal : logiciel permettant l’affichage de la console de commande du commutateur (PuTTY, HyperTerminal…).

![Illustration d'un routeur](img/routeur3.png)

![Câbles routeur](img/routeur_cables.png)

## Routage IP basé sur l'adresse du destinataire

Chaque équipement du réseau local&nbsp;:
- sait atteindre un autre équipement du même réseau : ARP (Address Resolution Protocol) [voir le protocole ARP](/protocoles/4-arp).

![Table ARP d'un PC](img/table_arp.png){data-zoomable width=40%}

- sait atteindre un équipement d'un autre réseau, s'il existe au moins **un équipement de routage** pour acheminer les datagrammes à l'extérieur du réseau local.

:::tip Rappel
Rappel : Lorsqu’une machine désire communiquer avec une autre machine, elle compare l’ip de son réseau local (en faisant un ET logique entre son adresse IP et son masque de sous-réseau) avec le sous-réseau de la cible (en faisant un ET logique entre l’adresse IP cible et son masque de sous-réseau).
- Si les deux résultats sont identiques, la machine cible se trouve dans le même réseau et la machine source transmet alors ses données directement à la machine cible.
- Si les deux résultats sont différents, la machine cible se trouve dans un autre réseau et la machine source transmet alors ses données à sa passerelle (routeur) qui se chargera de les transmettre à la machine cible.

![Routage adresse IP](img/routage_ip.png)

:::

### Exemple de routage IP

![Routage adresse IP](img/routage_ip2.png){data-zoomable width=60%}

2 réseaux différents :
- 192.168.1.0 /24
- 192.168.2.0 /24

Ce routeur a 2 interfaces :
- Fa0/0
- Fa0/1


## Détermination de la route

- La détermination de la voie, dans le cas du trafic acheminé sur un réseau, se produit à la couche réseau (couche 3).
- La fonction de détermination de voie permet à un routeur d'évaluer les voies se rendant à une destination et d'établir la voie à privilégier dans le cas d'un paquet grâce à sa table de routage.
- Une fois que le routeur a déterminé la voie à utiliser, il achemine le paquet. Il prend le paquet qu'il a accepté par une interface et le transmet à une autre interface (un port) qui reflète le plus court chemin permettant au paquet d'atteindre sa destination.

![Détermination de la route](img/route_determination.png){data-zoomable width=40%}

## Table de routage

La table de routage est une table qui contient des informations sur les réseaux accessibles par le routeur et les interfaces à utiliser pour les atteindre.
- Utilisée par le routeur pour déterminer la meilleure route pour acheminer les paquets de données.
- Mise à jour automatiquement par le routeur à l'aide de protocoles de routage.
- Peut être configurée manuellement par l'administrateur réseau.

![Exemple de routage](img/routage_exemple.png)

![Table de routage](img/table_routage.png)

La commande `show ip route` permet d'afficher la table de routage d'un routeur Cisco.

![Table de routage](img/table_routage1.png)

## Protocoles routés et protocoles de routage

À ne pas confondre&nbsp;:

### Protocole routé

Les protocoles routés sont des protocoles qui peuvent être utilisés pour envoyer des données (sous forme de paquets) à travers un réseau. Ils définissent la structure des paquets et fournissent les adresses nécessaires pour que les paquets soient acheminés du point d'origine au point de destination. Les protocoles routés sont responsables du transport des données sur le réseau.

Exemples :

- **IP** (Internet Protocol) : Le protocole le plus courant sur Internet, utilisé pour l'adressage et le routage des paquets entre les hôtes.
- **IPv6** (Internet Protocol version 6) : La version la plus récente d'IP, conçue pour remplacer IPv4 et corriger le problème de l'épuisement des adresses IP.

### Protocole de routage

Les protocoles de routage sont utilisés par les routeurs pour communiquer entre eux et échanger des informations sur les chemins d'accès disponibles. Le but des protocoles de routage est de construire et de maintenir des **tables de routage** qui guident le routage des paquets à travers le réseau. Ces protocoles permettent au réseau de réagir dynamiquement aux changements de topologie, comme l'ajout de nouveaux routeurs ou la perte de liaisons existantes.

Exemples :

- **OSPF** (Open Shortest Path First) : Un protocole de routage de type état de lien qui calcule le chemin le plus court entre les nœuds.
- **BGP** (Border Gateway Protocol) : Utilisé pour le routage entre les systèmes autonomes sur Internet.
- **EIGRP** (Enhanced Interior Gateway Routing Protocol) : Un protocole de routage propriétaire de Cisco, considéré comme un protocole de routage à vecteur de distance avancé.
- **RIP** (Routing Information Protocol) : Un des plus anciens protocoles de routage, utilisant l'algorithme de vecteur de distance.

## Routage statique

Pour le routage statique, la table est établie par l’administrateur.
- C’est simple…
- mais utilisé pour des petits réseaux locaux

::: tip Routes par défaut
Les routes par défaut constituent un type particulier de routes statiques.

Une route par défaut est une entrée de table de routage qui dirige les paquets au saut suivant, lorsque ce saut n'est pas explicitement listé dans la table de routage.

Cela signifie que tout paquet qui atteint un routeur frontière peut emprunter la route par défaut qui mène à Internet dans l'espoir qu'un routeur quelconque aura une entrée de table de routage relative à son adresse de destination.
:::



## Protocoles de routage dynamique

Pour le routage dynamique, la table est mise à jour périodiquement à l’aide de protocoles spécifiques. Les routeurs envoient régulièrement la liste des réseaux ou des sous-réseaux que l’on peut atteindre par eux. Ce qui permet aux autres routeurs de mettre à jour leurs tables de routage. Ils évaluent dynamiquement la meilleure route vers chaque réseau.

Trois types d’algorithmes de routage dynamiques existent :
- Algorithmes à **vecteurs de distance** (protocoles RIP, IGRP)
- Algorithmes à **état de lien** (protocoles OSPF…)
- Hybride (EIGRP) – protocole Cisco combine des aspects des algorithmes d'état de liens et de vecteur de distance?

Nous utiliserons le protocole **RIP** dans le cadre des travaux avec Packet Tracer


### Protocole à vecteur de distances
Les protocoles de routage à vecteur de distances construisent eux-mêmes les tables de routages. Par contre, aucun routeur ne possède la vision globale du réseau, car la diffusion des routes se fait uniquement de proche en proche.

Dans le terme « vecteur de distances », on peut associer le mot « Vecteur », comme un tableau qui comprend les autres nœuds du réseau. Et le mot « distance » correspond au nombre de sauts qui permet d'atteindre les routeurs voisins.


### Protocole à état de liens

Dans un protocole de routage à état de liens, chaque routeur connait entièrement la topologie du réseau, c’est-à-dire que les routeurs ne se fient pas qu’à leurs voisins !

Les routeurs qui exécutent des protocoles de routage échangent des messages afin de conserver leurs tables de routage à jour. 

Lorsqu'un routeur se rend compte qu’il y’a eu une modification dans le réseau, ou bien un nouvel itinéraire, alors, il en informera ses voisins qui font partit du même protocole de routage.

De cette manière, tous les routeurs maintiennent dynamiquement leurs tables de routage à jour.

## Métriques

La métrique est une valeur qui représente le coût d'une route. Elle est utilisée par les protocoles de routage pour déterminer la meilleure route vers une destination.

- Les protocoles de routage utilisent des métriques pour évaluer la qualité des routes et choisir la meilleure route vers une destination.
- Les métriques peuvent être basées sur des critères tels que la distance, la bande passante, le délai, le coût, etc.
- Les protocoles de routage utilisent des algorithmes pour calculer les métriques et choisir la meilleure route.

- La **bande passante** : c’est le débit du lien entre routeurs
- Le **délai** : c’est la durée que met le paquet IP à se déplacer d’un point à un autre
- **Coût** : qui est une valeur prédéfinie, et qu’un admin réseau peut modifier manuellement s’il le souhaite.
- Le **nombre de sauts** : qui est le nombre de routeurs qu’un paquet IP doit parcourir avant d'arriver à sa destination.


## Exemples

### Exemple de **routage statique**.

![Route statique](img/route_statique.png)

Voir [cette page](https://www.formip.com/pages/blog/configuration-route-statique/) pour voir la procédure.

### Exemple de **routage dynamique**.

![Route dynamiquw](img/route_dynamique.png)

Voir [cette page](https://www.formip.com/pages/blog/rip-configuration-du-protocole-de-routage) pour la mise en place du protocole RIP

## Exercice

- Avec Packet Tracer, créer le réseau ci-dessous et mettre en place les routes **statiques** dans un premier temps.
- Ensuite supprimer les routes statiques et configurer les routes **dynamiques** avec le protocole RIP.
- Utiliser les routeurs 2621XM.
- Pour le routeur 1, il faut ajouter une interface de type NM1E afin d’avoir une 3ème interface.

![Exercice routage](img/routage_exercice.png)

## Quiz 🎉 

<QuizComponent />

## Sources

- [https://formip.com](https://formip.com/)
- Transmissions et réseaux de Stéphane Lohier – ISBN 978-2-10-055561-1
- Cours de Lison Sergerie
