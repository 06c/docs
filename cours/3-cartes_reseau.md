<!-- Cours 1 -->
# Carte réseau et adresse MAC

Équipement de couche 2 du modèle OSI.

Tout les appareils relié à un réseau partage un élément essentiel : ils doivent être équipés d'au moins un dispositif de connexion réseau, connu sous le nom de carte réseau. 

La carte réseau peut être filaire ou sans fil. Elle est généralement intégrée à la carte mère de l'ordinateur mais les autres appareils comme les caméras de surveillance, les téléphones, les imprimantes, les routeurs, les commutateurs, les points d'accès sans fil, les modems, etc. sont également être équipés d'une carte réseau.

Chaque carte réseau est dotée d'une adresse matérielle unique.

Cette adresse est connue sous le nom d'[**adresse MAC**](/cours/2-vocabulaire.html#adresse-mac-media-access-control) (Media Access Control) ou **adresse physique**.


![carte_reseau](img/carte_reseau.png) ![carte_reseau_wifi](img/carte_reseau_wifi.png) ![cable_ethernet_pc_portable](img/cable_ethernet_pc_portable.jpg) ![rj45_cartemere](img/rj45_cartemere.jpg) 

## Port Ethernet et connecteur RJ45

Un port Ethernet est un connecteur physique permettant de brancher un câble `Ethernet`.

Le connecteur `RJ45` est le connecteur le plus utilisé pour les câbles Ethernet.

![RJ45 femelle](img/rj45f.png){data-zoomable width=15%}
![RJ45](img/rj45.png){data-zoomable width=22%}

## Interface réseau

Chaque carte réseau possède une ou plusieurs interfaces réseau.

Une interface réseau est un ensemble de paramètres qui permettent à un périphérique de communiquer avec un réseau.

Une interface réseau est identifiée par un nom unique.

Nous utiliserons deux types d'interfaces réseau :

- `GigabitEthernet` (Gi) 1Gbit/s
- `FastEthernet` (Fa) 100Mbit/s

Exemples&nbsp;:
- `GigabitEthernet0/1 (Gi0/1)`
- `FastEthernet0/1 (Fa0/1)`

Il y a également l'interface pour le wifi qui est notée `Wlan` ou `Wireless`.


## ipconfig

La commande `ipconfig` permet d'afficher les informations sur les interfaces réseau d'un ordinateur.

Voir le cours sur la [commande ipconfig](/cli/2-ipconfig).


