<!-- Cours 2 -->

# Classification des réseaux

## En fonction de la taille

- Les **PAN** (Personal Area Network) réseau d’étendue limitée à quelques mètres pour l’interconnexion des équipements domestique (bluetooth, RFID).
- Les **LAN** (Local Area Network), réseau d'ordinateurs situés sur un même site (réseau de votre maison, immeuble, usine, cegep). Utilise les normes Ethernet ou Wi-fi.
- Les **MAN** (Metropolitan Local Network) peuvent couvrir la taille d’une ville. (télévision par câble d’une ville, WiMax)
- Les **WAN** (Wide Area Network) ont au moins la dimension d'un pays, et ils englobent souvent la planète entière. Ces réseaux relient plusieurs réseaux locaux en les interconnectant grâce à des routeurs.
- **Interréseaux** (inter-network → Internet). Interconnexion de plusieurs réseaux. Le plus connu est maintenant Internet.

## En fonction de l'ouverture

- **Intranet** : Réseaux privés internes à l'intérieur d'une entreprise (ex. : réseau du Cégep).
- **Internet** : Réseaux publics, nationaux ou internationaux des entreprises de télécommunication.
- **Extranet** : Réseaux privés internes et externes, ouverts vers l'extérieur (ex. : gmail, omnivox, moodle, etc.).


## En fonction de la Topologie

Une topologie est la manière dont est câblé un réseau. On choisit cette topologie selon l’environnement, l’architecture (bâtiments,…) et les besoins techniques de débit pour l’entreprise.

La topologie est donc une image de la structure du réseau.

La topologie physique décrit comment les nœuds du réseau sont interconnectées matériellement entre eux.

![Topologie Réseau](img/topologies.png)

## Topologie de bus

- Tous les nœuds du réseau sont reliés les uns aux autres en formant une chaîne.
- À chaque extrémité du bus est placé un bouchon de terminaison (terminateur) signifiant que le réseau se termine.

### Fonctionnement
Chaque appareil connecté au bus peut communiquer avec les autres en envoyant des données sur le bus. Un signal, appelé **trame**, est envoyé à travers le bus et chaque appareil vérifie si la trame lui est destinée.

- Un seul poste émet sur le bus. Chaque poste reçoit l’information mais seul le poste pour lequel le message est adressé traite l’information.
- On utilise un câble coaxial pour ce type de topologie.

![Topologie de bus](img/topologie_bus.png)

- Avantages :
  - Facile à mettre en œuvre et à étendre.
  - Présente l'un des coûts de mise en réseau le plus bas.
- Inconvénients
  - Longueur du câble et nombre de stations limités.
  - Un câble coupé peut interrompre le réseau.
  - Les performances se dégradent avec l'ajout de stations.
  - Faible sécurité des données.

### Collisions

Dans une topologie de bus, les [collisions](/cours/2-vocabulaire.md#collision) se produisent lorsque deux appareils envoient des données en même temps sur le même bus. Les signaux peuvent se heurter et se mélanger, rendant les données inintelligibles.

- Gestion des Collisions : Les réseaux utilisant une topologie de bus emploient souvent le protocole **CSMA/CD** (Carrier Sense Multiple Access with Collision Detection) pour détecter et gérer les collisions. Lorsqu'une collision est détectée, les appareils arrêtent la transmission, attendent un délai aléatoire, puis tentent à nouveau d'envoyer les données.

## Topologie en anneau

- Chaque équipement est relié à deux équipements voisins de sorte que l’ensemble constitue une boucle fermée.
- La communication se fait à l'aide d'un "jeton" (Token ring) spécial qui circule autour de l'anneau.
- Une station ne peut envoyer des données que lorsqu'elle possède le jeton. Cela signifie qu'à tout moment, une seule station peut transmettre des données, ce qui réduit le risque de [collisions](/cours/2-vocabulaire.md#collision) de données par rapport aux topologies de bus.
- Les données circulent dans une seule direction. Chaque appareil reçoit le signal et, si les données ne lui sont pas destinées, il le transmet à l'appareil suivant dans l'anneau.
- Cette topologie qui a été délaissée il y a plusieurs années et a été reprise pour la fibre optique.
- En FDDI (Fiber Distributed Data Interface - Interface de données distribuées par fibre optique), il y a une deuxième boucle de secours au cas où la première serait inutilisable.

![Topologie en anneau](img/topologie_anneau.gif)

- Avantages :
  - Le protocole est simple, il évite la gestion des collisions.
  - Fonctionne mieux qu'une topologie de bus sous une lourde charge de réseau.
  - réduit les chances de [collision](/cours/2-vocabulaire.md#collision) de données car le signal ne circule que dans une seule direction
- Inconvénients :
  - Le retrait ou la panne d'une entité active paralyse le trafic du réseau.
  - Le délai de communication est directement proportionnel au nombre de nœuds du réseau


## Topologie en étoile

- Dans une topologie en étoile, tous les câbles aboutissent à un nœud central de concentration.
- Ce nœud central est un commutateur.
- Le système repose sur un équipement central qui va diriger toutes les connexions.
- Concerne maintenant la majorité des réseaux.

![Topologie en étoile](img/topologie_etoile.gif)

- Avantages :
  - Ajout facile de postes.
  - Le débranchement d'une connexion ne paralyse pas le reste du réseau.
  - Collision de données réduite voir inexistante.
- Inconvénients :
  - Demande davantage de câblage.
  - Plus onéreux qu'un réseau à topologie en bus.
  - Si le commutateur est défectueux, tout le réseau est en panne.


## Topologie maillée 

- Dans une topologie maillée, chaque appareil du réseau (comme des ordinateurs, des serveurs, etc.) est connecté à plusieurs autres appareils. Cela crée un réseau de connexions multiples, ressemblant à un maillage.
- Une topologie maillée  est utilisée dans les cas où il faut absolument éviter les coupures de communication, si une connexion échoue, il existe toujours une autre liaison pour assurer la communication.

Le réseau Internet peut être envisagé comme une vaste topologie maillée à grande échelle

![Topologie maillée](img/topologie_maillee.png)


## En fonction de l'architecture

L'architecture d'un réseau est la façon dont les composants sont arrangés. Il existe deux types d'architecture de réseau : client-serveur et poste à poste.

### Client-serveur

- Dans une architecture client-serveur, les ressources sont stockées sur un ou plusieurs serveurs dédiés et les clients se connectent à aux serveurs pour accéder aux ressources.
- Cette configuration exige un système d’exploitation réseau serveur (Windows 2016 server, Linux, …) qui permet à l’administrateur de gérer de façon centrale toutes les composantes du réseau et de le sécuriser :
  - Configuration des stations
  - Définition des comptes et des groupes d’utilisateurs
  - Permissions accordées aux groupes et aux utilisateurs
  - …

- Le serveur est un ordinateur puissant qui fournit des services à d'autres ordinateurs, appelés clients, sur le réseau.
- Les clients sont des ordinateurs qui accèdent aux ressources et aux services fournis par le serveur.

![Client-serveur](img/client_serveur.png)


- Avantages :
  - Les ressources sont centralisées, ce qui facilite la sauvegarde et la récupération des données.
  - Les ressources sont partagées entre les utilisateurs.
  - Les utilisateurs peuvent accéder aux ressources à distance.
  - Les utilisateurs peuvent se connecter à différents serveurs.

- Inconvénients :
  - Le serveur est un point de défaillance unique. Si le serveur tombe en panne, les utilisateurs ne peuvent pas accéder aux ressources.
  - Le serveur est un goulot d'étranglement. Si le serveur est surchargé, les performances du réseau peuvent être affectées.
  - Le serveur est coûteux à mettre en place et à maintenir.

### Poste à poste (P2P)

- Dans une architecture poste à poste (peer to peer), les ressources sont partagées entre les utilisateurs.
- Chaque ordinateur peut agir en tant que client (consommateur) ou serveur (fournisseur).
- Les utilisateurs peuvent accéder aux ressources à distance.
- Les utilisateurs peuvent se connecter à différents serveurs.

![Pair à pair](img/p2p.png)

- Avantages :
  - Installation simple
  - Utilisé pour des réseaux domestiques
  - Les ressources sont partagées entre les utilisateurs.
- Inconvénients :
  - La cohérence et la sécurité des données deviennent difficile à maintenir
  - Les sauvegardes sont difficiles car chaque machine héberge ses propres données
  - Les ressources ne sont pas centralisées, ce qui rend la sauvegarde et la récupération des données plus difficiles.

## Sources & en savoir plus

- https://www.courstechinfo.be/Reseaux
- https://sti2d.ecolelamache.org/ii_rseaux_informatiques___7_topologie_des_rseaux.html


## Quiz 🎉

### 1. Qu'est-ce qu'un réseau PAN ?
- a) Personal Area Network
- b) Party Animal Network
- c) Pretty Awesome Network
- d) Public Access Network

### 2. Quelle est la principale caractéristique d'un réseau LAN ?
- a) Il couvre de grandes distances comme un WAN.
- b) Il est utilisé pour connecter des appareils dans une zone géographique limitée.
- c) Il est uniquement utilisé pour les jeux en ligne.
- d) Il fonctionne uniquement avec des câbles LAN.

### 3. Quelle est la différence entre un MAN et un WAN ?
- a) MAN est une ville, WAN est le monde.
- b) MAN est un super-héros, WAN est son acolyte.
- c) MAN utilise des fibres optiques, WAN utilise du Wi-Fi.
- d) Il n'y a aucune différence.

### 4. Quelle est la caractéristique principale d'une topologie de bus ?
- a) Tous les ordinateurs sont connectés à un bus scolaire.
- b) Tous les ordinateurs sont connectés à un seul câble central.
- c) Les données sont envoyées via des bus de la ville.
- d) Chaque ordinateur a son propre bus.

### 5. Comment fonctionne la communication dans une topologie en anneau ?
- a) Les données dansent en cercle avant d'atteindre leur destination.
- b) Les données passent séquentiellement d'un appareil à l'autre.
- c) Un magicien fait apparaître les données à leur destination.
- d) Les ordinateurs jouent à "passer l'anneau".

### 6. Pourquoi choisirait-on une architecture P2P pour un réseau domestique ?
- a) C'est plus facile pour jouer à des jeux en ligne.
- b) Chaque ordinateur peut agir comme client et serveur.
- c) Pour impressionner les voisins.
- d) P2P signifie "Party to Party", idéal pour les fêtes.

