<!-- Cours 23 -->

# DNS

## Introduction

Le DNS (Domain Name System) est un service essentiel pour l'Internet, car il permet aux utilisateurs d'accéder aux sites Web, aux services en ligne et aux ressources réseau en utilisant des noms de domaine faciles à retenir plutôt que des adresses IP numériques.

 Son rôle principal est de traduire les noms de domaine faciles à retenir (comme www.example.com) en adresses IP numériques (comme 192.0.2.1) que les ordinateurs utilisent pour identifier chaque autre sur le réseau.

- DNS => Domain Name Service (Service de nom de domaine).
- Les adresses IP sont difficiles à mémoriser.
- Les noms de domaines ont donc été créés pour nous faciliter les choses.
- Par exemple le nom de domaine google.ca est facile à mémoriser et il correspond à l’adresse IP 172.217.13.163.
- On peut trouver l’adresse IP grâce à la commande nslookup.

![Commande nslookup](img/dns1.png)

## Résolveur DNS

DNS est donc un protocole qui permet de se connecter sur un résolveur DNS pour traduire les noms de domaines en adresse IP.

Le résolveur DNS utilisé par votre ordinateur est probablement fournis par votre fournisseur d’accès à Internet et il est configuré dans le routeur.

Dans les paramètres de la carte réseau, on voit sur la capture que l’adresse est celle du routeur 192.168.2.1 et qu’il y a une adresse secondaire 207.164.234.129 (utilisée si la première ne fonctionne pas).


![Configuration DNS sur un ordinateur](img/dns2.png)


Il est tout à fait possible de configurer d’autres résolveurs DNS sur votre ordinateur ou sur votre routeur. 

En voici quelques exemples :

| Nom du résolveur DNS | Adresse IP |
|-----------------------|------------|
| Quad9 |  	9.9.9.9 <br> 149.112.112.112 |
| FDN |  80.67.169.12 <br>  80.67.169.40 |
| OpenDNS |  208.67.222.222 <br>  208.67.220.220 |
| Google DNS | 8.8.8.8 <br>  8.8.4.4 |

Voir une liste plus complète sur [cette page](https://sebsauvage.net/wiki/doku.php?id=dns-alternatifs).


Gardez à l’esprit qu’un serveur DNS « connaît » et peut enregistrer les sites que vous visitez…

Certains résolveurs DNS peuvent bloquer les sites malveillants, les publicités, les trackers, etc.

## DOT - DOH

Il existe des protocoles plus sécurisés pour les résolveurs DNS :

- DNS over TLS ([DOT](https://fr.wikipedia.org/wiki/DNS_over_TLS#cite_note-18)) : DNS sur TLS (Transport Layer Security).
- DNS over HTTPS ([DOH](https://fr.wikipedia.org/wiki/DNS_over_HTTPS)) : DNS sur HTTPS.

Ces protocoles permettent de chiffrer les requêtes DNS pour plus de confidentialité et de sécurité.

Certains DNS, qualifiés de DNS menteurs, peuvent bloquer l'accès à certains sites Web sur demande de certains gouvernements. Les fournisseurs d'accès à Internet (FAI) peuvent également bloquer l'accès à certains sites Web ou rediriger les utilisateurs vers des publicités ou des pages de recherche personnalisées lorsqu'ils ne trouvent pas de correspondance pour un nom de domaine donné.

L'utilisation de résolveurs DNS alternatifs peut aider à contourner ces restrictions et à améliorer la confidentialité et la sécurité des utilisateurs.


## Cache DNS

L’ordinateur possède un cache DNS qui permet de limiter les appels au résolveur DNS. Pour voir les adresses en cache, utilisez la commande&nbsp;:

```bash
ipconfig /displaydns
```

Pour vider le cache, on utilise la commande&nbsp;:

```bash
ipconfig /flushdns
```

## Historique - Fichier hosts

Avant l'apparition du DNS, quand le réseau des réseaux était encore connu sous le nom d'Arpanet dans les années 70 et 80, l'association des noms de domaine avec les adresses IP se faisait manuellement via le fichier `hosts`.

Ce fichier devait être copié et mis à jour sur toutes les machines, un processus qui est rapidement devenu compliqué. C'est pourquoi le système DNS a été créé en 1983.

À titre informatif, ce fichier `hosts` existe toujours dans les systèmes d'exploitation modernes. Sur Windows, vous pouvez le trouver à l'emplacement suivant **C:\Windows\System32\drivers\etc\hosts**.

Sur un système Linux, il se situe dans : **/etc/hosts**.

Si vous ajoutez la ligne suivante au fichier `hosts`, votre ordinateur ne pourra plus accéder au site `google.ca` :

```bash	
0.0.0.0 www.google.ca
```

Le fichier hosts est toujours utilisé pour des configurations spécifiques, par exemple pour rediriger un site Web vers une adresse IP locale.



## Paquet DNS

- Capture d’un paquet DNS avec Wireshark.
- On peut voir que le DNS utilise le protocole UDP sur le port 53.
- On remarque que la requête DNS est envoyée en clair et que n'importe qui peut voir le site que vous visitez.


![Capture avec Wireshark](img/dns3.png)


- Réponse du serveur DNS

![Capture avec Wireshark](img/dns4.png)

## Fonctionnement

Le résolveur DNS ne « connaît » pas toutes les adresses IP de tout les sites Internet.

Il a cependant un cache qui se met à jour régulièrement (voir TTL, Time To Live).

Il va interroger plusieurs serveurs DNS en prenant les différentes parties du nom de domaine que l’on souhaite atteindre.

Exemple avec **fr.wikipedia.org**

L’extension (.org) va être envoyée au serveur [DNS racine](https://fr.wikipedia.org/wiki/Serveur_racine_du_DNS) qui va renvoyer l’adresse IP du serveur DNS de** niveau 1** qui gère le [TLD](https://fr.wikipedia.org/wiki/Domaine_de_premier_niveau) **.org**.

Le résolveur interroge donc ensuite ce serveur DNS de **niveau 1** qui gère le .org pour savoir quel serveur DNS de **niveau 2** gère le nom **wikipedia.org**.

Le résolveur contacte donc ensuite le serveur DNS de **niveau 2** qui va renvoyer l’adresse IP du serveur qui héberge le site wikipedia.org

Les serveurs DNS sont également utilisés pour la résolution des courriels

![Illustration de l’organisation des serveurs DNS](img/dns5.png){data-zoomable width=50%}

([source](https://ismailmht.over-blog.com/2021/03/un-routeur-comme-serveur-dns.html))

![Illustration de l’organisation des serveurs DNS](img/dns6.png){data-zoomable width=50%}

([source](https://www.youtube.com/watch?v=qzWdzAvfBoo))

## Enregistrement DNS

Quand on achète un nom de domaine, on doit configurer les enregistrements DNS. Dans cet exemple, on voit que le site est sur l’adresse IP 195.15.226.172 (type A)

Le type A est le plus important, il permet de faire la correspondance entre le nom de domaine et l’adresse IP.


![Configuration DNS](img/dns7.png)

Il existe plusieurs types d’enregistrements DNS :

- **A** : Enregistrement de type A (Address) qui associe un nom de domaine à une adresse IP.
- **CNAME** : Enregistrement de type CNAME (Canonical Name) qui permet de rediriger un nom de domaine vers un autre nom de domaine.
- **MX** : Enregistrement de type MX (Mail Exchange) qui indique le serveur de messagerie pour un domaine.
- **TXT** : Enregistrement de type TXT qui permet de stocker du texte arbitraire dans un enregistrement DNS.
- **NS** : Enregistrement de type NS (Name Server) qui indique les serveurs de noms pour un domaine.
- **AAAA** : Enregistrement de type AAAA qui associe un nom de domaine à une adresse IPv6.


Le site [https://www.whatsmydns.net](https://www.whatsmydns.net/) permet de vérifier la propagation des enregistrements DNS.

En effet, il faut parfois attendre quelques heures pour que les enregistrements DNS soient mis à jour sur tous les serveurs DNS du monde.


## Liens et ressources

- [Liste de DNS alternatifs](https://sebsauvage.net/wiki/doku.php?id=dns-alternatifs)
- [Liste de blocage DNS](https://sebsauvage.net/wiki/doku.php?id=dns-blocklist)
- [Changer son DNS sous Windows](https://lecrabeinfo.net/changer-les-dns-sur-windows.html)
- [Changer son DNS sous Android](https://www.numerama.com/tech/1541160-comment-changer-ses-dns-sur-android.html)

- Pour vérifier les DNS
  - https://www.whatsmydns.net
  - https://dns.google
  