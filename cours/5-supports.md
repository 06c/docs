<!-- Cours 3 -->

<script setup>
import QuizComponent from './components/QuizzSupportComponent.vue'
</script>

# Supports de transmission

La fonction des supports est de transmettre les données d’un nœud à un autre à travers le réseau.


- Plusieurs types :
  - Paire filaire torsadée
  - Câble coaxial
  - Fibre optique
  - Sans-fil

- Deux catégories :
  - les supports guidés (supports cuivre et optiques) ;
  - les supports libres (faisceaux hertziens et liaisons satellites).
- Les supports de transmission exploitent les propriétés de conductibilité des métaux (paire
filaire), des ondes électromagnétiques (faisceaux hertziens, satellites) ou du spectre visible de
la lumière (fibre optique).

## Normes

Afin que les équipements de différents constructeurs puissent communiquer entre eux, il est nécessaire que les supports et les protocoles de communication suivent des normes.

Les normes ont été créées pour fournir un cadre de référence unifié, favorisant l'[interopérabilité](https://fr.wikipedia.org/wiki/Interop%C3%A9rabilit%C3%A9), la qualité, la sécurité et l'efficacité des systèmes de télécommunications et des réseaux informatiques.

Les normes utilisées pour les supports de transmission et les transmissions de données sont :
- [IEEE](https://fr.wikipedia.org/wiki/Institute_of_Electrical_and_Electronics_Engineers), comme **IEEE 802.3 (Ethernet)** et **IEEE 802.11 (Wi-Fi)**,
- Les normes **TIA/EIA** (Telecommunications Industry Association/Electronic Industries Alliance), telles que TIA/EIA-568 pour le câblage structuré, ont été établies pour uniformiser le câblage structuré dans les bâtiments commerciaux et résidentiels.


::: tip Résumé
Les normes **IEEE** traitent de la manière dont les données sont transmises et gérées sur le réseau, alors que les normes **TIA/EIA** traitent de la manière dont le réseau est physiquement construit et organisé.
:::



## Norme Ethernet : IEEE 802.3

https://fr.wikipedia.org/wiki/Ethernet

Voici un aperçu de quelques-unes des normes Ethernet, depuis ses débuts avec des câbles coaxiaux jusqu'à l'utilisation de la fibre optique en passant par les paires torsadées&nbsp;:

- **10Base5** : Utilise un câble **coaxial** épais pour transmettre des données à une vitesse de 10 Mbits/s. Connu sous le nom d'Ethernet "thicknet", ce fut l'un des premiers types de câblage Ethernet.

- **10Base2** : Semblable au 10Base5 en termes de vitesse, cette norme utilise cependant un câble **coaxial** plus fin, offrant une plus grande flexibilité et facilité d'installation. Il est souvent appelé Ethernet "thinnet".

- **10BaseT** : Marque une transition majeure vers l'utilisation de **paires torsadées**, les câbles Ethernet standard que nous connaissons aujourd'hui. Cette norme permet une transmission des données à **10 Mbits/s**.

- **100BaseT** : Une évolution du 10BaseT, ce standard utilise également des **paires torsadées**, mais augmente la vitesse de transmission des données à **100 Mbits/s**.

- **1000BaseT** : Connu aussi sous le nom de **Gigabit Ethernet**, il utilise des **paires torsadées** pour une transmission des données à une vitesse de **1 Gbit/s**, offrant une performance considérablement accrue pour les réseaux modernes.

- **10GBaseT** : Cette norme utilise des **paires torsadées** pour une transmission des données à une vitesse de **10 Gbit/s**.

- **1000BaseF** : Cette norme représente l'adoption de la fibre optique pour les réseaux Ethernet, permettant des vitesses de transmission en gigabits avec les avantages de la fibre optique, tels que la résistance aux interférences électromagnétiques et une portée étendue.

## Norme Wi-Fi - IEEE 802.11

[Wikipedia](https://fr.wikipedia.org/wiki/IEEE_802.11)

::: tip Info
Wi-Fi signifie **Wireless Fidelity** (fidélité sans fil, en relation avec l'appellation des systèmes audio Hi-Fi (High Fidelity)).

C'est une marque déposée par la *Wi-Fi Alliance*, une organisation à but non lucratif qui supervise le développement des réseaux Wi-Fi et certifie les produits Wi-Fi satisfont aux normes de sécurité et d'interopérabilité.

La première norme Wi-Fi a été publiée en 1997.
:::

Les normes Wi-Fi sont des normes de communication sans fil qui définissent les protocoles de communication et les technologies de transmission utilisés pour les réseaux sans fil. Les normes Wi-Fi sont définies par l'**IEEE 802.11** et sont régulièrement mises à jour pour améliorer les performances et la sécurité.

Chaque nouvelle version améliore les performances du débit de données (Mbit/s ou Gbit/s), la portée (20m au début, 100m aujourd'hui) et la sécurité.

La dernière version de la norme Wi-Fi est **802.11ax** (Wi-Fi 6) qui a été publiée en 2019.

- 802.11ax : 10 Gbit/s, 2,4 GHz et 5 GHz, portée 35m

## Paire torsadée

![Paire torsadée](img/paire_torsadee.png){data-zoomable width=60%}

Elle est constituée de deux conducteurs identiques torsadés. Les torsades **réduisent les interférences de la ligne**.
- Généralement plusieurs paires sont regroupées sous une enveloppe protectrice appelée gaine pour former un câble.
- Les câbles contiennent **quatre paires**.

![Paire torsadée](img/paire_torsadee1.png){data-zoomable width=40%}

## Câble à paire torsadée

Les normes Ethernet **100BASE-T**, **1000BASE-T**, **10GBASE-T** utilisent des câbles à paires torsadées.

Le câble à paire torsadée (Twisted-Pair) est composé de 4 ou 8 fils placés en paires et organisés en spirale et d’une enveloppe isolante.

Dans les noms des normes, la partie **100, 1000** représente la vitesse en Mb/s – **10G** représente la vitesse de 10 Gb/s.
- Le **T** signifie que le câble est en paires torsadées (Twisted → Torsadé.)
- Les torsades permettent de limiter les interférences électromagnétiques mais la protection d’un blindage est bien plus efficace pour diminuer les risques d’interférences.
- La paire torsadée est utilisée en téléphonie et dans les réseaux locaux en raison de son faible coût.
- Elle pose des problèmes dans les transmissions à grande vitesse dès que la distance dépasse **100 m**.
- Elle présente une forte sensibilité aux **interférences électromagnétiques**, ce qui l'expose à des erreurs de transmission dont le taux peut être non négligeable.
- La norme [TIA/EIA-568](https://fr.wikipedia.org/wiki/ANSI/TIA-568) définit les caractéristiques des câbles à paires torsadées.

### Longueur
- La norme **TIA/EIA-568** définit que la longueur maximum des câbles ne doit pas excéder **100m**.
entre deux équipements (PC à Commutateur par exemple).
- Dans les faits, on limite à 90 m la longueur de câble entre la prise murale et l’armoire de brassage; ce qui permet de garder deux fois cinq mètres pour les cordons de brassage.

![Schéma réseau](img/SchemaVDIGrade3.png){data-zoomable width=50%}

Source : [Réseau VDI](https://reseau-vdi.fr/schema-composants-reseau-vdi-grade-3/)

### Type de câble
- Paires torsadées **non blindées UTP** (Unshielded
Twisted Pair)
- Paires torsadées **blindées STP** (Shielded Twisted Pair)

![Type de câble](img/type_cable.png)

![Type de câble blindage](img/type_cable1.png)


| Ancienne désignation | Nouvelle désignation |
|----------------------|----------------------|
| UTP                  | U/UTP                |
| FTP                  | F/UTP                |
| S-FTP                | S/UTP                |
| S-STP                | S/FTP                |


![Câble](img/cable.png)

### Catégories de câbles

| Catégories   | Vitesse Max.          | Fréquence | Distance Max. |
|--------------|-----------------------|-----------|---------------|
| Catégorie 1  | 1 Mbps                | 0.4 MHz   | -             |
| Catégorie 2  | 4 Mbps                | 4 MHz     | -             |
| Catégorie 3  | 10 Mbps               | 16 MHz    | 100m          |
| Catégorie 4  | 16 Mbps               | 20 MHz    | 100m          |
| Catégorie 5  | 100 Mbps              | 100 MHz   | 100m          |
| Catégorie 5e | 1 Gbps                | 100 MHz   | 100m          |
| Catégorie 6  | 1 Gbps                | 250 MHz   | 100m          |
| Catégorie 6a | 10 Gbps               | 500 MHz   | 100m          |
| Catégorie 7  | 10 Gbps               | 600 MHz   | 100m          |
| Catégorie 7a | 10 Gbps               | 1000 MHz  | 100m          |
| Catégorie 8  | 25 Gbps - 40 Gbps     | 2000 MHz  | 30m           |

### Code couleurs

| Ordre | EIA/TIA 568A | EIA/TIA 568B |
|-------|--------------|--------------|
| 1     | Blanc Vert   | Blanc Orange |
| 2     | Vert         | Orange       |
| 3     | Blanc Orange | Blanc Vert   |
| 4     | Bleu         | Bleu         |
| 5     | Blanc Bleu   | Blanc Bleu   |
| 6     | Orange       | Vert         |
| 7     | Blanc Brun   | Blanc Brun   |
| 8     | Brun         | Brun         |


![Code couleur câble](img/code_couleur.png)

### Câblage droit ou croisé
- Les **câbles croisés** utilisent le code des couleurs T568A d’un coté et T568B de l’autre. Les câbles croisés sont utilisés afin d'interconnecter **les unités de même type**&nbsp;:
  - Commutateur à Commutateur
  - PC à PC
  - Routeur à Routeur
  - Routeur à PC

![Câble croisé](img/cable_croise.png)


- Les **câbles droits** utilisent le code des couleurs T568B des deux cotés. Les câbles droits sont quant à eux utilisés pour interconnecter les unités suivantes&nbsp;:
  - Commutateur à PC
  - Commutateur à Routeur
  - Commutateur à Serveur

::: tip Info
Les équipement modernes (après 2000) sont souvent équipés de ports **auto MDI/MDI-X**. Ces ports sont conçus pour détecter automatiquement le type de câble Ethernet connecté.

Avec l'auto MDI/MDI-X, le port détecte automatiquement si un câble droit ou un câble croisé est connecté. Il ajuste ensuite sa configuration interne pour s'aligner correctement avec le type de câble.

- MDI est typiquement utilisé dans les cartes réseau des ordinateurs.
- MDI-X est utilisé dans les dispositifs tels que les commutateurs ou les routeurs.

:::


## Câble coaxial – 10B2 - 10B5
- Composé d’une partie centrale, c'est-à-dire un fil de cuivre, enveloppé dans un isolant, puis d’un blindage métallique tressé et d'une gaine extérieure.
- Utilisé dans plusieurs domaines:
  - Entre une antenne TV et un récepteur de télévision.
  - Dans le réseau câblé urbain.
  - Entre un émetteur et l'antenne d'émission, par exemple une carte électronique Wi-Fi et son antenne.
  - Entre des équipements de traitement du son.
  - Dans les anciennes versions de réseaux Ethernet.
  - Topologie de type Bus ou anneau.
- Longueur maximale 200 mètres (10B2) et 500 mètres (10B5).
- Débit de 10 Mb/s.
- Il présente une bonne immunité aux parasites, mais cher et exigeant en contraintes d’installation (rayon de courbure…)
- Dans les réseaux locaux, il est remplacé par la paire torsadée.
- Le câble coaxial est maintenant remplacé par la fibre optique sur les longues distances (supérieures à quelques kilomètres).

![Câble coaxial text](img/coaxial.png)

![Prise pour câble coaxial](img/prise_coaxial.png)

## Fibre optique – 100BaseFX - 1000BaseSX - 10GBaseSR

La fibre optique est un fil en verre ou en plastique très fin qui sert à transmettre de la lumière. 

**Transmission de Données** : Les données sont transmises sous forme de signaux lumineux, ce qui permet des vitesses de transmission très élevées, bien supérieures à celles des câbles en cuivre.

**Vitesse de Transmission** : Les vitesses commerciales typiques varient de 1 Gbps à 100 Gbps, mais les expériences en laboratoire ont atteint des vitesses de plusieurs térabits par seconde (Tbps).


- Un émetteur de lumière (transmetteur), constitué d’une diode électroluminescente (Led) ou d’une diode laser, transforme les impulsions électriques en impulsions lumineuses.
- Un récepteur de lumière traduit les impulsions lumineuses en signaux électriques.
- Entourée d'une gaine protectrice, elle peut être utilisée pour conduire de la lumière entre deux lieux distants de plusieurs milliers de kilomètres (avec des répéteurs).
- Elle rend possible les communications à très longue distance et à des débits importants, la fibre optique est l'un des éléments clef de la révolution des télécommunications optiques.

**Avantages** :
- Haute Bande Passante : Capacité à transporter de grandes quantités de données rapidement.
- Faible Atténuation : Peu de perte de signal sur de longues distances comparativement aux câbles en cuivre.
- Immunité aux Interférences Électromagnétiques : Performances stables dans divers environnements.
- Taux d'erreur de transmission très faible, ce qui allège les temps de détection et de retransmission.
- Matériau très léger, ce qui est intéressant là où les contraintes de poids interviennent comme dans le cas de son utilisation comme conducteur électrique dans les avions et les satellites.

![Fibre optique](img/fibre_optique.png){data-zoomable width=20%}


**Applications** :
- Télécommunications : Utilisée pour les réseaux de téléphonie et d'internet à haut débit.
- Réseaux Informatiques : Idéale pour les réseaux d'entreprise et de centres de données.
- Câbles Sous-marins : Essentielle pour les liaisons de communication internationales.

**Types de Fibres Optiques** :
- **Monomode** : Permet une seule trajectoire de lumière, utilisée pour les transmissions à longue distance et à haute vitesse.
- **Multimode** : Permet plusieurs trajectoires de lumière, utilisée pour les distances plus courtes comme dans les bâtiments ou les campus.

- **Installation et Coût** : L'installation de la fibre optique peut être coûteuse et techniquement exigeante, mais elle offre un retour sur investissement important grâce à sa vitesse et sa fiabilité.

![Fibre optique](img/fibre_optique1.png){data-zoomable width=20%}

Source images : [Fibre optique](https://fr.wikipedia.org/wiki/Fibre_optique)

## Sans fil - norme IEEE 802.11

- Les réseaux sans fil utilisent des fréquences radios ou des infrarouge pour transporter les
données.
- Les signaux sans fil sont des ondes électromagnétiques qui transitent dans l’air.
- Aucun média de transport physique n’est nécessaire.
- Le terme Wi-Fi est un ensemble de normes concernant les réseaux sans fil.

![LiFi](img/lifi.png){data-zoomable width=20%}![WiFi](img/wifi.png){data-zoomable width=19%}

- **LiFi** : Communication par la lumière [Wikipedia](https://fr.wikipedia.org/wiki/Li-Fi)


## Mode de transmission 

- **Simplex** : Un seul sens de transmission. Exemple : Télévision, Radio.
- **Half-duplex** : Les données peuvent être transmises dans les deux sens, mais pas simultanément. Utilise une paire torsadée. Analogie : Talkie-walkie. 
- **Full-duplex** : Les données peuvent être transmises dans les deux sens simultanément. Utilise deux paires torsadées. Exemple : Téléphone, Internet.
- **Auto négociation** : Mécanisme de détection des partenaires. Lorsque deux équipements sont connectés entre eux, ils vont s'échanger leurs paramètres de fonctionnement le type de duplex supporté, le débit et l'ensemble des paramètres associés. En fonction de ces données, ces deux équipements vont tenter de négocier une configuration de fonctionnement optimale.
- **Collision** : Intervient lorsque deux équipements d'un réseau émettent simultanément sur un même média partagé.

![Mode de transmission](img/transmission.png)

Le mode de transmission peut parfois être choisi par l'utilisateur, mais dans la plupart des cas, il est déterminé par le matériel et les protocoles utilisés.

## Bande passante

La bande passante est la quantité de données qui peut être transmise en un temps donné. Elle est mesurée en bits par seconde (bps).


## Quiz 🎉 

<QuizComponent />
