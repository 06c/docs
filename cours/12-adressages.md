<!-- Cours 7 -->

<script setup>
import QuizComponent from './components/QuizzAdressageComponent.vue'
</script>


# Adressage IP statique et dynamique (DHCP)

On distingue 2 méthodes d’attribution d’adresses IP pour les hôtes :
- **Statique** : chaque équipement est configuré manuellement avec une adresse unique. 
- **Dynamique** : avec le protocole **DHCP** (Dynamic Host Configuration Protocol). Permet l’obtention dynamique d’IP. C’est un protocole qui fonctionne en client/serveur avec le protocole UDP sur le port 67 pour le serveur et le port 68 pour le client.



## Adressage statique

L'adressage statique est une méthode d'attribution d'adresses IP qui est configurée manuellement par un administrateur réseau. Cela signifie que chaque appareil sur le réseau **doit être configuré avec une adresse IP unique**, ainsi que d'autres paramètres réseau tels que le masque de sous-réseau, la passerelle par défaut et les serveurs DNS.

L'adressage statique est souvent utilisé dans les petits réseaux où le nombre d'appareils est limité et où les adresses IP ne changent pas souvent. Cela permet un contrôle précis sur les adresses IP attribuées à chaque appareil, mais cela peut devenir fastidieux et sujet aux erreurs dans les grands réseaux.

## Adressage dynamique - Protocole DHCP

L'adressage dynamique est une méthode d'attribution d'adresses IP qui est gérée automatiquement par un serveur DHCP (Dynamic Host Configuration Protocol). Lorsqu'un appareil se connecte à un réseau, il envoie une requête DHCP pour obtenir une adresse IP et le masque de sous-réseau.

Le serveur DHCP répond à cette requête en attribuant une adresse IP disponible à l'appareil, souvent pour une durée déterminée (bail). Cela simplifie considérablement la gestion des adresses IP, surtout dans les grands réseaux où l'attribution manuelle serait fastidieuse et sujette aux erreurs.


Le processus d’attribution dynamique d’une adresse IP se déroule en 4 étapes : discover, offer, request, ackowlege.


### Fonctionnement

- **Découverte** (*Discovery*) : L'appareil client qui a besoin d'une configuration IP envoie un paquet `DHCPDISCOVER` en broadcast (donc sur tous les appareils) sur le réseau local. Ce paquet indique qu'il recherche un serveur DHCP pour obtenir une configuration réseau. Ll’adresse IP du client en attente d’attribution est l’adresse réservée 0.0.0.0.
- **Offre** (*Offer*) : Les serveurs DHCP qui reçoivent la requête de découverte répondent avec un paquet `DHCPOFFER`. Chaque offre contient une adresse IP que le serveur est prêt à louer au client, ainsi que d'autres configurations réseau telles que le masque de sous-réseau, la passerelle par défaut, et les serveurs DNS.
- **Demande** (*Request*) : Après avoir reçu une ou plusieurs offres, le client sélectionne une offre et envoie un paquet `DHCPREQUEST` en broadcast pour indiquer à tous les serveurs DHCP son intention d'accepter une offre spécifique. Ce paquet sert également à informer les autres serveurs DHCP que leur offre n'a pas été choisie, leur permettant de remettre l'adresse IP proposée dans le pool d'adresses disponibles.
- **Attribution** (*Acknowledgment*) : Le serveur DHCP qui a fait l'offre choisie par le client répond avec un paquet `DHCPACK`, confirmant l'attribution de l'adresse IP et fournissant les détails de la configuration réseau. Si pour une raison quelconque l'adresse IP n'est plus disponible ou si le client a fait une demande invalide, un serveur peut envoyer un paquet `DHCPNAK` pour rejeter la demande..

![Fonctionnement DHCP](img/fonc_dhcp.png)

### Bail

Le bail est la durée pendant laquelle l'adresse IP est attribuée à l'appareil.

Si un client voit son bail expirer, il doit renouveler son bail en demandant une nouvelle adresse IP au serveur DHCP.

La commande `ipconfig /renew` sous Windows permet de renouveler le bail.

`ipconfig /release` permet de libérer le bail.


## APIPA

Il arrive parfois sur les machines clientes attendant une configuration DHCP  que l'on se retrouve après un délai d'attente avec une adresse de type **169.254.X.X**.

Ces adresses se nomment **APIPA** pour "Automatic Private Internet Protocol Addressing" soit "Adressage automatique du protocole IP". Il s'agit en réalité d'une adresse que la machine va s'attribuer automatiquement si les requêtes DHCP effectuées auparavant échouent.

L'APIPA se trouvera dans le réseau 169.254.0.0/16 (de 169.254.0.1 à 169.254.255.254). C'est un réseau privé qui n'est routable ni sur Internet ni ailleurs. Il faut savoir que le service APIPA continue de vérifier la présence d'un serveur DHCP dans les environs toutes les cinq minutes, plus concrètement il réémet une requête de type `DHCPDISCOVER` en espérant avoir une réponse d'un serveur.

![APIPA](img/apipa.png)

## Démo

::: tip Pratique
- Créez un réseau avec un serveur DHCP
- Activez l’adressage dynamique des clients.
- Suivre les messages transmis entre le client et le serveur DHCP
- Vérifiez l’attribution des adresses IP sur les clients.
- Éteignez le serveur DHCP et vérifiez le comportement des clients.


![DHCP animation](img/dhcp.gif)

:::

## Configuration du serveur DHCP

La plupart des routeurs ont un serveur DHCP intégré.

![Routeur RV340](img/routeur_rv340.png)

Sur le routeur RV340, vous avez déjà configuré le serveur DHCP  en réalisant l'activité  [Config DHCP](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EV_ZywmVUPNGineo6AbCpEABt6FFV1GsnERCIfAWgagaMw?e=Dvpvtk).

---

Dans **Packet Tracer**, il faut utililser un périphérique de type serveur et configurer le service DHCP.

![Serveur DHCP dans Packet Tracer](img/server_dhcp.png)


## Quiz 🎉 

<QuizComponent />