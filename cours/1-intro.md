<!-- Cours 1 -->
# Présentation générale

## Historique (rapide) 

**Télécommunication (communication à distance)**

Vient du préfixe grec *tele-* signifiant **loin**, et du latin *communicare*, signifiant **partager**.

Désigne la transmission d’informations à distance en utilisant des technologies électronique, informatique, de transmission filaire, optique ou électromagnétique (sans fil).


- Signaux de fumée
- Télégraphe et code Morse (1839)
- Téléphone (Alexander Bell - 1876)
- Réseau informatique (années 1960)
- Internet (années 1980)
- ...


Depuis la création des premiers ordinateurs, il est vite devenu nécessaire de les connecter entre eux pour partager **des informations et des ressources**.
- Les premiers réseaux étaient des réseaux locaux (LAN) qui permettaient de connecter des ordinateurs dans un lieu restreint (université, entreprise, site militaire).
- Les réseaux se sont ensuite étendus à des réseaux d’entreprise (WAN) qui permettaient de connecter des ordinateurs dans des bâtiments différents.
- Les différents réseaux ont ensuite été interconnectés pour former Internet, le réseau des réseaux qui se développe de façon importante depuis les années 1980.

De nos jours, de plus en plus d’objets autres que des ordinateurs sont connectés à Internet : caméras de surveillance, radars routiers, télévisions, frigos,  équipements médicaux, jouets pour enfants, voitures et même des ampoules électriques. On parle alors d’Internet des objets (IoT pour Internet of Things).

## Définition

::: danger Un réseau informatique
 est un ensemble **d’équipements** informatiques variés (ordinateurs, imprimantes, serveurs…) qui communiquent entre eux par l’intermédiaire de **supports de transmission**.
 
 Les communications se font grâce à des règles précises qu’on appelle **protocoles**.
:::

C’est l’interconnexion des différents matériels informatiques dans le but d’échanger des **données**.

::: tip Pour mettre en œuvre un réseau, il faut au minimum :
- deux ordinateurs (avec une carte réseau ou une carte wifi)
- une connexion physique ou sans fil
- un langage commun (protocole)
:::

Exemple de réseau informatique

![Réseau](img/reseau_.drawio.png)

## Utilité des réseaux

**Réduction des coûts – Partage des ressources – Facilité de communication – Sécurité**

- À l'origine le besoin qui a abouti à la création des réseaux est le partage des ressources les plus coûteuses, comme le stockage des fichiers sur un disque dur, ou les imprimantes.
- Les logiciels sont moins coûteux en version multipostes, qu’en version monoposte. Leur évolutivité est plus facile à assurer, il suffit de les mettre à jour une seule fois sur le serveur.
- L'interconnexion des ordinateurs permet une meilleure communication des documents, des messages, et des ressources humaines au sein d'une entreprise.
- Les données sont regroupées sur le serveur ce qui facilite leur mise à jour et leur sauvegarde.
- Le réseau permet l'accès à l'ensemble des utilisateurs des ressources informatiques comme&nbsp;:
  - les imprimantes,
  - les disques hautes capacités,
  - les modems.

## Avantages des réseaux

- **Partager** des ressources matérielles et logicielles (serveur d'impression et serveur d'application)
- **Centraliser** les données importantes (serveur de fichier)
- **Communication et collaboration** en temps réel
- **Accès à distance** : Les utilisateurs peuvent se connecter au réseau d'entreprise à distance.
- **Scalabilité** : Les réseaux sont conçus pour être extensibles, permettant l'ajout facile de nouveaux utilisateurs et ressources.
- **Partage de connexion Internet** : Un réseau permet à de nombreux utilisateurs de partager une connexion Internet.
- **Sauvegarde et récupération de données**.
- **Mise à jour et gestion logicielle** : Les réseaux permettent de déployer des mises à jour logicielles de manière centralisée plutôt que machine par machine, économisant temps et efforts.
- **Répartition des charges de travail** : La capacité à répartir les tâches sur plusieurs ordinateurs peut améliorer la performance et la fiabilité.
- **Réduction du papier**.


## Inconvénients des réseaux

- Si le serveur de réseau tombe en panne, tout le réseau est en panne.
- Les **coûts d'installation** sont élevés à court terme, mais ils peuvent vite être rentabilisés à moyen et long terme.
- L'aspect **sécurité** devient primordial et surtout dans un contexte sans-fil.
- **Complexité de gestion** : Plus un réseau est grand et complexe, plus il est difficile à gérer.
- **Mises à jour et maintenance**.

#