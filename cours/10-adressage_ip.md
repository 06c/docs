<!-- Cours 5 -->
# Adressage IP

Liens vers les cours sur Teams

- [Système binaire](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EbWcxSvYffhIle_5PNI8XckBXci-SgZWrTnjkFzAnfWXdQ?e=821Kk9)
- [Adressage IP](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EeRrAshGcG1FqanjtObKKCABYELZAM83cFL7FEZm1UCT9w?e=qgUO2j)
- [Tableau des classes](https://livecegepfxgqc.sharepoint.com/:b:/s/H24-42006CFX-00001552/EbWcxSvYffhIle_5PNI8XckBXci-SgZWrTnjkFzAnfWXdQ?e=821Kk9)
- [Exercices IP](https://livecegepfxgqc.sharepoint.com/:w:/s/H24-42006CFX-00001552/EcDhyfpfNKBLl4EIRb3MPv0B2PfPVJcfofhHLZlal6UXoQ?e=fiAXPP)