<!-- Cours 17 -->

<script setup>
import QuizComponent from './components/QuizzVlanComponent.vue'
</script>

# VLAN

Un VLAN (Virtual Local Area Network) est une technologie de réseau qui permet de segmenter un réseau physique en plusieurs réseaux logiques distincts. Ces réseaux logiques peuvent être configurés de manière à ce que les groupes d'appareils sur un même réseau physique se comportent comme s'ils étaient sur des réseaux séparés, permettant une meilleure gestion du trafic, une amélioration de la sécurité et une plus grande efficacité dans l'utilisation des ressources réseau.

- Un VLAN (Virtual Local Area Network - réseau local virtuel) est un réseau non pas physique mais logique, il segmente un réseau physique en plusieurs réseaux logiques.
- La configuration d'un réseau local virtuel est effectuée dans le commutateur et plusieurs VLAN peuvent coexister sur un même commutateur.
- Permet de regrouper un groupe logique d'unités ou d'utilisateurs par fonctions, services ou applications peu importe l'emplacement de leur segment physique. 

## Exemple

Par exemple, une entreprise peut occuper tout un immeuble et avoir plusieurs secteurs répartis dans chaque niveau de l’immeuble utilisant les mêmes ressources. 

On peut retrouver par exemple le service marketing au premier niveau, au troisième niveau et au dernier niveau de l’immeuble. Étant dans le même réseau bien sûr ses activités sont dispersées.

Un VLAN permettra de placer tous les postes du service marketing dans un même réseau indépendant des autres réseaux.

![Exemple de 3 VLAN](img/vlan1.png){data-zoomable width=40%}

![Exemple de VLAN](img/VLANs.gif){data-zoomable width=50%}
<figure>Exemples de VLAN</figure>

## Intérêt

- La technique des VLAN (Virtual Local Area Network) permet de donner au réseau l’architecture logique souhaitée par l’administrateur, en le libérant de certaines contraintes physiques.
- C’est une technique de segmentation, qui participe donc à la sécurité.

Les VLAN doivent être utilisés pour :
- Regrouper des postes selon un critère logique et non plus géographique.
- Améliorer la gestion du réseau.
- Optimiser la bande passante.
- Gérer correctement la mobilité des postes.
- Contrôler la taille des domaines de broadcast.
- Leur utilisation n’est pas motivée par des raisons liées à la sécurité, mais à l’architecture.
- La conception d’une architecture bien pensée, avec si besoin l’utilisation des VLAN est le prérequis à une bonne gestion de la sécurité.

## Avantages

- **Sécurité** : Les VLAN permettent de segmenter le réseau en fonction des services ou des départements. Cela permet de limiter l'accès aux ressources réseau en fonction des besoins de chaque groupe.
- **Performance** : Les VLAN permettent de réduire le trafic réseau en limitant la diffusion des trames aux seuls appareils du même VLAN.
- **Flexibilité** : Les VLAN permettent de déplacer des appareils d'un VLAN à un autre sans avoir à modifier le câblage physique du réseau.
- **Gestion** : Les VLAN permettent de simplifier la gestion du réseau en regroupant les appareils en fonction de leur utilisation ou de leur emplacement.

## Inconvénients

- **Complexité** : La configuration des VLAN peut être complexe, en particulier dans les réseaux de grande taille.
- **Coût** : Les VLAN nécessitent des équipements réseau compatibles, ce qui peut entraîner des coûts supplémentaires.
- **Sécurité** : Les VLAN ne sont pas une solution de sécurité en soi et ne doivent pas remplacer d'autres mesures de sécurité, telles que les pare-feu ou les systèmes de détection d'intrusion.

## Fonctionnement

- Les VLAN sont configurés au niveau des commutateurs réseau, qui utilisent des étiquettes VLAN pour identifier les trames appartenant à chaque VLAN.
- Les commutateurs réseau utilisent des protocoles de signalisation, tels que le protocole IEEE 802.1Q, pour marquer les trames avec des étiquettes VLAN.
- Les commutateurs réseau utilisent des tables de commutation pour acheminer les trames entre les appareils du même VLAN.
- Les routeurs réseau peuvent être utilisés pour relier les VLAN entre eux et permettre la communication entre les appareils de différents VLAN.
- Les équipements terminaux ignorent leur appartenance à un VLAN.
- Les commutateurs ont donc un IOS qui permet de mettre à jour une micro base de données.
- Cela peut se faire par le branchement d’une console en mode texte directement sur switch.
- Certains switch contiennent un mini serveur web qui permet de paramétrer les VLAN (entre autres choses).

## Segmentation

### Réseau local commuté traditionnel.
Dans cette figure, le routeur effectue la segmentation du réseau en fonction des concentrateurs, ou commutateurs.

![Réseau local commuté traditionnel](img/reseau_tradi.png){data-zoomable width=30%}

### Réseaux locaux virtuels.
Dans cette figure, les commutateurs autorisant les réseaux locaux virtuels permettent la segmentation en domaines de collision et de diffusion plus petits. 

![Réseaux locaux virtuels](img/reseau_virtuel.png){data-zoomable width=30%}

## Trunking (liaison de tronc)

- Le trunking est une technique qui permet de transporter plusieurs VLAN sur une seule liaison entre deux commutateurs.
- Il est utilisé pour relier les commutateurs qui gèrent plusieurs VLAN.
- Il permet de transporter les trames de plusieurs VLAN sur une seule liaison, ce qui permet de réduire le nombre de liaisons nécessaires pour connecter les commutateurs.

![Trunking](img/VLAN_trunk.gif){data-zoomable width=60%}


## Type de VLAN

### Les VLAN par port (Vlan de niveau 1)

- Les VLAN par port associent un port du commutateur à un numéro de VLAN.
- On dit que le port est tagué suivant le VLAN donné.
- Le commutateur possède une table qui lit chaque VLAN au port associé.

![VLAN par port](img/vlan_port.png){data-zoomable width=60%}

![VLAN par port](img/VLANs_per_port.gif){data-zoomable width=60%}


### Les VLAN par adresse MAC (Vlan de niveau 2) 

- On affecte chaque adresse MAC à un VLAN. L’appartenance d’une trame à un VLAN est déterminée par son adresse MAC.
- Il s’agit, à partir de l’association MAC/VLAN, d‘affecter dynamiquement les ports des commutateurs à chacun des VLAN en fonction de l’adresse MAC de l’hôte qui émet sur ce port. L'intérêt principal de ce type de VLAN est l'indépendance vis-à-vis de la localisation géographique.
- Offre une sécurité moindre que le VLAN par port de par la possibilité de « spoofer » l’adresse MAC (technique qui permet de modifier son adresse MAC).

![VLAN par adresse MAC](img/vlan_mac.png){data-zoomable width=60%}

### Les VLAN par adresse IP (VLAN de niveau 3)

- On affecte une adresse IP à un VLAN. L’appartenance d’une trame à un VLAN est alors déterminée par l’adresse IP.
- Moins sécurisé que le VLAN par port car il est facile de modifier une adresse IP.

## VTP (VLAN Trunking Protocol)

- Le protocole VTP (VLAN Trunking Protocol) est un protocole de gestion des VLAN qui permet de configurer automatiquement les VLAN sur plusieurs commutateurs.
- Il permet de synchroniser les informations de VLAN entre les commutateurs et de simplifier la gestion des VLAN dans un réseau.
- Le protocole VTP utilise un serveur VTP pour distribuer les informations de VLAN aux autres commutateurs du réseau.
- Le protocole VTP utilise des messages de publicité pour annoncer les changements de VLAN et pour synchroniser les informations de VLAN entre les commutateurs.


## Quiz 🎉 

<QuizComponent />

## Sources

https://www.networkacademy.io/ccna/ethernet/vlan-concept