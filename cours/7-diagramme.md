<!-- Cours 3 -->

# Diagramme de réseau

Un diagramme de réseau est une représentation graphique d'un réseau informatique. Il montre les appareils et les connexions entre eux. Il est utilisé pour planifier, concevoir, documenter ou dépanner un réseau.

Il peut inclure des éléments tels que des ordinateurs, des serveurs, des routeurs, des commutateurs, des câbles et des lignes de transmission.

Ils peuvent montrer des réseaux de différents niveaux de détail, allant des réseaux de haut niveau tels que la connectivité entre les villes aux réseaux plus granulaires comme celui d'un immeuble de bureaux.

![Diagramme de réseau](img/reseau1.png)

## Créer un diagramme de réseau

Logiciels ou application pour créer des diagrammes :
- [Lucidchart](https://lucid.app)
- [Drawio](https://app.diagrams.net/)
- Microsoft Visio (se rendre sur Azure pour l’installer, ou connectez vous sur www.office.com pour utiliser la version en ligne)

En utilisant un diagramme de réseau, les administrateurs réseau peuvent rapidement identifier les points faibles et les problèmes potentiels dans le système, ainsi que planifier les mises à jour et les améliorations nécessaires pour améliorer les performances et la fiabilité du réseau.

## Icônes et symboles

Il existe de nombreux symboles / icônes utilisés dans les diagrammes de réseau.

Principalement vous trouverez les icônes génériques, et ceux proposés par Cisco.

Cette [page](https://www.edrawsoft.com/network-symbols.html) vous présente de nombreux icônes.

| Icônes routeur | Icônes commutateur |
|--|--|
|![Routeur](img/routeur.png)|![Commutateur](img/commutateur.png)|


Les logiciels proposent en général des bibliothèques d’icônes.

Il est important de choisir une bibliothèque et de s’y tenir. 

![Icônes de LucidChart](img/icone_lucid.png){ width=50% }

Exemples de bibliothèques proposés par Lucidchart


## Règles de création

- Utilisez des symboles standard pour les différents éléments du réseau, tels que les ordinateurs, les routeurs, les commutateurs, etc.
- Utilisez des lignes droites pour représenter les connexions physiques entre les éléments du réseau.
- Utilisez des étiquettes claires et lisibles pour identifier les différents éléments du réseau.
- Organisez les éléments du réseau de manière logique et facile à comprendre.
- Utilisez des couleurs pour différencier les différents types d'éléments ou de connexions dans le réseau.

## Cartouche



Le cartouche est un élément graphique rectangulaire placé en général en bas à droite d’une feuille de dessin.
Il permet :
- d’identifier le nom et le numéro de version du diagramme de réseau, ainsi que la date de création ou de mise à jour
- d’indiquer d’autres informations utiles comme une description, nom ou logo de l’entreprise...
- d’indiquer les informations des personnes qui ont créé ou mis à jour le diagramme
- de protéger les informations et les propriétés intellectuelles du dessin en indiquant les droits d'auteur et les restrictions d'utilisation.

Il doit au moins inclure les informations suivantes :
- Le nom du réseau
- Le nom du créateur du diagramme
- La date de création
- La version du diagramme
- Une description du réseau


Voir des exemples sur cette [page](http://cisconetworkingcenter.blogspot.com/2012/09/network-diagram-templates.html).

![Exemples de cartouches](img/cartouches.png){ width=50%}

## Légende / étiquettes

Il est souvent nécessaire d’ajouter des étiquettes afin de mieux comprendre les symboles.

La légende est également très utile pour clarifier et faciliter la lecture d’un diagramme.

![Exemple de légende et d'étiquette](img/legende1.png)

Exemple de schéma avec légende et étiquettes

## Pratique

::: tip Réaliser un schéma de réseau
Utilisez soit LuciChart, Draw.io ou Visio pour reproduire ce schéma afin de vous familiariser avec les différentes commandes.

![Diagramme à réaliser](img/diag_reseau.png)

:::

::: tip Analyser un schéma de réseau

Analysez le schéma suivant et répondez aux questions suivantes :

1. Quel support de transmission peut être utilisé pour les connexions à 30Gb/s ?
2. Pourquoi les concepteurs ont-ils choisi d'utiliser des connexions à 10 Gb/s et d'autres à 1 Gb/s ?
3. Combien y a-t-il de routeurs dans ce schéma ?
4. Combien y a-t-il de commutateurs dans ce schéma ?

![Diagramme à analyser](img/ex_reseau.png)

:::