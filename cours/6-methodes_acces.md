<!-- Cours 3 -->

# Les méthodes d'accès

Les méthodes d'accès sont des techniques qui permettent de réguler l'accès aux ressources partagées. Elles sont utilisées dans les réseaux informatiques pour éviter les collisions et les conflits.

Elles permettent de contrôler le trafic sur un réseau (qui parle, quand et pour combien de temps).

Les méthodes d’accès au réseau permettent de différencier et de classer les réseaux en plusieurs catégories :
- **CSMA/CD** et **CSMA/CA** pour les réseaux en bus et en étoile (Ethernet)
- Le passage du jeton pour les réseaux en anneau (**Token ring**)

Chacune des méthodes est normalisé par l’[IEEE](https://fr.wikipedia.org/wiki/Institute_of_Electrical_and_Electronics_Engineers).

**CSMA** (Carrier Sense Multiple Access = Accès multiple avec détection de porteuse) => écoute d’un support à accès multiple

- **CSMA/CD** (Collision Detection) Utilisé dans les réseaux **Ethernet** traditionnels. Les dispositifs détectent les collisions pendant la transmission et arrêtent de transmettre, réduisant ainsi le temps perdu à cause de collisions. Les dernières informations peuvent être retransmises.
  - Implémenté dans le matériel de la carte réseau et géré par les pilotes. Les algorithmes de détection de collision sont intégrés dans les circuits de la carte réseau.

- **CSMA/CA** (Collision Avoidance - Prévention des collisions) Utilisé dans les réseaux sans fil comme Wi-Fi. Ici, les dispositifs tentent de prévenir les collisions en utilisant des mécanismes comme l'attente de temps aléatoire avant de transmettre. Cela minimise efficacement les risques de collision et permet une utilisation plus efficace du support. 
  - Intégré dans le matériel des dispositifs sans fil (comme les routeurs Wi-Fi et les adaptateurs sans fil) et est également géré par les pilotes et le logiciel du système d'exploitation.

