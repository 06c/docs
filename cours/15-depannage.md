<!-- Cours 22 -->

# Dépannage de réseau

## Les erreurs de couche 1 

Vérifier :
- Les connectivités physiques.
- Les câbles brisés.
- Les câbles déconnectés.
- Les câbles raccordés au mauvais port.
- Vérifier les voyants de liaison.
- Les connexions intermittentes.
- Vérifier les configurations de vitesse et de duplex.
- L'utilisation du mauvais câble pour la tâche courante (les câbles d'interconnexion, à paires inversées et droits doivent être correctement utilisés).
- Les unités hors tension.

![Dépannage couche 1](img/dep1.png)

## Les erreurs de couche 2

- Les interfaces Ethernet mal configurées.
- Un jeu d'encapsulage inapproprié.
- Une vitesse d'horloge inappropriée sur les interfaces série.

![Dépannage couche 2](img/dep2.png)

## Les erreurs de couche 3

- L'absence de protocole de routage activé.
- Un protocole de routage incorrect activé.
- Des adresses IP incorrectes.
- Des masques de sous- réseau incorrects.
- Des liaisons DNS-IP incorrectes.

![Dépannage couche 3](img/dep3.png)

## Autres erreurs possibles

- Les ports TCP/UDP bloqués.
- Les applications, protocoles ou ports bloquées par un pare-feu.
- Les applications bloquées par un ACL (Access Control List).

## Stratégies de dépannage de réseau

- Remonter les couches.
- Diviser le travail à l'avance, une personne par routeur.
- Avoir les journaux techniques et les outils disponibles.
- Douter de tout.
- Commencer à la couche 1 et remonter les couches. 
- Obtenir les voyants de liaison.
- Envoyer des requêtes ping.
- Ouvrir des sessions telnet ou ssh.

## Dépannage des problèmes courants

<p class="sticky">
<img src="/cours/img/dep4.png" alt="Dépannage des problèmes courants" style="width: 100%;">
</p>


Exemple d’un ordinateur (PC) qui ne peut pas communiquer avec le serveur à travers 2 routeurs (R1 et R2).

### À partir du PC, on peut faire un `ping` sur l’adresse de **loopback**

![ping sur l’adresse de loopback](img/dep5.png)

:::tip Remarque
Si le ping ne fonctionne pas, il y a un problème au niveau de la pile TCP/IP.
Il faut  réinstaller ou réinitialiser la carte réseau du PC.
:::


### `ping` sur l’hôte local, IP du PC

![ping sur l’hôte local](img/dep7.png)

::: tip Remarque
Si le ping ne fonctionne pas, il y a un problème au niveau de la pile TCP/IP. 
Il faut vérifier les paramètres TCP/IP du PC.
:::

### `ping` sur la passerelle par défaut, routeur R1

![ping sur le routeur R1](img/dep8.png)

### `ping` sur le routeur R2

![ping sur le routeur R2](img/dep9.png)

:::tip Remarque
Si les ping ne fonctionne pas, il y a un prolèeme entre le PC et le routeur R1 ou entre le routeur R1 et le routeur R2.
:::

### `ping` sur le serveur distant

![ping sur le serveur distant](img/dep10.png)

:::tip Remarque
Si le ping ne fonctionne pas, il y a un problème entre le routeur R2 et le serveur.
:::

## Outils


- ping
- traceroute (tracert sous windows)
- arp –a
- show ip arp => sur un routeur
- ipconfig ou ipconfig /all
- telnet / ssh

Voir le [cours sur Telnet](/cli/6-telnet.md)

Voir le [cours sur SSH](/cli/7-ssh.md)



<!-- style -->
<style>
  p.sticky {
    position: sticky;
    top: 70px;
    z-index: 500;
  }
  h3 {
    z-index: 100;
  }
</style>

<!-- 
<client-only>
window.addEventListener('load', function() {
  const element = document.querySelector('.sticky'); 
  console.log("element", element)
  document.addEventListener('scroll', function() {
    const scrollPosition = window.scrollY; 
    console.log(scrollPosition);
    if (scrollPosition > 5100) { 
      element.classList.remove('sticky'); 
    } else {
      element.classList.add('sticky'); 
    }
  });
});
</client-only> -->