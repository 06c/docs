# Vocabulaires

Voici une liste des termes les plus courants utilisés dans le domaine des réseaux informatiques afin de vous familiariser avec le vocabulaire.

Les notions seront approfondies dans les prochains cours.

## Nœuds 

Tout ce qui possède une adresse sur le réseau.
C’est par eux que transite l’information.

- Serveur
- Client (ordinateur qui n’est pas un serveur)
- Imprimante
- Téléphone IP
- Commutateur
- Routeur
- Point d’accès sans fil
- Modem
- …

« Hôtes » peut également désigner seulement un appareil qui est un point de terminaison comme l’ordinateur ou l’imprimante par exemple.

![Réseau](img/reseau_noeuds.png)

## Nuage

Le symbole nuage schématise la présence d'un autre réseau, par exemple Internet dans son ensemble. Il indique qu'il est possible de se connecter à cet autre réseau, sans donner de détails sur le mode de connexion ou le réseau.

Il peut également représenter l’inconnu.

![Nuage](img/nuage.png)

## Périphériques

### Périphériques de terminaison
Tous les composants qui se trouvent en bout de chaînes&nbsp;:
- Ordinateurs
- Imprimantes
- Téléphones
- Serveurs
- …
### Périphériques « intermédiaires »

Tous les composants qui permettent de relier les périphériques de terminaison entre eux&nbsp;:

- [Commutateurs](https://fr.wikipedia.org/wiki/Commutateur_réseau) (Switch) → relie plusieurs composants entre eux.
- [Routeur](https://fr.wikipedia.org/wiki/Routeur) → relie deux réseaux entre eux.
- [Point d’accès sans fil](https://fr.wikipedia.org/wiki/Point_d%27acc%C3%A8s_sans_fil) → permet aux périphériques sans fil de se connecter au réseau.
- [Modem](https://fr.wikipedia.org/wiki/Modem) (modulateur-démodulateur) → relie le réseau à Internet.

::: tip À savoir

Le modem installé par votre fournisseur d’accès regroupe plusieurs appareils :
- Un routeur
- Un commutateur
- Un point d’accès Wifi
- Un modem


![Modem Bell](img/m_bell.png){data-zoomable width=20%} ![Modem Vidéotron](img/m_videotron.png){data-zoomable width=20%} 

:::

## Supports 

Les supports de transmission sont les moyens physiques utilisés pour transmettre les données entre les différents nœuds du réseau.

- Câble Ethernet avec connecteur RJ45 

![RJ45](img/rj45.png)

- Fibre optique
- Ondes électromagnétiques (Wifi, Bluetooth, 4G, 5G, …)


## Schéma d'un réseau avec les différents éléments
![Réseau ](img/reseau_couleur.png)


## Serveurs

Un serveur, dans le contexte des réseaux informatiques, est un ordinateur ou un système informatique qui fournit des données, des services ou des ressources à d'autres ordinateurs, appelés clients, sur un réseau. 

Un serveur peut proposer plusieurs services à la fois.

En session 2, quand vous avez installé MySQL, vous avez installé un serveur de base de données sur votre ordinateur.

### Exemples de serveurs

- Serveur Web : Héberge des sites web et fournit des pages web aux clients via Internet.
- Serveur de Fichiers : Stocke et gère l'accès aux fichiers et aux données.
- Serveur de Base de Données.
- Serveur de Courriels.
- Serveur d'Application : Héberge et exécute des applications spécifiques.

`Serveur`

![Serveur](img/serveur.jpg){data-zoomable width=30%}


`Salle de serveurs`

![google-data-center](img/google-data-center-2.jpg){data-zoomable width=30%}


## Adresse IP

Une adresse IP (Internet Protocol) est un numéro d'identification qui est attribué à chaque périphérique connecté à un réseau informatique.

Par analogie avec le courrier postal, une adresse IP correspond à l'adresse de votre domicile.

Il existe deux versions d'adresses IP&nbsp;:

- IPv4 (32 bits)
- IPv6 (128 bits)

Une adresse IPv4 est composée de 4 nombres compris entre 0 et 255 séparés par des points.

Exemple&nbsp;: `56.223.23.2`

Une adresse IPv6 est composée de 8 nombres hexadécimaux séparés par des deux-points.

Exemple&nbsp;: `2001:0db8:85a3:0000:0000:8a2e:0370:7334`

::: tip À savoir

Votre modem est connecté à Internet et possède une adresse IP publique. Tous ce que vous faites sur Internet passe par cette adresse IP et les sites web que vous visitez peuvent la voir.
:::


<!-- Cours 2 -->

## Adresse MAC (Media Access Control)

L'adresse MAC (Media Access Control) est un identifiant physique unique attribué à chaque périphérique réseau.

Par analogie avec une voiture, une adresse MAC correspondrait au numéro de série de votre véhicule. Quelque soit l'endroit où vous vous trouvez, votre voiture aura toujours le même numéro de série.

Une adresse MAC est composée de 6 nombres hexadécimaux séparés par des deux-points ou des tirets.

Exemple&nbsp;: `00:0a:95:9d:68:16`

- L'adresse est fixe car elle est assignée à une interface réseau lors de sa production. Cependant certains logiciels permettent de la changer temporairement (MAC spoofing = usurpation d'adresse MAC)

- Cette adresse est unique à l'échelle mondiale.

- Elle consiste en un total de six octets (48 bits).

*1 chiffre hexadécimal est codé sur 4 bits. 1 octet est codé sur 8 bits*

La commande `ipconfig /all` permet d'afficher l'adresse MAC d'une interface réseau.


![Adresse MAC](img/adresse_mac.png)

Source : [OpenClassRooms](https://openclassrooms.com/fr/courses/6944606-concevez-votre-reseau-tcp-ip/7235400-identifiez-votre-machine-au-sein-d-un-reseau-local#/id/r-7235455)

Une adresse MAC est composée de 2 parties :

- La première moitié (les 3 premiers octets) identifie le fabricant de la carte réseau.

- La deuxième moitié identifie la carte réseau elle-même.

[Ce site](https://macvendors.com/) permet de trouver le fabricant d'une carte réseau à partir de son adresse MAC.

## Collision

Une collision se produit lorsqu'un périphérique envoie des données sur le réseau et qu'un autre périphérique envoie également des données sur le réseau en même temps.

Lorsqu'une collision se produit, les données sont perdues et doivent être renvoyées.

- Conséquences : Une collision entraîne une perte temporaire de communication sur le réseau. Tous les appareils doivent cesser d'envoyer des données, attendre un temps aléatoire, puis tenter de renvoyer leurs données, augmentant ainsi le temps de transmission et réduisant l'efficacité du réseau.

Les technologies modernes de réseau, comme l'utilisation de commutateurs et des topologies en étoile, réduisent considérablement le risque de collisions en fournissant des chemins de communication dédiés.


