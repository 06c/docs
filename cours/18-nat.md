<!-- Cours 26 -->

# NAT

Network Address Translation (NAT) est un mécanisme qui permet de traduire les adresses IP et les ports d'un paquet IP lorsqu'il traverse un routeur. Cela permet de masquer les adresses IP des machines du réseau interne et de les remplacer par une seule adresse IP publique.

Ce procédé est très largement utilisé par les box internet (modem / routeur) des fournisseurs d'accès pour cacher les ordinateurs domestiques derrière une seule identification publique. Il est également utilisé de façon similaire dans des réseaux privés virtuels.

Les appareils connectés chez vous ont probablement des adresses IP sur le réseau 192.168.1.0 /24. Ce sont des adresses **IP privées** qui ne sont pas routables sur Internet.

Mais sur Internet, vous avez une seule adresse **IP publique** qui est délivrée par votre fournisseur d'accès à Internet.

Vous pouvez connaître l'adresse IP qui vous est attribué par votre fournisseur d'accès à Internet en allant sur http://www.mon-ip.com/

## Exemple

![Exemple de NAT pour un domicile](img/nat1.png)


- L'ordinateur `192.168.1.5` envoi une requête au site `cegepgarneau.ca`. 
- La requête passe par le routeur.
- Le routeur envoi la requête vers le serveur qui héberge le site cegepgarneau.ca avec l'adresse IP `145.12.52.41` dans l'en-tête. 
- Le serveur reçoit la requête. 
- Le serveur envoi sa réponse vers l'IP `145.12.52.41` car il n'a aucune connaissance de l'adresse de l'ordinateur (`192.168.1.5`).
- Le routeur reçoit la réponse du serveur et il doit la rediriger vers l'ordinateur `192.168.1.5`. 

Mais comment le routeur connaît l'adresse IP vers laquelle il doit rediriger ce paquet ? C'est là que le NAT entre en jeu…

## Autre exemple

![Exemple de NAT pour un campus](img/nat2.png)

Un campus est composé de 1000 hôtes (ordinateurs, imprimantes, etc.), répartis dans plusieurs sous-réseaux. Sans utilisation du NAT, un tel campus nécessiterait l'attribution de presque 1000 adresses uniques et routées.

En connectant ce campus à Internet via un routeur qui implémente NAT, il est possible d'acheter un pool d'adresse IP. Par exemple 128 adresses du réseau `145.12.52.1/25` ont été attribuées à ce campus.

Les adresses privées seront « translatées » vers les adresses publiques.

::: tip Rappel
## Adresse IP privée

Les adresses IP privées sont des adresses IP qui ne sont pas routables sur Internet. Elles sont utilisées pour les réseaux privés.

Plage d'adresses privées&nbsp;:
| De          | à                |
|-------------|------------------|
| 10.0.0.0    | 10.255.255.255   |
| 172.16.0.0  | 172.31.255.255   |
| 192.168.0.0 | 192.168.255.255  |

## Adresse ip publique
Les adresses IP publiques sont des adresses attribuées à des systèmes informatiques, des serveurs ou des routeurs pour les connecter à Internet. Ces adresses sont uniques et routables sur Internet.

L'adresse IP publique est délivrée par votre FAI (fournisseur d'accès à Internet).

L'adresse IP publique est une adresse qui est unique au niveau mondial.
:::


## Pourquoi utiliser NAT&nbsp;?

- **Économie d'adresses IP**: Les adresses IP publiques sont limitées et coûteuses. En utilisant NAT, une seule adresse IP publique peut être utilisée pour plusieurs machines sur un réseau privé.
- **Simplicité**: Les adresses IP privées peuvent être utilisées sans conflit avec d'autres réseaux privés.
- **Sécurité**: Les adresses IP privées ne sont pas routables sur Internet. En utilisant NAT, les adresses IP privées sont masquées et ne sont pas visibles sur Internet. Il n'y a aucun moyen pour quelqu'un de l'extérieur, d'accéder aux machines internes (l'extérieur ne voit qu'une seule machine : la passerelle sur laquelle est implémentée NAT).

Cependant ce n'est pas une solution de sécurité complète : bien que le NAT fournisse une forme de sécurité par obscurité, il ne remplace pas les besoins pour un pare-feu robuste. Le NAT seul ne bloque pas les attaques qui peuvent passer par des connexions légitimes initiées de l'intérieur ou les malwares qui pourraient communiquer avec un serveur externe.

## Fonctionnement

Le NAT va remplacer les champs d'adresses dans les paquets qui sont destinés à un autre réseau.

Le NAT doit être effectué entre les deux interfaces réseau.

![Fonctionnement du NAT](img/nat3.png)

- Le NAT va remplacer l'IP source de A par son IP X puis va router le paquet vers l'extérieur.
- Le routeur conserve la correspondance des	adresses dans une table.
- Au retour du paquet, l'IP destination X sera remplacée par son correspondant sur le réseau soit A.

## Table de correspondance

Le routeur conserve une table de correspondance des adresses IP privées et publiques. Ainsi, il peut traduire les adresses IP des paquets qui transitent par lui et les rediriger vers les bonnes machines.

![Table de correspondance NAT](img/nat4.png)

## Techniques de translation


- **NAT statique**: Associe une adresse IP privée à une adresse IP publique de manière permanente.
- **NAT dynamique**: Associe une adresse IP privée à une adresse IP publique à partir d'un pool d'adresses IP publiques.
- **NAT dynamique avec surcharge (PAT)**: Associe une adresse IP privée à une adresse IP publique et un port.

La technique utilisée dépendra :
- du nombre d'IP publiques du réseau;
- des besoins en terme de services;
- de l'accessibilité;
- de la visibilité de l'extérieur.

## Configuration commune à tout type de NAT

La première chose à faire lorsque l'on configure du NAT, quel qu'en soit le type, c'est d'indiquer au routeur où se situe le réseau privé et où se situe le réseau public.

Le NAT ne prend effet que lorsqu'un paquet est routé d'une interface « inside » (côté privé) vers une interface « outside » (côté publique) et vice-versa.

Dans notre cas, les interfaces `Fa0/0` et `Fa1/0` sont du côté privé et seront déclarées comme « inside », l'interface `Se2/0` par contre, étant du côté public, sera configurée comme « outside ».

```shell
R1(config)# int fa0/0
R1(config-if)# ip nat inside
R1(config-if)# exit
R1(config)# int fa1/0
R1(config-if)# ip nat inside
R1(config-if)# exit
R1(config)# int s2/0
R1(config-if)# ip nat outside
R1(config-if)# exit
```

![Configuration commune à tout type de NAT](img/nat5.png)

## NAT statique

La NAT statique est généralement utilisée pour permettre aux périphériques externes de joindre des hôtes internes.

La NAT statique, se base sur l'association de `n` adresses avec `n` adresses. C'est à dire qu'à une adresse IP interne, on associe une adresse IP externe.

Dans ce cas, la seule action qui sera effectuée par le routeur sera de remplacer l'adresse source ou destination par l'adresse correspondante.

Ceci permet par exemple d'atteindre des serveurs publics, comme un serveur web, à l'intérieur d'une zone démilitarisée (DMZ) située derrière un pare-feu (firewall).

Aussi utilisé lorsqu'une machine interne doit être accédée de l'extérieur avec une adresse fixée. Par exemple, si cette machine se connecte à une base de données payante qui autorise l'accès en détectant l'adresse IP du client.

![NAT statique](img/nat6.png)

### Avantages et inconvénients

Il permet à une machine possédant une IP privée d'être vue et d'y accéder depuis Internet.

En revanche, il faut autant d'adresse publique que l'on souhaite avoir de poste relié à l'internet.


### Configuration du NAT statique pour le serveur `192.168.0.10`

![Configuration du nat statique](img/nat7.png)

Nous allons configurer une translation statique dans la table de translation NAT. Nous allons explicitement indiquer au routeur que ce qui arrive sur son interface publique (`S2/0`) et dont l'adresse destination est `201.49.10.30` (une des adresses du pool publique) doit être redirigé vers `192.168.0.10`.

Du point de vue du routeur cela revient à modifier l'adresse IP destination dans l'en-tête `IPv4` avant de router le paquet. Cela signifie aussi que si le serveur envoi un paquet vers internet, à la sortie de `S2/0` de `R1` l'adresse source (`192.168.0.10`) sera remplacée par l'adresse indiquée dans la translation, soit `201.49.10.30`

```shell	
R1(config)# ip nat inside source static 192.168.0.10 201.49.10.30
```

::: tip Info
Afin de tester dans Packet Tracer, il faut configurer une route vers le pool d'adresses publiques dans le routeur 2

```shell	
R2(config)# ip route 201.49.10.16 255.255.255.240 serial 2/0
```

Sur le routeur 1, il faut configurer une route par défaut comme suit&nbsp;:

```shell	
R1(config)# ip route 0.0.0.0 0.0.0.0 serial 2/0
```
:::

### Visualisation des translations en cours

```shell
R1# show ip nat translations
```

![Visualisation des translations en cours](img/nat8.png)


Pour visualiser le trafic :

```shell
R1# show ip nat statistics
```

## NAT dynamique

Contrairement au NAT statique, où on doit définir manuellement un mappage statique entre une adresse privée et publique, avec le NAT dynamique, le mappage d'une adresse locale à une adresse globale se produit de manière dynamique.

Cela signifie que le routeur sélectionne dynamiquement une adresse du groupe (pool) d'adresses global qui n'est pas actuellement affectée. Il peut s'agir de n'importe quelle adresse du pool. L'entrée dynamique reste dans la table des traductions NAT tant que le trafic est échangé. L'entrée expire après une période d'inactivité et l'adresse IP globale peut être utilisée pour de nouvelles traductions.

### Configuration du nat dynamique

Ne pas oublier de configurer les interfaces internes et externes du routeur tel que vu précédemment.

#### Étape 1: Définir le pool d'adresses IP publiques

Il faut créer le pool d'adresses :


```shell
R1(config)#ip nat pool POOL-NAT-LAN2 201.49.10.17 201.49.10.30 netmask 255.255.255.240
```

Ici on crée donc une plage d'adresse nommée `POOL-NAT-LAN2` allant de `201.49.10.17` à `201.49.10.30` avec un masque de sous-réseau de `255.255.255.240'.

#### Étape 2: Définir la liste d'accès (ACL) pour spécifier les adresses internes

Il faut définir une liste de contrôle d'accès (ACL) pour identifier quel trafic provenant de votre réseau interne doit être traduit.

```shell
R1(config)#access-list 10 permit 192.168.1.0 0.0.0.255
```

On autorise donc à être translatées les adresses ip du réseau `192.168.1.0/24`.

#### Étape 3: Activer le NAT dynamique

Il ne reste plus qu'à configurer le NAT en lui-même avec la commande
`ip nat inside source list <ACL number> pool <pool name>`

```shell
R1(config)#ip nat inside source list 10 pool POOL-NAT-LAN2
```

On instruit donc ici le routeur de créer dynamiquement une translation pour les paquets arrivant sur une interface « inside » routés par une interface « outside » dont l'adresse IP source correspond à l'`ACL 10` et de remplacer l'IP source par une de celles comprises dans le pool `POOL-NAT-LAN2`.

#### Étape 4: Enregistrer la configuration

Sauvegardez la configuration pour vous assurer qu'elle persiste après un redémarrage du routeur.

```shell
Router(config)# copy run start
```	


### Visualisation

```shell	
R1# sh ip nat translations 
Pro Inside global Inside local Outside local Outside global
icmp 201.49.10.18:2 192.168.1.10:2 80.79.100.1:2 80.79.100.1:2
```

### Table de correspondance

![Table de correspondance NAT dynamique](img/nat9.png)

Le NAT conserve la correspondance pour le retour des paquets.

Par défaut, les entrées de traduction expirent au bout de 24 heures, sauf si les compteurs ont été reconfigurés.

Si toutes les adresses du pool ont été utilisées, le prochain périphérique doit attendre qu'une adresse se libère avant de pouvoir accéder au réseau externe.


## NAT dynamique avec surcharge (sans pool)

Cette méthode est également appelée SUA (Single User Account) ou **PAT** (traduction d'adresse de port).

Une seule adresse IP publique est utilisée pour toutes les adresses IP privées internes, mais un port différent est attribué à chaque adresse IP privée.

Ce type de NAT est également connu sous le nom de NAT dynamique avec surcharge et c'est la forme typique de NAT utilisée dans les réseaux d'aujourd'hui. Il est même pris en charge par la plupart des routeurs grand public.

C'est cette technique qui est utilisé dans le modem/routeur que vous avez à la maison.

- La PAT est un NAT qui utilise **une seule adresse de sortie**. La difficulté réside dans la possibilité de trier les réponses au retour.
- Utilise des numéros de ports TCP. Lorsque le datagramme arrive au routeur, celui-ci remplace l'adresse privée par l'unique adresse, et le numéro de port de la station cliente (numéro de port choisi arbitrairement).
- Il note la correspondance entre ce numéro et l'adresse et le numéro de port de la station.
- Au retour, correspondance avec l'adresse IP de la station et le bon numéro de port.

## Configuration du NAT dynamique avec surcharge

Création d'une nouvelle ACL.

```shell	
R1(config)# access-list 2 permit 192.168.1.0 0.0.0.255
```

Il ne reste plus qu'à configurer le NAT.

```shell	
R1(config)# ip nat inside source list 2 interface serial 2/0 overload
```

Nous disons ici au routeur de translater les paquets provenant des adresses décrites dans l'ACL 2 (`192.168.1.0/24`) et de remplacer l'adresse IP source par celle configurée sur l'interface `Serial 2/0` en la surchargeant pour permettre à plus d'une machine de communiquer avec l'extérieur (PAT).

Il n'y a pas de groupe puisque seulement une adresse IP publique est disponible. Comme cette adresse, déjà utilisée par l'interface de sortie, est souvent dynamique, la seule information invariable est l'interface de sortie.

### NAT dynamique avec surcharge (avec pool)

Il est également possible d'utiliser la surcharge avec un pool d'adresse IP.

Ainsi le système pourra utiliser plusieurs adresses IP pour rejoindre l'extérieur mais également ajouter des numéros de port quand une adresse IP est utilisé plusieurs fois.

Typiquement c'est utile s'il y a davantage de machine dans le réseau privé que d'adresses publiques disponibles.

Il faut reprendre la configuration du **NAT dynamique** et rajouter le mot clé **overload** à la commande :

```shell	
R1(config)#ip nat inside source list 1 pool POOL-NAT-LAN2 overload
```

Ceci permet de « partager » les adresses publiques en translatant également les numéros de ports dans l'entête de la couche transport.
