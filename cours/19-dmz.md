<!-- Cours 26 -->

# DMZ

- DMZ => (Demilitarized Zone - Zone démilitarisée) fait référence à un sous-réseau qui héberge les services exposés et accessibles de l'extérieur d'une entreprise.
- Elle agit comme une zone tampon avec les réseaux non sécurisés tels qu'Internet. 
- Située entre le réseau local et Internet, derrière le parefeu.
- Zone considérée comme « peu fiable ».
- Correspond à un réseau intermédiaire regroupant des serveurs publics (HTTP, SMTP, FTP, DNS, etc.).
- Éviter toute connexion directe avec le réseau interne et prévenir celui-ci de toute attaque extérieure depuis le Web.
- Terme qui fait référence au nom donné à la zone tampon entre la Corée du Sud et la Corée du Nord.


![Représentation DMZ](img/dmz1.png)


Une DMZ, ou zone démilitarisée, est un concept de sécurité réseau utilisé pour ajouter une couche supplémentaire de protection pour les réseaux internes d'une organisation. Elle est conçue pour agir comme un tampon entre le réseau interne sécurisé d'une entreprise (intranet) et un réseau externe non sécurisé, généralement Internet.

### Objectif de la DMZ

L'objectif principal d'une DMZ est de fournir une zone où les services accessibles depuis Internet peuvent être placés, isolant ainsi le reste du réseau interne de la portée directe de l'Internet. Cela aide à protéger les données sensibles en cas d'attaque réussie contre les services hébergés dans la DMZ.

### Fonctionnement de la DMZ

Les systèmes placés dans une DMZ sont accessibles à la fois depuis le réseau interne et depuis Internet, mais ils ont des restrictions strictes quant aux types de communication qu'ils peuvent initier avec le réseau interne. Par exemple, un serveur web dans la DMZ peut répondre aux requêtes HTTP provenant d'Internet sans exposer les autres parties du réseau interne à ces interactions directes.

### Configuration Typique

Dans une configuration typique de DMZ, le réseau est protégé par au moins deux pare-feux :

1. **Premier Pare-feu :** Placé entre l'Internet et la DMZ, ce pare-feu est configuré pour permettre le trafic destiné aux services spécifiques dans la DMZ (comme le serveur web, le serveur de mail, etc.), tout en bloquant tout autre trafic vers le réseau interne.

2. **Deuxième Pare-feu :** Placé entre la DMZ et le réseau interne, ce pare-feu est configuré pour bloquer presque tous les accès directs de la DMZ au réseau interne, permettant seulement les communications nécessaires et strictement contrôlées qui sont essentielles pour le fonctionnement des services.

![Représentation DMZ](img/dmz2.png)

### Exemples d'Utilisation de la DMZ

- **Serveurs Web :** Les serveurs qui hébergent des sites web sont souvent placés dans la DMZ. Ils ont besoin d'être accessibles depuis Internet, mais vous ne voulez pas exposer votre réseau interne si le serveur est compromis.

- **Serveurs de Mail :** De même, les serveurs de messagerie qui doivent communiquer avec Internet pour envoyer et recevoir des courriels de l'extérieur sont généralement placés dans la DMZ.

- **Serveurs FTP :** Les serveurs FTP destinés à l'échange de fichiers avec des entités externes peuvent également être hébergés dans la DMZ pour isoler le trafic réseau associé du reste du réseau interne.

