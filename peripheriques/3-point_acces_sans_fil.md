<!-- Cours 2 -->
# Point d'accès sans fil (Wireless Access Point)

Équipement de couche 2 du modèle OSI.

- Un point d'accès sans fil (en anglais Wireless Access Point ou WAP) est un équipement réseau qui permet de connecter des ordinateurs, des tablettes ou des smartphones à un réseau sans fil (WLAN).
- Il permet de créer un réseau local sans fil (WLAN) et de faire communiquer les ordinateurs entre eux.
- Il est relié à un routeur ou un commutateur par un câble Ethernet.
- Il peut être relié en cascade avec un autre point d'accès sans fil.
- Travaille avec les **adresses MAC** (Media Access Control)

![Point d'accès sans fil](img/point_acces_sans_fil.png)
