<!-- Cours 4 -->

# Routeur (Router)

Équipement de couche 3 du modèle OSI.

- Le routeur analyse le contenu de la trame et choisit le meilleur chemin pour que celle-ci arrive à destination. Il travaille avec des adresses réseau comme IP.
- Un routeur sert d'aiguillage entre plusieurs réseaux. Il est capable de détecter l'adresse de destination d'un paquet pour l'orienter au mieux vers sa destination. Les routeurs contiennent des tables de routage dans lesquelles ils mémorisent les itinéraires entre réseaux. 
- Travaille avec les **adresses IP** (Internet Protocol)

- Le routeur que vous voyez sur la figure peut gérer deux réseaux à partir des ports Ethernet 0/0 et Ethernet 0/1

Remarque : On trouve sur le marché des routeurs qui regroupent un routeur, un commutateur et un point d'accés sans fil en un seul appareil.

![Routeur](img/routeur.png)