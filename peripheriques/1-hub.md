<!-- Cours 2 -->
# Concentrateur (Hub)

Équipement de couche 1 du modèle OSI.

- Pièce d’équipement réseau qui sert à relier les câbles provenant des postes de travail. C’est à travers le concentrateur que les postes peuvent communiquer. Tous les câbles du réseau y sont concentrés en un seul point.
- Dès qu’une machine communique, **toutes les autres machines reçoivent le signal**. Un concentrateur est un répéteur multiports. Il ne fait que répéter l’information sans analyser le contenu de la trame qui le traverse. On le nomme parfois concentrateur passif.
- Il peut être relié en cascade avec un autre concentrateur.

- Génère un nombre important de collisions (lorsque deux équipement émettent en même temps sur un support partagé).
- N’est plus utilisé aujourd’hui.

![Hub](img/hub.png)