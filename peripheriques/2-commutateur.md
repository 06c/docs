<!-- Cours 2 -->
# Commutateur (Switch)

Équipement de couche 2 du modèle OSI.

Un commutateur (switch en anglais) est un équipement réseau qui permet de connecter plusieurs ordinateurs entre eux. Il permet de créer un réseau local (LAN) et de faire communiquer les ordinateurs entre eux.

- On l’appelle aussi concentrateur actif.
- Travaille avec les **adresses MAC** (Media Access Control)
- Comme pour le concentrateur il peut être relié en cascade, c'est à dire qu'on peut connecter un commutateur à un autre commutateur.

![Commutateur](img/commutateur.png)

Le commutateur travaille avec les **adresses MAC**, c'est à dire qu'il va ouvrir le paquet qu'il reçoit et regarder l'adresse MAC de destination pour envoyer le paquet uniquement sur le port correspondant à l'adresse MAC de destination.

S'il ne trouve pas l'adresse MAC de destination dans sa table, il envoie le paquet sur tous les ports sauf sur le port d'origine.